package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioSeisTest {

    @Test
    void obterAlturaDoPredio() {
        //arrange
        double sombraEd = 40;
        double sombraP = 4;
        double altP = 2;
        double expected = 20;

        //act
        //não precisamos de colocar dentro dos () o double por só estamos a inserir as variáveis
        double result = ExercicioSeis.obterAlturaDoPredio(sombraEd, sombraP, altP);

        //assert
        assertEquals(expected,result,0.001);
    }
}