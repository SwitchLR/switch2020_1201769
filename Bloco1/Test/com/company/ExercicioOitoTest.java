package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioOitoTest {

    @Test
    void getLadoC() {
        //arrange
        double ladoA = 60;
        double ladoB = 40;
        double anguloC = 60;
        double expected = 98.8513141338685;

        //act
        double result = ExercicioOito.getLadoC(ladoA,ladoB,anguloC);

        //assert
        assertEquals(expected,result, 0.0000001);

    }
}