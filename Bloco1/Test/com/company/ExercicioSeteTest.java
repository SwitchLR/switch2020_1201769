package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioSeteTest {

    @Test
    void getDistancia() {
        //arrange
        double seg = 3900;
        double expected = 11.3255677907777;


        //act
        double result =ExercicioSete.getDistancia(seg);

        //assert
        assertEquals(expected,result,0.0001);
    }
}