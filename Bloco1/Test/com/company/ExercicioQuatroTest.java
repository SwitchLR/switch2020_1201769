package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioQuatroTest {

    @Test
    void obterDistancia() {
        // arrange
        int vs = 340;
        double intTempo = 10;
        double expected = 3400;

        //act
        double result = ExercicioQuatro.obterDistancia(intTempo,vs);

        //assert
        assertEquals(expected,result,0.0001);
    }
}