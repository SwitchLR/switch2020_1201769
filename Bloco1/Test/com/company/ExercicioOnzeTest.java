package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioOnzeTest {

    @Test
    void getExpressao() {
        //arrange
        double variavel = -1;
        double expected = 5;


        //act
        double result = ExercicioOnze.getExpressao(variavel);

        //assert
        assertEquals(expected,result,0.000001);

    }
}