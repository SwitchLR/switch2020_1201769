package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioTrezeTest {

    @Test
    void getSoMinutos() {
        //arrange
        int minutos = 50;
        int horas = 5;
        int expected = 350;

        //act
        int result = ExercicioTreze.getSoMinutos(minutos, horas);

        //assert
        assertEquals(expected, result, 0.1);

    }
}