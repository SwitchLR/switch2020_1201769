package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDoisTest {

    @Test
    void obterTotalFlores() {
        //arrange
        int rosas = 2;
        int tulipas = 2;
        double precoRosa = 2;
        double precoTulipa = 2;
        double expected = 8;

        //act
        double result = ExercicioDois.obterTotalFlores(rosas,tulipas,precoRosa,precoTulipa);

        //assert
        assertEquals(expected,result,0.001);
    }

    @Test
    void obterTotalFloresTest2() {
        //arrange
        int rosas = 4;
        int tulipas = 4;
        double precoRosa = 4.5;
        double precoTulipa = 4;
        double expected = 34;

        //act
        double result = ExercicioDois.obterTotalFlores(rosas,tulipas,precoRosa,precoTulipa);

        //assert
        assertEquals(expected,result,0.001);
    }

}