package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezTest {

    @Test
    void getHipotenusa() {
        //arrrange
        double cateto1 = 6;
        double cateto2 = 8;
        double expected = 10;


        //act

        double result = ExercicioDez.getHipotenusa(cateto1,cateto2);

        //asserts
        assertEquals(expected,result,0.001);

    }
}