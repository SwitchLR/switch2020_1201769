package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioNoveTest {

    @Test
    void getPerimetro() {
    //arrange
    double ladoMenor = 4;
    double ladoMaior = 10;
    double expected = 28;


    //act
    double result = ExercicioNove.getPerimetro(ladoMenor, ladoMaior);


    //assert
    assertEquals(expected,result,0.00001);

    }
}