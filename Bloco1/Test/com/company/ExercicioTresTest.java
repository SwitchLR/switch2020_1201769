package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioTresTest {

    @Test
    void volumeCilindro() {
        //arrange
        double raio = 2;
        double altura = 10;
        double expected = 125.66370614359172;

        //act
        double result = ExercicioTres.volumeCilindro(raio,altura);

        //assert
        assertEquals(expected,result,0.00001);

    }
}