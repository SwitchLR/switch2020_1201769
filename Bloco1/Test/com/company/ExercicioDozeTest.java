package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDozeTest {

    @Test
    void getFahrenheit() {
        //arrange
        double celsius = 18;
        double expected = 50;


        //act
        double result = ExercicioDoze.getFahrenheit(celsius);

        //asserts
        assertEquals(expected,result,0.000001);

    }
}