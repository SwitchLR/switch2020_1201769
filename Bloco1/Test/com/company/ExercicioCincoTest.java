package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioCincoTest {

    @Test
    void getD() {
        //arrange
        int v = 0;
        double a = 9.8;
        double t = 2;
        double expected = 19.6;

        //act

        double result = ExercicioCinco.getD(t,v,a);

        //assert
        assertEquals(expected,result,0.001);

    }
}