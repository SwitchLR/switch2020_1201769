package com.company;

import java.util.Scanner;

public class ExercicioDoze {
    public static void main(String[] args) {
        Fahrenheit();
    }

    public static double Fahrenheit(){
        double fahrenheit, celsius;

        Scanner ler = new Scanner(System.in);
        System.out.println("Indique a tempuratura em Celsius:");
        celsius = ler.nextDouble();

        fahrenheit = getFahrenheit(celsius);

        System.out.println("O valor em Fahrenheit é: " + fahrenheit);

        return fahrenheit;

    }

    public static double getFahrenheit(double celsius) {
        double fahrenheit;
        fahrenheit = 32 + (9.0/5.0) * celsius;
        return fahrenheit;
    }
}
