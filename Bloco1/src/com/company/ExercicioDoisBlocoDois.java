package com.company;

import java.util.Scanner;


public class ExercicioDoisBlocoDois {
    public static void main(String[] args) {
        digitos();
    }

    public static double digitos() {
        int numero, digito1, digito2, digito3;

        Scanner ler = new Scanner(System.in);
        System.out.println("Indique o numero de 3 digitos:");
        numero = ler.nextInt();

        if (numero < 100 || numero > 999) {
            System.out.println("O numero não tem três digitos!");

        } else {
            digito3 = numero % 10;
            digito2 = (numero / 10) % 10;
            digito1 = (numero / 100) % 10;

            System.out.println(digito1 + " " + digito2 + " " + digito3 + ".");
        }
        if (numero % 2 == 0) {
            System.out.println("O número é par.");
        } else
            System.out.println("O número é impar.");


        return numero;
    }

    public static double getDigito3(int numero) {
        double digito3;
        digito3 = numero % 10;
        return digito3;
    }

    public static double getDigito2(int numero) {
        double digito2;
        digito2 = (numero / 10) % 10;
        return digito2;
    }

    public static double getDigito1(int numero) {
        double digito1;
        digito1 = (numero / 100) % 10;
        return digito1;
    }

    public static boolean parOuImpar(int numero) {
        if (numero % 2 == 0) {
            return true;

        } else
            return false;

    }

}
