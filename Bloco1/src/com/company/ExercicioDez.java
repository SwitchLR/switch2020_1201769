package com.company;

import java.util.Scanner;

public class ExercicioDez {
    public static void main(String[] args) {
        hipotenusa();

    }

    public static double hipotenusa() {
        double cateto1, cateto2, hipotenusa;

        Scanner ler = new Scanner(System.in);
        System.out.println("Indique as medidas de um cateto:");
        cateto1 = ler.nextDouble();

        System.out.println("Indique as medidas do outro cateto:");
        cateto2 = ler.nextDouble();

        hipotenusa = getHipotenusa(cateto1, cateto2);

        System.out.println("A medida da hipotenusa é: " + hipotenusa);

        return hipotenusa;

    }

    public static double getHipotenusa(double cateto1, double cateto2) {
        double hipotenusa;
        hipotenusa = Math.sqrt((Math.pow(cateto1, 2)) + Math.pow(cateto2, 2));
        return hipotenusa;

    }

}
