package com.company;

import java.util.Scanner;


public class ExercicioSete {
    public static void main(String[] args) {
        distanciaZe();
    }

    public static double distanciaZe() {
        double distancia;
        double seg;
        //double velocidadeMedia = 42195/14530;

        Scanner intTempo = new Scanner(System.in);
        System.out.println("Indique quanto tempo correu em segundos: ");
        seg = intTempo.nextDouble();

        distancia = (((42195 * seg) / 14530) / 1000);

        System.out.println("Percorreu " + String.format("%.2f", distancia) + "kms");

        return distancia;
    }

    public static double getDistancia(double seg) {
        double distancia;
        distancia = (((42195 * seg) / 14530) / 1000);
        return distancia;
    }


}
