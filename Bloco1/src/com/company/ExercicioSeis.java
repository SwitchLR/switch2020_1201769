package com.company;

public class ExercicioSeis  {

    // a função corre com o main, mas para fazer o teste não precisamos de "correr" a função
    //public static void main (String[] args){

    //}

    public static double obterAlturaDoPredio(double sombraEd, double sombraP, double altP) {

        double altEd;
        altEd = (sombraEd / sombraP) * altP;

        return altEd;
    }

}