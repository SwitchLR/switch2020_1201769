package com.company;
import java.util.Scanner;

public class ExercicioUm {
    public static void main (String[] args) {
        exercicio1();
    }

      public static void exercicio1() {

           //estrutura de dados
           int rapazes, raparigas;
           double percentagemDeRapazes, percentagemDeRaparigas, total;

           //Inicialização para leitura de dados
           Scanner ler = new Scanner(System.in);

           //leitura de dados
           System.out.println("Introduza o número de rapazes");
           rapazes = ler.nextInt();

           System.out.println("Introduza o número de raparigas");
           raparigas = ler.nextInt();

           //processamento
           total = rapazes + raparigas;
           percentagemDeRapazes = rapazes/total;
           percentagemDeRaparigas = raparigas/total;

           //saida de dados
           System.out.println("Percentagem de rapazes: " + percentagemDeRapazes * 100);
           System.out.println("Percentagem de raprigas: " + percentagemDeRaparigas * 100);

       }

   }

