package com.company;
import java.util.Scanner;


public class ExercicioTresBlocoDois {
    public static void main(String[] args) {
        pontoPlano();
    }

    public static double pontoPlano(){

        double d;

        int ponto1 [] = new int[2];
        int ponto2 [] = new int[2];
        Scanner ler = new Scanner(System.in);

        System.out.println("Indique as coordenadas x,y do primeiro ponto");
        ponto1 [0] = ler.nextInt();
        ponto1 [1] = ler.nextInt();

        System.out.println("Indique as coordenadas x,y do segundo ponto");
        ponto2 [0] = ler.nextInt();
        ponto2 [1] = ler.nextInt();

        d = Math.sqrt(Math.pow((ponto2 [0]- ponto1 [0]),2) + (Math.pow((ponto2[1] - ponto1 [1]),2)));

        System.out.println("Distância entre os pontos: " + d);

        return d;
    }

}
