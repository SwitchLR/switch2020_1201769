package com.company;

import java.util.Scanner;


public class ExercicioTreze {
    public static void main(String[] args) {
        emMinutos();
    }

    public static int emMinutos() {
        int minutos, horas, soMinutos;


        Scanner ler = new Scanner(System.in);
        System.out.println("Indique o número de horas:");
        horas = ler.nextInt();

        System.out.println("Indique o número de minutos:");
        minutos = ler.nextInt();

        soMinutos = getSoMinutos(minutos, horas);

        System.out.println("O total de minutos é: " + soMinutos);

        return soMinutos;

    }

    public static int getSoMinutos(int minutos, int horas) {
        int soMinutos;
        soMinutos = (horas * 60) + minutos;
        return soMinutos;
    }
}

