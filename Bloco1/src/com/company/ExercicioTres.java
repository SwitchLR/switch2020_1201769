package com.company;
import java.util.Scanner;



public class ExercicioTres {
    public static void main (String[] args) {
        exercicio3();
    }

    public static void exercicio3() {
       double raio, altura, volume;

       Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o valor do raio da base e metros");
        raio = ler.nextInt();
        System.out.println("Introduza o valor da altura em metros");
        altura = ler.nextInt();

        volume = volumeCilindro ( raio , altura) * 1000;
        System.out.println("O volume em litros deste sólido é de "+ String.format("%.2f",volume) + "litros.");

        //return volume;
    }

    public static double volumeCilindro (double raio, double altura){
        double volume;
        volume = Math.PI * raio * raio * altura;
        return volume;

    }
}