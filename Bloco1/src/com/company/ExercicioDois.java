package com.company;

import java.util.Scanner;


public class ExercicioDois {
    public static void main(String[] args) {
        exercicio2();
    }

    public static void exercicio2() {
        int rosas, tulipas;
        double precoRosa, precoTulipa;

        //Inicialização para leitura de dados
        Scanner ler = new Scanner(System.in);

        //leitura de dados
        System.out.println("Introduza o número de rosas: ");
        rosas = ler.nextInt();

        System.out.println("Introduza o número de tulipas: ");
        tulipas = ler.nextInt();

        System.out.println("Intruduza o preço das rosas: ");
        precoRosa = ler.nextDouble();

        System.out.println("Introduza o preço das tulipas");
        precoTulipa = ler.nextDouble();

        /*return obterTotalFlores(rosas, tulipas, precoRosa, precoTulipa);
        botão direito do rato -> refactor
        só na "formula"
        */
        System.out.println(obterTotalFlores(rosas, tulipas, precoRosa, precoTulipa));
    }

    public static double obterTotalFlores(int rosas, int tulipas, double precoRosa, double precoTulipa) {
        return (rosas * precoRosa) + (tulipas * precoTulipa);

    }

}

