package com.company;

import java.util.Scanner;

public class ExercicioCinco {

    public static void main(String[] args) {

        exercicio5();
    }

    public static double exercicio5() {
        double d, t;
        int v = 0;
        double a = 9.8;

        Scanner ler = new Scanner(System.in);
        System.out.println("Indique o tempo cronemetrado em segundos:");
        t = ler.nextInt();

        d = getD(t, v, a);

        System.out.println("A altura do prédio é: " + d + " metros.");
        return d;

    }

    //isto foi feito sublinhando a formula -> refactor
    public static double getD(double t, int v, double a) {
        double d;
        d = (v * t) + ((a * t * t) / 2);
        return d;
    }


    /*
        Isto foi o que fiz à mão:
        public static double getDist(int v, double a, double t) {
        double dist;
        dist = (v * t) + ((a * t * t) / 2);
        return dist;
    }

 */
}

