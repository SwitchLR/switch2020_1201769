package com.company;

import java.util.Scanner;

public class ExercicioNove {
    public static void main(String[] args) {

        perimetroRectangulo();
    }

    public static double perimetroRectangulo() {
        double ladoMenor, ladoMaior, perimetro;

        Scanner ler = new Scanner(System.in);
        System.out.println("Indique a dimensão do lado menor");
        ladoMenor = ler.nextDouble();

        System.out.println("Indique a dimensão do lado maior");
        ladoMaior = ler.nextDouble();

        perimetro = getPerimetro(ladoMenor, ladoMaior);

        System.out.println("O perimetro do rectangulo é " + perimetro + ".");

        return perimetro;
    }

    public static double getPerimetro(double ladoMenor, double ladoMaior) {
        double perimetro;
        perimetro = (2 * ladoMenor) + (2 * ladoMaior);
        return perimetro;
    }

}
