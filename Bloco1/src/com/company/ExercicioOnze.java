package com.company;

import java.util.Scanner;

public class ExercicioOnze {

    public static void main(String[] args) {
        expressao();
    }

    public static double expressao() {
        double variavel, expressao;

        Scanner ler = new Scanner(System.in);
        System.out.println("Indique o valor da variável: ");
        variavel = ler.nextDouble();

        expressao = Math.pow(variavel, 2) - (3 * variavel) + 1;

        System.out.println("o valor é: " + expressao);

        return expressao;
    }

    public static double getExpressao(double variavel) {
        double expressao;
        expressao = Math.pow(variavel, 2) - (3 * variavel) + 1;
        return expressao;
    }
}
