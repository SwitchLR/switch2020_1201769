package com.company;

import java.util.Scanner;


public class ExercicioOito {
    public static void main(String[] args) {
        distanciaOperarios();
    }

    public static double distanciaOperarios() {
        double ladoA, ladoB, ladoC, anguloC;

        Scanner ler = new Scanner(System.in);
        System.out.println("Indique a dimensão do cabo maior em metros:");
        ladoA = ler.nextDouble();

        System.out.println("Indique a dimensão do cabo menor em metros:");
        ladoB = ler.nextDouble();

        System.out.println("Indique o ângulo entre os dois cabos:");
        anguloC = ler.nextDouble();

        ladoC = getLadoC(ladoA, ladoB, anguloC);

        System.out.println("Os operários estão a " + String.format("%.2f", ladoC) + "m de distância.");

        return ladoC;

    }

    public static double getLadoC(double ladoA, double ladoB, double anguloC) {
        double ladoC;
        ladoC = Math.sqrt(Math.pow(ladoA, 2) + Math.pow(ladoB, 2) - ((2 * ladoA * ladoB) * Math.cos(anguloC)));
        return ladoC;
    }

}