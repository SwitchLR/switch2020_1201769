package com.company;
import java.util.Scanner;



public class ExercicioQuatro {
    public static void main (String[] args) {
        exercicio4();
        intervaloTempo();
    }

        public static double exercicio4() {
        double distancia,intTempo;
        int vs = 340;

        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o intervalo de tempo em segundos");
        intTempo = ler.nextInt();

        distancia = vs * intTempo;

        System.out.println("a distancia é :" + distancia);

        return distancia;

    }

    public static double intervaloTempo() {
        int vs = 340;
        double tempo;


        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza a distância em metros");
        double dist = ler.nextInt();

        tempo = dist/vs;

        System.out.println("o tempo é :" + String.format("%.2f",tempo));
        return tempo;
    }

    public static double obterDistancia (double intTempo, int vs) {
        double distancia;
        distancia = vs * intTempo;
        return distancia;


    }

}
