package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentTest {

    @Test
    public void createValidStudent() {
        //Arrange+Act
        Student student = new Student(1190001, "Paulo");
        //Assert
        assertNotNull(student);
    }

    /*   @Test
       public void doEvaluation() {
           //arrange + act
           Student estudante = new Student(1217069, "Rodrigues");

           estudante.doEvaluation(15);
           int expected = 15;
           int result = estudante.grade;

           assertEquals(expected,result);
       }
   */
    @Test
    public void createStudentNegativeNumberWith6Digits() {
        assertThrows(IllegalArgumentException.class, () -> {
            Student student = new Student(-1900001, "Paulo Maio");
        });
    }

    @Test
    public void creatStudentNegativeNumberWith7Digits() {
        assertThrows(IllegalArgumentException.class, () -> {
            Student student = new Student(-1190001, "Paulo Maio");
        });
    }

    @Test
    public void createStudentLongerNumberDigits() {
        assertThrows(IllegalArgumentException.class, () -> {
            Student student = new Student(11900013, "Paulo Maio");
        });
    }

    @Test
    public void creatStudentNameIsEmpty() {
        assertThrows(IllegalArgumentException.class, () -> {
            Student student = new Student(1190001, "");
        });
    }

    @Test
    public void createStudentNameIsFullOfSpaces() {
        assertThrows(IllegalArgumentException.class, () -> {
            Student student = new Student(1190001, "    ");
        });
    }

    @Test
    public void createStudentShortNameLength() {
        assertThrows(IllegalArgumentException.class, () -> {
            Student student = new Student(1190001, "Rui");
        });
    }

    @Test
    public void createStudentInvalidadValidNumberButInvalidadeName() {
        assertThrows(IllegalArgumentException.class, () -> {
            Student student = new Student(1980398, "Bia");
        });
    }

    @Test
    public void sortStudentsByNumberAscWithSeveralElementsIncorrectlyOrdered() {
        //arrange
        Student student1 = new Student(1200145, "Sampaio");
        Student student2 = new Student(1200054, "Moreira");
        Student student3 = new Student(1200086, "Silva");

        Student[] students = {student1, student2, student3};
        Student[] expected = {student2, student3, student1};

        //act
        boolean result = Student.sortStudentsByNumberAsc(students);

        //assert
        assertTrue(result);
        assertArrayEquals(expected, students);
    }

    @Test
    public void createStudentList() {
        //act
        StudentList stList = new StudentList(); //StudentList empty
        Student[] result = stList.toArray();

        //assert
        assertEquals(0, result.length); //check array
    }

    @Test
    public void creatStudentListWithSomeElements() {
        //arrange
        Student student1 = new Student(1200145, "Sampaio");
        Student student2 = new Student(1200054, "Moreira");
        Student student3 = new Student(1200086, "Silva");

        Student[] students = {student1, student2, student3}; //some order
        Student[] expected = {student1, student2, student3}; // a copy of students

        //act
        StudentList stList = new StudentList(students);
        students[2] = student1; // change the original array
        Student[] result = stList.toArray();

        //assert
        assertArrayEquals(expected, result); //check students are the same
        assertNotSame(students, result);
    }

    @Test

    public void sortByGradeDescWithSeveralElementsIncorrectlyOrdered() {
        //arrange
        Student student1 = new Student(1200145, "Sampaio");
        student1.doEvaluation(12);
        Student student2 = new Student(1200054, "Moreira");
        student2.doEvaluation(17);
        Student student3 = new Student(1200086, "Silva");
        student3.doEvaluation(15);

        Student[] students = {student1, student2, student3}; //unordered
        StudentList stList = new StudentList(students);
        Student[] expected = {student2, student3, student1}; // Ordered

        //act
        stList.sortByGradeDesc();
        Student[] result = stList.toArray();

        //assert
        assertArrayEquals(expected, result);
    }

    @Test
    public void equalsTrue() {
        //arrange
        Student student1 = new Student(1980398, "Beatriz");
        Student student2 = new Student(1980398, "Beatriz Costa");

        //act
        boolean result = student1.equals(student2);

        //assert
        assertTrue(result);
    }

    @Test
    public void equalsTrueToItSelf() {
        //arrange
        Student student1 = new Student(1980398, "Beatriz");

        //act+assert
        assertTrue(student1.equals(student1));
    }

    @Test
    public void equalsFalseDueToNull() {
        //arrange
        Student student1 = new Student(1980398, "Beatriz");

        //act
        boolean result = student1.equals(null);

        //assert
        assertFalse(result);
    }

    @Test
    public void equalsFalseDueToDifferentType() {
        //arrange
        Student student1 = new Student(1980398, "Beatriz");

        //act
        boolean result = student1.equals(new String("1980398"));

        //assert
        assertFalse(result);
    }

    @Test
    public void equalsFalseDueToDifferentNumbers() {
        //arrange
        Student student1 = new Student(1980398, "Beatriz");
        Student student2 = new Student(1980399, "Beatriz Costa");

        //act
        boolean result = student1.equals(student2);

        //assert
        assertFalse(result);

    }


}