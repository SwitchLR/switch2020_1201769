package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioQuatorzeTest {

    @Test
    void verificaMatrizRectuanguloTesteUm() {
        int[][] array2D = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9, 5},
        };

        boolean expected = false;

        boolean result = ExercicioQuatorze.verificaMatrizRectuangulo(array2D);

        assertEquals(expected, result);
    }

    @Test
    void verificaMatrizRectuanguloTesteDois() {
        int[][] array2D = {
                {1, 2, 3, 5},
                {4, 5, 6, 5},
                {7, 8, 9, 5},
        };

        boolean expected = true;

        boolean result = ExercicioQuatorze.verificaMatrizRectuangulo(array2D);

        assertEquals(expected, result);
    }

    @Test
    void verificaMatrizRectuanguloTesteTres() {
        int[][] array2D = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9},
        };

        boolean expected = false;

        boolean result = ExercicioQuatorze.verificaMatrizRectuangulo(array2D);

        assertEquals(expected, result);
    }
}