package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDoisTest {

    @Test
    void arrayDigitsTest() {
        int number = 1234;
        int [] expected = {1,2,3,4};

        int [] result = ExercicioDois.arrayDigits(number);

        assertArrayEquals(expected,result);
    }

    @Test
    void arrayDigitsTestDois() {
        int number = 123004;
        int [] expected = {1,2,3,0,0,4};

        int [] result = ExercicioDois.arrayDigits(number);

        assertArrayEquals(expected,result);
    }

    @Test
    void arrayDigitsTestTres() {
        int number = 123004000;
        int [] expected = {1,2,3,0,0,4,0,0,0};

        int [] result = ExercicioDois.arrayDigits(number);

        assertArrayEquals(expected,result);
    }
}