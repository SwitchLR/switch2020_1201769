package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioOnzeTest {

    @Test
    void produtoEscalarDoisArraysTest() {
        int[] array1 = {1, 2, 3};
        int[] array2 = {4, 5, 6};
        int expected = 32;

        int result = ExercicioOnze.produtoEscalarDoisArrays(array1, array2);

        assertEquals(expected,result);
    }
    @Test
    void produtoEscalarDoisArraysTestDois() {
        int[] array1 = {1, 2, 3,4};
        int[] array2 = {4, 5, 6};
        int expected = 32;

        int result = ExercicioOnze.produtoEscalarDoisArrays(array1, array2);

        assertEquals(expected,result);
    }

    @Test
    void produtoEscalarDoisArraysTestTres() {
        int[] array1 = {1, 2, 3};
        int[] array2 = {4, 5, 6,7};
        int expected = 32;

        int result = ExercicioOnze.produtoEscalarDoisArrays(array1, array2);

        assertEquals(expected,result);
    }
}