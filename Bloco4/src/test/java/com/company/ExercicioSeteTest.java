package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioSeteTest {

    @Test
    void getMultipleOfThreeTest() {
        int n = 10;
        int m = 4;
        int[]expected = {6,9};

        int [] result = ExercicioSete.getMultipleOfThree(n,m);

        assertArrayEquals(expected,result);
    }

    @Test
    void getMultipleOfThreeTestTwo() {
        int n = 10;
        int m = 11;
        int[]expected = {};

        int [] result = ExercicioSete.getMultipleOfThree(n,m);

        assertArrayEquals(expected,result);
    }
}