package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AvalicaoFormativaTest {

    @Test
    void correspondentes() {
        String p1 = "mare";
        String p2 = "maresia";

        int expected = 4;

        int result = AvalicaoFormativa.verificaCorrespondentes(p1, p2);

        assertEquals(expected, result);

    }

    @Test
    void transposicaoP1() {
        String p1 = "mare";
        String p2 = "maresia";

        char[] expected = {'m', 'a', 'r', 'e'};

        char[] result = AvalicaoFormativa.transposicoes(p1, p2);

        assertArrayEquals(expected, result);

    }
    @Test
    void contatransposicao() {
        char[] transposicao1 = {'m', 'a', 'r', 'e'};
        char[] transposicao2 = {'m', 'a', 'r', 'e'};;

        int expected = 0;

        int result = AvalicaoFormativa.contaTransposições(transposicao1, transposicao2);

        assertEquals(expected, result);

    }
    @Test
    void grauSemelhançaPalavras() {
        String p1 = "mare";
        String p2 = "maresia";

        double expected = 0.726;

        double result = AvalicaoFormativa.grauSemelhancaPalavras(p1,p2);

        assertEquals(expected, result);

    }


}