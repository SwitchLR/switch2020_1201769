package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezTest {
    //a)
    @Test
    void menorValorArrayTest() {
        int[] array = {4, 2, 3, 4};
        int expected = 2;

        int result = ExercicioDez.menorValorArray(array);

        assertEquals(expected, result);
    }

    @Test
    void menorValorArrayTestDois() {
        int[] array = {4, 2, -3, 4};
        int expected = -3;

        int result = ExercicioDez.menorValorArray(array);

        assertEquals(expected, result);
    }

    @Test
    void menorValorArrayTestTres() {
        int[] array = {-4, 2, -3, 4};
        int expected = -4;

        int result = ExercicioDez.menorValorArray(array);

        assertEquals(expected, result);
    }

    //b)
    @Test
    void maiorValorArrayTestUm() {
        int[] array = {-4, 2, -3, 4};
        int expected = 4;

        int result = ExercicioDez.maiorValorArray(array);

        assertEquals(expected, result);
    }

    @Test
    void maiorValorArrayTestDois() {
        int[] array = {4, 2, -3, 0};
        int expected = 4;

        int result = ExercicioDez.maiorValorArray(array);

        assertEquals(expected, result);
    }

    //c)
    @Test
    void valorMedioArrayTestUm() {
        int[] array = {4, 2, 2, 8};
        double expected = 4.0;

        double result = ExercicioDez.valorMedioArray(array);

        assertEquals(expected, result);
    }

    @Test
    void valorMedioArrayTestDois() {
        int[] array = {4, 2, 3, 8};
        double expected = 4.25;

        double result = ExercicioDez.valorMedioArray(array);

        assertEquals(expected, result);
    }

    //d)
    @Test
    void produtoArrayTestUm() {
        int[] array = {4, 2, 3, 1};
        int expected = 24;

        int result = ExercicioDez.produtoArray(array);

        assertEquals(expected, result);
    }

    @Test
    void produtoArrayTestDois() {
        int[] array = {4, 2, 3, 0};
        int expected = 0;

        int result = ExercicioDez.produtoArray(array);

        assertEquals(expected, result);
    }

    //e)
    @Test
    void encontraValorArrayTest() {
        int[] array = {1, 2, 2, 3, 4, 2};
        int n = 3;
        boolean expected = true;

        boolean result = ExercicioDez.encontraValorArray(n, array);

        assertEquals(expected, result);
    }

    @Test
    void naoRepetidosArrayTest() {
        int[] array = {1, 2, 2, 3, 2, 1, 3, 2, 1, 1, 3};
        int[] expected = {1, 2, 3};

        int[] result = ExercicioDez.naoRepetidosArray(array);

        assertArrayEquals(expected, result);
    }

    @Test
    void naoRepetidosArrayTestDois() {
        int[] array = {1, 0, 0, 2, 1, 9};
        int[] expected = {1, 2, 9, 0};

        int[] result = ExercicioDez.naoRepetidosArray(array);

        assertArrayEquals(expected, result);
    }

    @Test
    void naoRepetidosArrayTestTres() {
        int[] array = {0, 0, 0, 0, 0};
        int[] expected = {0};

        int[] result = ExercicioDez.naoRepetidosArray(array);

        assertArrayEquals(expected, result);
    }

    @Test
    void naoRepetidosArrayTestQuatro() {
        int[] array = {0, 0, 1, 0, 0};
        int[] expected = {1, 0};

        int[] result = ExercicioDez.naoRepetidosArray(array);

        assertArrayEquals(expected, result);
    }
    //f)já fiz os teste para o metodo pedido no exercicio 9

    // g)
    @Test
    void numeroPrimoTest() {
        int numero = 101;
        boolean expected = true;

        boolean result = ExercicioDez.numeroPrimo(numero);

        assertEquals(expected, result);
    }

    @Test
    void numeroPrimoTestDois() {
        int numero = 24;
        boolean expected = false;

        boolean result = ExercicioDez.numeroPrimo(numero);

        assertEquals(expected, result);
    }

    @Test
    void verificaPrimosArrayTestUm() {
        int[] array = {1, 2, 3, 4, 5};
        int[] expected = {2, 3, 5};

        int[] result = ExercicioDez.verificaPrimosArray(array);

        assertArrayEquals(expected, result);
    }

    @Test
    void verificaPrimosArrayTestDois() {
        int[] array = {1, 0, 4, 6, 5, 31};
        int[] expected = {5, 31};

        int[] result = ExercicioDez.verificaPrimosArray(array);

        assertArrayEquals(expected, result);
    }

    @Test
    void verificaPrimosArrayTestTres() {
        int[] array = {1, 0, 2, 2, 4, 6, 6, 31,31};
        int[] expected = {2, 31};

        int[] result = ExercicioDez.verificaPrimosArray(array);

        assertArrayEquals(expected, result);
    }

}