package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioQuatroTest {

    @Test
    void getEvenArrayTest() {
        int[] sequence = {1, 22, 3, 4};
        int[] expected = {22, 4};

        int[] result = ExercicioQuatro.getEvenArray(sequence);

        assertArrayEquals(expected, result);
    }

    @Test
    void getEvenArrayTestDois() {
        int[] sequence = {1, 0, 3, 4};
        int[] expected = {0, 4};

        int[] result = ExercicioQuatro.getEvenArray(sequence);

        assertArrayEquals(expected, result);
    }

    @Test
    void getEvenArrayTestTres() {
        int[] sequence = {1, 0, 300, 4, 67, 26};
        int[] expected = {0, 300, 4, 26};

        int[] result = ExercicioQuatro.getEvenArray(sequence);

        assertArrayEquals(expected, result);
    }
}