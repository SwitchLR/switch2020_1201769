package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDozeTest {

    @Test
    void confirmaNumeroColunas() {
        int [][] array2D = {
                {1,2,3},
                {4,5,6},
                {7,8,9},
        };
        int expected = 3;

        int result = ExercicioDoze.confirmaNumeroColunas(array2D);

        assertEquals(expected,result);
    }

    @Test
    void confirmaNumeroColunasDois() {
        int [][] array2D = {
                {1,2,3},
                {4,5,6},
                {7,8,9,2},
        };
        int expected = -1;

        int result = ExercicioDoze.confirmaNumeroColunas(array2D);

        assertEquals(expected,result);
    }
    @Test
    void confirmaNumeroColunasTres() {
        int [][] array2D = {
                {},
                {},
                {},
        };
        int expected = 0;

        int result = ExercicioDoze.confirmaNumeroColunas(array2D);

        assertEquals(expected,result);
    }
}