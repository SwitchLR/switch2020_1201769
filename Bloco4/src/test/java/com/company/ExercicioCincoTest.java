package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioCincoTest {

    @Test
    void getEvenSumTest() {
        int number = 123456;
        int exepected = 12;

        int result = ExercicioCinco.getEvenSum(number);

        assertEquals(exepected,result);
    }

    @Test
    void getEvenSumTestDois() {
        int number = 4261379;
        int exepected = 12;

        int result = ExercicioCinco.getEvenSum(number);

        assertEquals(exepected,result);
    }

    @Test
    void getEvenSumTestTres() {
        int number = 95461742;
        int exepected = 16;

        int result = ExercicioCinco.getEvenSum(number);

        assertEquals(exepected,result);
    }

    @Test
    void getEvenSumTestQuatro() {
        int number = 0;
        int exepected = 0;

        int result = ExercicioCinco.getEvenSum(number);

        assertEquals(exepected,result);
    }

}