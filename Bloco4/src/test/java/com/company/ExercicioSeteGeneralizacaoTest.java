package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioSeteGeneralizacaoTest {

    @Test
    void getMultipleOfNumberTest() {
        int n = 10;
        int m = 4;
        int multiples = 2;
        int[] expected = {4, 6, 8};

        int[] result = ExercicioSeteGeneralizacao.getMultipleOfNumber(n, m, multiples);

        assertArrayEquals(expected, result);
    }

    @Test
    void getMultipleOfNumberTestTwo() {
        int n = 20;
        int m = 0;
        int multiples = 5;
        int[] expected = {0, 5, 10, 15};

        int[] result = ExercicioSeteGeneralizacao.getMultipleOfNumber(n, m, multiples);

        assertArrayEquals(expected, result);
    }

}