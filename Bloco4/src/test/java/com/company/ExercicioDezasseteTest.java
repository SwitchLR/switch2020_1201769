package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezasseteTest {
    //a)
    @Test
    void calculaMatrizPorConstanteTesteParaMatrizQuadrada() {
        int[][] array2D = {
                {1, 2, 3},
                {3, 1, 4},
                {0, 0, 5},
        };
        int constante = 3;

        int[][] expected = {
                {3, 6, 9},
                {9, 3, 12},
                {0, 0, 15},
        };
        int[][] result = ExercicioDezassete.calculaMatrizPorConstante(array2D, constante);

        assertArrayEquals(expected, result);
    }

    @Test
    void calculaMatrizPorConstanteTesteParaMatrizRectangularLarguraMaiorComprimento() {
        int[][] array2D = {
                {1, 2, 3},
                {3, 1, 4},
                //{0, 0, 5},
        };
        int constante = 3;

        int[][] expected = {
                {3, 6, 9},
                {9, 3, 12},
                //{0, 0, 15},
        };
        int[][] result = ExercicioDezassete.calculaMatrizPorConstante(array2D, constante);

        assertArrayEquals(expected, result);
    }

    @Test
    void calculaMatrizPorConstanteTesteParaMatrizRectangularComprimentoMaiorLargura() {
        int[][] array2D = {
                {1, 2},
                {3, 1},
                {0, 0},
        };
        int constante = 3;

        int[][] expected = {
                {3, 6},
                {9, 3},
                {0, 0},
        };
        int[][] result = ExercicioDezassete.calculaMatrizPorConstante(array2D, constante);

        assertArrayEquals(expected, result);
    }

    @Test
    void calculaMatrizPorConstanteTesteParaConstanteZero() {
        int[][] array2D = {
                {1, 2, 3},
                {3, 1, 4},
                {0, 0, 5},
        };
        int constante = 0;

        int[][] expected = {
                {0, 0, 0},
                {0, 0, 0},
                {0, 0, 0},
        };
        int[][] result = ExercicioDezassete.calculaMatrizPorConstante(array2D, constante);

        assertArrayEquals(expected, result);
    }

    @Test
    void calculaMatrizPorConstanteTesteParaMatrizVazia() {
        int[][] array2D = {
                {},
        };
        int constante = 3;

        int[][] expected = {
                {},
        };
        int[][] result = ExercicioDezassete.calculaMatrizPorConstante(array2D, constante);

        assertArrayEquals(expected, result);
    }

    //b)
    @Test
    void criaMaiorMatrizPossivelComZerosQuadradas() {
        int[][] array2DA = {
                {1, 2, 3},
                {3, 1, 4},
                {0, 0, 5},
        };
        int[][] array2DB = {
                {1, 2, 3},
                {3, 1, 4},
                {0, 0, 5},
        };

        int[][] expected = {
                {0, 0, 0},
                {0, 0, 0},
                {0, 0, 0},
        };

        int[][] result = ExercicioDezassete.criaMaiorMatrizPossivel(array2DA, array2DB);

        assertArrayEquals(expected, result);
    }

    @Test
    void criaMaiorMatrizPossivelComZerosQuadradoRectanguloLadoColuna() {
        int[][] array2DA = {
                {1, 2, 3},
                {3, 1, 4},
                {0, 0, 5},
        };
        int[][] array2DB = {
                {1, 2, 3},
                {3, 1, 4},
                //       {0, 0, 5},
        };

        int[][] expected = {
                {0, 0, 0},
                {0, 0, 0},
                {0, 0, 0},
        };

        int[][] result = ExercicioDezassete.criaMaiorMatrizPossivel(array2DA, array2DB);

        assertArrayEquals(expected, result);
    }


    //b)

    @Test
    void calculaAdicaoMatrizes_2x2() {
        int[][] array2DA = {
                {1, 2},
                {3, 4},
                // {5, 6},
        };
        int[][] array2DB = {
                {1, 2},
                {4, 5},
                //{5, 6},
        };

        int[][] expected = {
                {2, 4},
                {7, 9},
                //{29, 40, 51},
        };

        int[][] result = ExercicioDezassete.calculaAdicaoMatrizes(array2DA, array2DB);

        assertArrayEquals(expected, result);
    }

    @Test
    void calculaAdicaoMatrizes_3x3() {
        int[][] array2DA = {
                {1, 2, 1},
                {3, 4, 0},
                {5, 6, 2},
        };
        int[][] array2DB = {
                {1, 2, 3},
                {4, 5, 2},
                {5, 6, 1},
        };

        int[][] expected = {
                {2, 4, 4},
                {7, 9, 2},
                {10, 12, 3},
        };

        int[][] result = ExercicioDezassete.calculaAdicaoMatrizes(array2DA, array2DB);

        assertArrayEquals(expected, result);
    }

    @Test
    void calculaAdicaoMatrizes_tamanhosDiferentes() {
        int[][] array2DA = {
                {1, 2},
                {3, 4},
                // {5, 6},
        };
        int[][] array2DB = {
                {1, 2, 3},
                {4, 5, 4},
                //{5, 6},
        };

        int[][] expected = null;

        int[][] result = ExercicioDezassete.calculaAdicaoMatrizes(array2DA,array2DB);

        assertNull(result);
    }


    //c)
    @Test
    void calculaProdutoMatrizes_3x2_2x3() {
        int[][] array2DA = {
                {1, 2},
                {3, 4},
                {5, 6},
        };
        int[][] array2DB = {
                {1, 2, 3},
                {4, 5, 6},
                //{5, 6},
        };

        int[][] expected = {
                {9, 12, 15},
                {19, 26, 33},
                {29, 40, 51},
        };

        int[][] result = ExercicioDezassete.calculaProdutoMatrizes(array2DA, array2DB);

        assertArrayEquals(expected, result);
    }

    @Test
    void calculaProdutoMatrizes_2x3_3_2() {
        int[][] array2DA = {
                {1, 2, 5},
                {3, 4, 6},
                //{5, 6},
        };
        int[][] array2DB = {
                {1, 2,},
                {4, 5,},
                {5, 6},
        };

        int[][] expected = {
                {34, 42},
                {49, 62},
                // {29, 40, 51},
        };

        int[][] result = ExercicioDezassete.calculaProdutoMatrizes(array2DA, array2DB);

        assertArrayEquals(expected, result);
    }

    @Test
    void calculaProdutoMatrizes_3x3_3x3() {
        int[][] array2DA = {
                {1, 2, 5},
                {3, 4, 6},
                {5, 6, 7},
        };
        int[][] array2DB = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9},
        };

        int[][] expected = {
                {44, 52, 60},
                {61, 74, 87},
                {78, 96, 114},
        };

        int[][] result = ExercicioDezassete.calculaProdutoMatrizes(array2DA, array2DB);

        assertArrayEquals(expected, result);
    }

    @Test
    void calculaProdutoMatrizes_3x2_2x4() {
        int[][] array2DA = {
                {1, 2},
                {3, 4},
                {5, 6},
        };
        int[][] array2DB = {
                {1, 2, 3, 1},
                {4, 5, 6, 2},
                //{7, 8, 9},
        };

        int[][] expected = {
                {9, 12, 15, 5},
                {19, 26, 33, 11},
                {29, 40, 51, 17},
        };

        int[][] result = ExercicioDezassete.calculaProdutoMatrizes(array2DA, array2DB);

        assertArrayEquals(expected, result);
    }

    @Test
    void calculaProdutoMatrizesComColunasADiferenteLinhasB() {
        int[][] array2DA = {
                {1, 2, 5},
                {3, 4, 6},
                {5, 6, 7},
        };
        int[][] array2DB = {
                {1, 2, 3},
                {4, 5, 6},
                //{7, 8, 9},
        };

        int[][] expected = null;

        int[][] result = ExercicioDezassete.calculaProdutoMatrizes(array2DA, array2DB);

        assertNull(result);
    }
}