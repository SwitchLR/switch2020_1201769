package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioAulaTresNovembroTest {

    @Test
    void creatCopy() {
        int[]original = {3,5,7};
        int[]expected = {3,5};

        int[] result= ExercicioAulaTresNovembro.creatCopy(original,2);

        assertArrayEquals(expected,result);
        assertEquals(expected.length,result.length);
        assertNotSame(result,original);
    }
}