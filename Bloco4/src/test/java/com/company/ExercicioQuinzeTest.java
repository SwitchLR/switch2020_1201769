package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioQuinzeTest {
    // a)
    @Test
    void procuraMenorValorMatrizTeste() {
        int[][] array2D = {
                {1, 2, 3, 5},
                {4, 5, 6, 5},
                {7, 8, 9, 5},
        };
        int expected = 1;

        int result = ExercicioQuinze.procuraMenorValorMatriz(array2D);

        assertEquals(expected, result);
    }

    @Test
    void procuraMenorValorMatrizTesteDois() {
        int[][] array2D = {
                {7, 5, 3, 5},
                {4, 5, 2, 5},
                {7, 8, 9, 5},
        };
        int expected = 2;

        int result = ExercicioQuinze.procuraMenorValorMatriz(array2D);

        assertEquals(expected, result);
    }

    @Test
    void procuraMenorValorMatrizTesteTres() {
        int[][] array2D = {
                {7, 8, 3, 5},
                {4, 0, 2,},
        };
        int expected = 0;

        int result = ExercicioQuinze.procuraMenorValorMatriz(array2D);

        assertEquals(expected, result);
    }

    // b)
    @Test
    void procuraMaiorValorMatrizTesteUm() {
        int[][] array2D = {
                {7, 8, 3, 5},
                {4, 0, 2,},
        };
        int expected = 8;

        int result = ExercicioQuinze.procuraMaiorValorMatriz(array2D);

        assertEquals(expected, result);
    }

    @Test
    void procuraMaiorValorMatrizTesteDois() {
        int[][] array2D = {
                {-7, -8, -3, -5},
                {-4, 0, -2,},
        };
        int expected = 0;

        int result = ExercicioQuinze.procuraMaiorValorMatriz(array2D);

        assertEquals(expected, result);
    }

    @Test
    void procuraMaiorValorMatrizTesteTres() {
        int[][] array2D = {
                {79, 78, 3, 5},
                {14, 0, 2,},
        };
        int expected = 79;

        int result = ExercicioQuinze.procuraMaiorValorMatriz(array2D);

        assertEquals(expected, result);
    }

    // c)
    @Test
    void calculaValorMedioMatrizTesteUm() {
        int[][] array2D = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9},
        };
        double expected = 5.0;

        double result = ExercicioQuinze.calculaValorMedioMatriz(array2D);

        assertEquals(expected, result);
    }

    @Test
    void calculaValorMedioMatrizTesteDois() {
        int[][] array2D = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 0},
        };
        double expected = 4.0;

        double result = ExercicioQuinze.calculaValorMedioMatriz(array2D);

        assertEquals(expected, result);
    }

    @Test
    void calculaValorMedioMatrizTesteTres() {
        int[][] array2D = {
                {0, 0, 0},
                {0, 0, 0},
                {0, 0, 9},
        };
        double expected = 1.0;

        double result = ExercicioQuinze.calculaValorMedioMatriz(array2D);

        assertEquals(expected, result);
    }

    // d)
    @Test
    void calculaProdutoMatrizTesteUm() {
        int[][] array2D = {
                {0, 0, 0},
                {0, 0, 0},
                {0, 0, 9},
        };
        int expected = 0;

        int result = ExercicioQuinze.calculaProdutoMatriz(array2D);

        assertEquals(expected, result);
    }

    @Test
    void calculaProdutoMatrizTesteDois() {
        int[][] array2D = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 0},
        };
        int expected = 0;

        int result = ExercicioQuinze.calculaProdutoMatriz(array2D);

        assertEquals(expected, result);
    }

    @Test
    void calculaProdutoMatrizTesteTres() {
        int[][] array2D = {
                {1, 2, 3, 2},
                {4, 5},
                {7, 8, 9},
        };
        int expected = 120960;

        int result = ExercicioQuinze.calculaProdutoMatriz(array2D);

        assertEquals(expected, result);
    }

    // e)
    @Test
    void calculaTamanhoMatrizTesteUm() {
        int[][] array2D = {
                {1, 2, 3, 2},
                {4, 5},
                {7, 8, 9},
        };
        int expected = 9;

        int result = ExercicioQuinze.calculaTamanhoMatriz(array2D);

        assertEquals(expected, result);
    }

    @Test
    void calculaTamanhoMatrizTesteDois() {
        int[][] array2D = {
                {1, 2, 3, 4},
                {4, 5},
                {7, 8},
        };
        int expected = 8;

        int result = ExercicioQuinze.calculaTamanhoMatriz(array2D);

        assertEquals(expected, result);
    }

    @Test
    void conjuntosNaoRepetidosMatrizTesteUm() {
        int[][] array2D = {
                {1, 2, 2},
                {4, 3},
                //{5, 3},
        };
        int[] expected = {1, 2, 4, 3};

        int[] result = ExercicioQuinze.conjuntosNaoRepetidosMatriz(array2D);

        assertArrayEquals(expected, result);
    }

    @Test
    void conjuntosNaoRepetidosMatrizTesteDois() {
        int[][] array2D = {
                {0, 2, 2},
                {4, 3},
                {5, 3},
        };
        int[] expected = {2, 4, 3, 5, 0};

        int[] result = ExercicioQuinze.conjuntosNaoRepetidosMatriz(array2D);

        assertArrayEquals(expected, result);
    }

    @Test
    void conjuntosNaoRepetidosMatrizTesteTres() {
        int[][] array2D = {
                {0, 2, 2},
                {0, 3},
                {5, 0},
        };
        int[] expected = {2, 3, 5, 0};

        int[] result = ExercicioQuinze.conjuntosNaoRepetidosMatriz(array2D);

        assertArrayEquals(expected, result);
    }

    //f)
    @Test
    void conjuntosPrimosMatrizTesteUm() {
        int[][] array2D = {
                {1, 2, 2},
                {4, 3},
                {5, 3},
        };
        int[] expected = {2, 3, 5};

        int[] result = ExercicioQuinze.conjuntoPrimosMatriz(array2D);

        assertArrayEquals(expected, result);
    }

    @Test
    void conjuntosPrimosMatrizTesteDois() {
        int[][] array2D = {
                {1, 0, 2},
                {},
                {5, 3, 31},
                {5, 17, 3, 2}
        };
        int[] expected = {2, 5, 3, 31, 17};

        int[] result = ExercicioQuinze.conjuntoPrimosMatriz(array2D);

        assertArrayEquals(expected, result);
    }

    //g)
    @Test
    void conjuntoDiagonalPrincipalMatrizTesteUm() {
        int[][] array2D = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9},
        };
        int[] expected = {1, 5, 9};

        int[] result = ExercicioQuinze.conjuntoDiagonalPrincipalMatriz(array2D);

        assertArrayEquals(expected, result);

    }

    @Test
    void conjuntoDiagonalPrincipalMatrizTesteDois() {
        int[][] array2D = {
                {1, 2},
                {4, 5},
                {7, 8},
        };
        int[] expected = {1, 5};

        int[] result = ExercicioQuinze.conjuntoDiagonalPrincipalMatriz(array2D);

        assertArrayEquals(expected, result);

    }

    @Test
    void conjuntoDiagonalPrincipalMatrizTesteTres() {
        int[][] array2D = {
                {1, 2, 3, 11},
                {4, 5, 6, 12},
                {7, 8, 9, 13},
        };
        int[] expected = {1, 5, 9};

        int[] result = ExercicioQuinze.conjuntoDiagonalPrincipalMatriz(array2D);

        assertArrayEquals(expected, result);
    }

    @Test
    void conjuntoDiagonalPrincipalMatrizTesteQuatro() {
        int[][] array2D = {
                {1, 2, 3, 11},
                {4, 5, 6},
                {7, 8, 9, 13},
        };
        int[] expected = null;

        int[] result = ExercicioQuinze.conjuntoDiagonalPrincipalMatriz(array2D);

        assertArrayEquals(expected, result);
    }

    @Test
    void conjuntoDiagonalPrincipalMatrizTesteCinco() {
        int[][] array2D = {
                {1},
                {4},
                {7},
        };
        int[] expected = {1};

        int[] result = ExercicioQuinze.conjuntoDiagonalPrincipalMatriz(array2D);

        assertArrayEquals(expected, result);
    }

    //h)
    @Test
    void conjuntoDiagonalSecundariaTesteUm() {
        int[][] array2D = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9},
        };
        int[] expected = {3, 5, 7};

        int[] result = ExercicioQuinze.conjuntoDiagonalSecundariaMatriz(array2D);

        assertArrayEquals(expected, result);
    }

    @Test
    void conjuntoDiagonalSecundariaTesteDois() {
        int[][] array2D = {
                {1, 2, 3, 10},
                {4, 5, 6, 11},
                {7, 8, 9, 12},
        };
        int[] expected = {10, 6, 8};

        int[] result = ExercicioQuinze.conjuntoDiagonalSecundariaMatriz(array2D);

        assertArrayEquals(expected, result);
    }

    @Test
    void conjuntoDiagonalSecundariaTesteTres() {
        int[][] array2D = {
                {1, 2, 3, 10},
                {4, 5, 6, 11},
                //{7, 8, 9, 12},
        };
        int[] expected = {10, 6};

        int[] result = ExercicioQuinze.conjuntoDiagonalSecundariaMatriz(array2D);

        assertArrayEquals(expected, result);
    }

    @Test
    void conjuntoDiagonalSecundariaTesteQuatro() {
        int[][] array2D = {
                {1, 2, 3, 10},
                {4, 5, 6,},
                {7, 8, 9, 12},
        };
        int[] expected = null;

        int[] result = ExercicioQuinze.conjuntoDiagonalSecundariaMatriz(array2D);

        assertNull(result);
    }

    //i)
    @Test
    void verificaMatrizIdentidadeTesteUm() {
        int[][] array2D = {
                {1, 0, 0},
                {0, 5, 0},
                {0, 0, 7},
        };
        boolean expected = true;

        boolean result = ExercicioQuinze.verificaMatrizIdentidade(array2D);

        assertTrue(result);
    }

    @Test
    void verificaMatrizIdentidadeTesteDois() {
        int[][] array2D = {
                {0, 0, 0},
                {0, 0, 0},
                {0, 0, 0},
        };
        boolean expected = true;

        boolean result = ExercicioQuinze.verificaMatrizIdentidade(array2D);

        assertEquals(expected, result);
    }

    @Test
    void verificaMatrizIdentidadeTesteTres() {
        int[][] array2D = {
                {1, 0, 0},
                {0, 5, 2},
                {0, 0, 9},
        };
        boolean expected = false;

        boolean result = ExercicioQuinze.verificaMatrizIdentidade(array2D);

        assertTrue(result);
    }

    @Test
    void verificaMatrizIdentidadeTesteQuatro() {
        int[][] array2D = {
                {1, 0, 0},
                {0, 5, 2, 9},
                {0, 0, 9},
        };
        boolean expected = false;

        boolean result = ExercicioQuinze.verificaMatrizIdentidade(array2D);

        assertTrue(result);
    }

    //k)
    @Test
    void criaMatrizTranspostaTesteUm() {
        int[][] array2D = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9},
        };
        int[][] expected = {
                {1, 4, 7},
                {2, 5, 8},
                {3, 6, 9},
        };

        int[][] result = ExercicioQuinze.criaMatrizTransposta(array2D);

        assertArrayEquals(expected, result);
    }

    @Test
    void criaMatrizTranspostaTesteDois() {
        int[][] array2D = {
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                //{7, 8, 9},
        };
        int[][] expected = {
                {1, 5},
                {2, 6},
                {3, 7},
                {4, 8},
        };

        int[][] result = ExercicioQuinze.criaMatrizTransposta(array2D);

        assertArrayEquals(expected, result);
    }

    @Test
    void criaMatrizTranspostaTesteTres() {
        int[][] array2D = {
                {1, 2},
                {3, 4},
                {5, 6},
                {7, 8},
        };
        int[][] expected = {
                {1, 3, 5, 7},
                {2, 4, 6, 8},
                //{7, 8, 9},
        };

        int[][] result = ExercicioQuinze.criaMatrizTransposta(array2D);

        assertArrayEquals(expected, result);
    }

    //j)
    @Test
    void removeLinhaMatrizTesteUm() {
        int[][] array2D = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9},
        };
        int linhaRemovida = 1;

        int[][] expected = {
                {1, 2, 3},
                {7, 8, 9},
                //{3, 6, 9},
        };

        int[][] result = ExercicioQuinze.removeLinhaMatrizQuadrada(array2D, linhaRemovida);

        assertArrayEquals(expected, result);
    }

    @Test
    void removeLinhaMatrizTesteDois() {
        int[][] array2D = {
                {1, 2, 3},
                {4, 5, 6},
        };
        int linhaRemovida = 1;

        int[][] expected = {
                {1, 2, 3},
        };
        int[][] result = ExercicioQuinze.removeLinhaMatrizQuadrada(array2D, linhaRemovida);

        assertArrayEquals(expected, result);
    }

    @Test
    void removeColunaMatrizTesteUm() {
        int[][] array2D = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9},
        };
        int colunaRemovida = 1;

        int[][] expected = {
                {1, 3},
                {4, 6},
                {7, 9},
        };

        int[][] result = ExercicioQuinze.removeColunaMatrizQuadrada(array2D, colunaRemovida);

        assertArrayEquals(expected, result);
    }


    @Test
    void removeColunaMatrizTesteDois() {
        int[][] array2D = {
                {1, 3},
                {4, 6},
                {7, 9},
        };
        int colunaRemovida = 1;

        int[][] expected = {
                {1},
                {4},
                {7},
        };
        int[][] result = ExercicioQuinze.removeColunaMatrizQuadrada(array2D, colunaRemovida);

        assertArrayEquals(expected, result);
    }

    @Test
    void removeColunaMatrizTesteTres() {
        int[][] array2D = {
                {1, 2, 3, 1},
                {4, 5, 6, 0},
                {7, 8, 9, 2},
                {1, 2, 3, 4}
        };
        int colunaRemovida = 2;

        int[][] expected = {
                {1, 2, 1},
                {4, 5, 0},
                {7, 8, 2},
                {1, 2, 4}
        };

        int[][] result = ExercicioQuinze.removeColunaMatrizQuadrada(array2D, colunaRemovida);

        assertArrayEquals(expected, result);
    }

    @Test
    void removeLinhaEColunaMatrizTeste() {
        int[][] array2D = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9},
        };
        int linhaRemovida = 1;
        int colunaRemovida = 1;

        int[][] expected = {
                {1, 3},
                {7, 9},
        };

        int[][] result = ExercicioQuinze.removeLinhaEColunaMatriz(array2D, linhaRemovida, colunaRemovida);

        assertArrayEquals(expected, result);
    }

    @Test
    void criaMatrizCofactoresTeste() {
        int[][] array2D = {
                {1, 2, 1},
                {2, 1, 1},
                {1, 1, 2},
        };

        int[][] expected = {
                {1, -3, 1},
                {-3, 1, 1},
                {1, 1, -3},
        };

        int[][] result = ExercicioQuinze.criaMatrizCofactores(array2D);

        assertArrayEquals(expected, result);
    }
    @Test
    void criaMatrizCofactoresTesteDois() {
        int[][] array2D = {
                {0,1,0},
                {1,2,1},
                {1,0,2},
        };

        int[][] expected = {
                {4,-1,-2},
                {-2,0,1},
                {1,0,-1},
        };

        int[][] result = ExercicioQuinze.criaMatrizCofactores(array2D);

        assertArrayEquals(expected, result);
    }

    @Test
    void criaMatrizCofactoresTesteTresMatrizNaoQuadrada() {
        int[][] array2D = {
                {0,1,0},
                {1,2,1},
               // {1,0,2},
        };

        int[][] expected = null;

        int[][] result = ExercicioQuinze.criaMatrizCofactores(array2D);

        assertNull(result);
    }

    @Test
    void criaMatrizInversaTesteUm() {
        int[][] array2D = {
                {0,1,0},
                {1,2,1},
                {1,0,2},
        };

        int[][] expected = {
                {-4,2,-1},
                {1,0,0},
                {2,-1,1},
        };

        int[][] result = ExercicioQuinze.criaMatrizInversa(array2D);

        assertArrayEquals(expected, result);
    }

    @Test
    void criaMatrizInversaTesteDois() {
        int[][] array2D = {
                {1,2,1},
                {0,1,0},
                {1,2,0},
        };

        int[][] expected = {
                {-4,2,-1},
                {1,0,0},
                {2,-1,1},
        };

        int[][] result = ExercicioQuinze.criaMatrizInversa(array2D);

        assertArrayEquals(expected, result);
    }
}