package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioTrezeTest {

    @Test
    void confirmaNumeroLinhasTesteUm() {
        int [][] array2D = {
                {1,2,3},
                {4,5,6},
                {7,8,9,5},
        };

        boolean expected= false;

        boolean result = ExercicioTreze.verificaMatrizQuadrada(array2D);

        assertEquals(expected,result);
    }
    @Test
    void confirmaNumeroLinhasTesteDois() {
        int [][] array2D = {
                {1,2,3},
                {4,5,6},
                {7,8,9},
        };

        boolean expected= true;

        boolean result = ExercicioTreze.verificaMatrizQuadrada(array2D);

        assertEquals(expected,result);
    }
}