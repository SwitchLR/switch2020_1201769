package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioSeisTest {

    @Test
    void getFirstNNumbersTest() {
        int[] arraySequence = {1,2,3,4,5};
        int n = 3;
        int[] expected = {1,2,3};

        int[] result = ExercicioSeis.getFirstNNumbers(arraySequence,n);

        assertArrayEquals(expected,result);
    }

    @Test
    void getFirstNNumbersTestTwo() {
        int[] arraySequence = {0,1,0,3,0,5,2};
        int n = 5;
        int[] expected = {0,1,0,3,0};

        int[] result = ExercicioSeis.getFirstNNumbers(arraySequence,n);

        assertArrayEquals(expected,result);
    }
}