package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezasseisTest {

    @Test
    void determinanteLaPlaceTesteUm() {
        int[][] array2D = {
                {1, 2, 3},
                {3, 1, 4},
                {0, 0, 5},
        };
        int expected = -25;

        int result = ExercicioDezasseis.determinanteLaPlace(array2D);

        assertEquals(expected, result);
    }

    @Test
    void determinanteLaPlaceTesteDois() {
        int[][] matriz = {
                {1, 2, 3, 1},
                {3, 1, 4, 0},
                {0, 0, 5, 2},
                {0, 0, 1, 0},
        };
        int expected = 10;

        int result = ExercicioDezasseis.determinanteLaPlace(matriz);
        assertEquals(expected, result);
    }

    @Test
    void determinanteLaPlaceTesteTres() {
        int[][] matriz = {
                {1, 2, 3, 1, 1},
                {3, 1, 4, 0, 2},
                {0, 0, 5, 2, 0},
                {0, 0, 1, 0, 1},
                {0, 0, 1, 0, 1},
        };
        int expected = 0;

        int result = ExercicioDezasseis.determinanteLaPlace(matriz);
        assertEquals(expected, result);
    }
    @Test
    void determinanteLaPlaceTesteQuatro() {
        int[][] matriz = {
                {1, 2, 3, 1, 1},
                {3, 1, 4, 0, 1},
                {0, 2, 5, 2, 0},
                {1, 1, 1, 0, 1},
                {0, 2, 1, 3, 1},
        };
        int expected = 4;

        int result = ExercicioDezasseis.determinanteLaPlace(matriz);
        assertEquals(expected, result);
    }
}