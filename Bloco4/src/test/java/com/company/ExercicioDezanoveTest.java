package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezanoveTest {

    @Test
    void verificaSeEstaPreenchidaTesteUm() { //o jogo não está preenchido
        int[][] sudoku = {
                {8, 0, 0,  0, 0, 0,  0, 0, 0},
                {0, 0, 3,  6, 0, 0,  0, 0, 0},
                {0, 7, 0,  0, 9, 0,  2, 0, 0},

                {0, 5, 0,  0, 0, 7,  0, 0, 0},
                {0, 0, 0,  0, 4, 5,  7, 0, 0},
                {0, 0, 0,  1, 0, 0,  0, 3, 0},

                {0, 0, 1,  0, 0, 0,  0, 6, 8},
                {0, 0, 8,  5, 0, 0,  0, 1, 0},
                {0, 9, 0,  0, 0, 0,  4, 0, 0}
        };

        boolean expected = false;

        boolean result = ExercicioDezanove.verificaSeEstaPreenchida(sudoku);

        assertEquals(expected, result);
    }

    @Test
    void verificaSeEstaPreenchidaTesteDois() {  // o jogo está preenchido
        int[][] sudoku = {
                {8, 1, 2, 1, 1, 1, 1, 1, 1},
                {8, 1, 2, 1, 1, 1, 1, 1, 1},
                {8, 1, 2, 1, 1, 1, 1, 1, 1},

                {8, 1, 2, 1, 1, 1, 1, 1, 1},
                {8, 1, 2, 1, 1, 1, 1, 1, 1},
                {8, 1, 2, 1, 1, 1, 1, 1, 1},

                {8, 1, 2, 1, 1, 1, 1, 1, 1},
                {8, 1, 2, 1, 1, 1, 1, 1, 1},
                {8, 1, 2, 1, 1, 1, 1, 1, 1}
        };

        boolean expected = true;

        boolean result = ExercicioDezanove.verificaSeEstaPreenchida(sudoku);

        assertEquals(expected, result);
    }

    @Test
    void verificaSeCelulaEstaVazia() { //o jogo não está preenchido
        int[][] sudoku = {
                {8, 0, 0,  0, 0, 0,  0, 0, 0},
                {0, 0, 3,  6, 0, 0,  0, 0, 0},
                {0, 7, 0,  0, 9, 0,  2, 0, 0},

                {0, 5, 0,  0, 0, 7,  0, 0, 0},
                {0, 0, 0,  0, 4, 5,  7, 0, 0},
                {0, 0, 0,  1, 0, 0,  0, 3, 0},

                {0, 0, 1,  0, 0, 0,  0, 6, 8},
                {0, 0, 8,  5, 0, 0,  0, 1, 0},
                {0, 9, 0,  0, 0, 0,  4, 0, 0}
        };

        int x=1;
        int y=1;

        boolean expected = true;

        boolean result = ExercicioDezanove.verificaSeCelulaEstaVazia(sudoku,x,y);

        assertEquals(expected, result);
    }

    @Test
    void criaMatrizMascaraNumeroTestUm() {
        int[][] sudoku = {
                {8, 0, 0,  0, 0, 0,  0, 0, 0},
                {0, 0, 3,  6, 0, 0,  0, 0, 0},
                {0, 7, 0,  0, 9, 0,  2, 0, 0},

                {0, 5, 0,  0, 0, 7,  0, 0, 0},
                {0, 0, 0,  0, 4, 5,  7, 0, 0},
                {0, 0, 0,  1, 0, 0,  0, 3, 0},

                {0, 0, 1,  0, 0, 0,  0, 6, 8},
                {0, 0, 8,  5, 0, 0,  0, 1, 0},
                {0, 9, 0,  0, 0, 0,  4, 0, 0}
        };

        int numero=3;

        int [][] expected = {
                {0, 0, 0,  0, 0, 0,  0, 0, 0},
                {0, 0, 1,  0, 0, 0,  0, 0, 0},
                {0, 0, 0,  0, 0, 0,  0, 0, 0},

                {0, 0, 0,  0, 0, 0,  0, 0, 0},
                {0, 0, 0,  0, 0, 0,  0, 0, 0},
                {0, 0, 0,  0, 0, 0,  0, 1, 0},

                {0, 0, 0,  0, 0, 0,  0, 0, 0},
                {0, 0, 0,  0, 0, 0,  0, 0, 0},
                {0, 0, 0,  0, 0, 0,  0, 0, 0},
        };

        int[][] result = ExercicioDezanove.criaMatrizMascaraNumero(sudoku,numero);

        assertArrayEquals(expected, result);
    }

}