package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioOitoTest {

    @Test
    void getVariousMultiplesInInterval() {
        int n = 4;
        int m = 12;
        int[] multiples = {2, 3};
        int[] expected = {6, 12};

        int[] result = ExercicioOito.getVariousMultiplesInInterval(n, m, multiples);

        assertArrayEquals(expected, result);
    }

    @Test
    void getVariousMultiplesInIntervalTesteDois() {
        int n = 4;
        int m = 12;
        int[] multiples = {2, 3, 4};
        int[] expected = {12};

        int[] result = ExercicioOito.getVariousMultiplesInInterval(n, m, multiples);

        assertArrayEquals(expected, result);
    }

    @Test
    void getVariousMultiplesInIntervalTesteTres() {
        int n = 4;
        int m = 12;
        int[] multiples = {2, 3, 4, 6};
        int[] expected = {12};

        int[] result = ExercicioOito.getVariousMultiplesInInterval(n, m, multiples);

        assertArrayEquals(expected, result);
    }
}