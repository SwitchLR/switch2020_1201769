package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezoitoTest {

    @Test
    void criaMatrizMascara() {
        char[][] array2DA = {
                {'m', 'i', 'm', 'i'},
                {'p', 'a', 'p', 'a'},
                {'m', 'a', 'm', 'a'},
        };
        char letra = 'p';

        int[][] expected = {
                {0, 0, 0, 0},
                {1, 0, 1, 0},
                {0, 0, 0, 0},
        };

        int[][] result = ExercicioDezoito.criaMatrizMascara(array2DA, letra);

        assertArrayEquals(expected, result);
    }


    @Test
    void esquerdaDireitaTesteUm() { // testa se encontra palavra na horizontal
        char[][] array2DA = {
                {'v', 'c', 'm', 'e', 'l'},
                {'m', 'i', 'm', 'i', 'y'},
                {'j', 'j', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };
        String palavra = "jaa";
        boolean expected = true;

        boolean result = ExercicioDezoito.esquerdaDireita(array2DA, palavra);

        assertEquals(expected, result);
    }

    @Test
    void esquerdaDireitaTesteDois() { //testa se não encontra palavra na horizontal
        char[][] array2DA = {
                {'v', 'c', 'm', 'e', 'l'},
                {'m', 'i', 'm', 'a', 'y'},
                {'i', 'j', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };
        String palavra = "mimi";
        boolean expected = false;

        boolean result = ExercicioDezoito.esquerdaDireita(array2DA, palavra);

        assertEquals(expected, result);
    }

    @Test
    void esquerdaDireitaTesteTres() { //testa se encontra palavra "no meio"
        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'a', 'y'},
                {'i', 'j', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };
        String palavra = "jaa";
        boolean expected = true;

        boolean result = ExercicioDezoito.esquerdaDireita(array2DA, palavra);

        assertEquals(expected, result);
    }

    @Test
    void esquerdaDireitaTesteQuatro() { //testa se encontra palavra maior que a matriz
        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'a', 'y'},
                {'i', 'j', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };
        String palavra = "mimayta";
        boolean expected = false;

        boolean result = ExercicioDezoito.esquerdaDireita(array2DA, palavra);

        assertEquals(expected, result);
    }

    @Test
    void cimaBaixoTesteUm() { //testa se encontra palavra de na vertical de cima para baixo
        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'a', 'a', 'y'},
                {'i', 'j', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };
        String palavra = "aba";
        boolean expected = true;

        boolean result = ExercicioDezoito.cimaBaixo(array2DA, palavra);

        assertEquals(expected, result);
    }

    @Test
    void cimaBaixoTesteDois() { //testa se encontra palavra de na vertical de cima para baixo,
        // palavra com demasiado tamanho
        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'a', 'y'},
                {'i', 'j', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };
        String palavra = "cijanl";
        boolean expected = false;

        boolean result = ExercicioDezoito.cimaBaixo(array2DA, palavra);

        assertEquals(expected, result);
    }

    @Test
    void cimaBaixoTesteTres() { //testa se encontra palavra de na vertical de cima para baixo,
        // com parcial da String
        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'a', 'y'},
                {'i', 'j', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };
        String palavra = "ano";
        boolean expected = true;

        boolean result = ExercicioDezoito.cimaBaixo(array2DA, palavra);

        assertEquals(expected, result);
    }


    @Test
    void direitaEsquerdaTesteUm() { //testa se encontra palavra da direita
        // para a esquerda na horizontal
        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'a', 'y'},
                {'i', 'j', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };
        String palavra = "taaji";
        boolean expected = true;

        boolean result = ExercicioDezoito.direitaEsquerda(array2DA, palavra);

        assertEquals(expected, result);
    }

    @Test
    void direitaEsquerdaTesteDois() { //testa se encontra palavra na vertical de
        // cima para baixo
        char[][] array2DA = {
                {'v', 'y', 's', 'e', 'l'},
                {'m', 'i', 't', 'a', 'y'},
                {'i', 'j', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };
        String palavra = "yatim";
        boolean expected = true;

        boolean result = ExercicioDezoito.direitaEsquerda(array2DA, palavra);

        assertEquals(expected, result);
    }

    @Test
    void direitaEsquerdaTesteTres() { //testa se encontra palavra na vertical de
        // cima para baixo e controla os limites
        char[][] array2DA = {
                {'v', 'y', 's', 'e', 'l'},
                {'m', 'i', 't', 'a', 'y'},
                {'i', 'j', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };
        String palavra = "yatimk";
        boolean expected = false;

        boolean result = ExercicioDezoito.direitaEsquerda(array2DA, palavra);

        assertEquals(expected, result);
    }

    @Test
    void baixoCimaTesteUm() { //testa se encontra palavra de cima para baixo

        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'a', 'y'},
                {'i', 'j', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };
        String palavra = "clim";
        boolean expected = true;

        boolean result = ExercicioDezoito.baixoCima(array2DA, palavra);

        assertEquals(expected, result);
    }

    @Test
    void baixoCimaTesteDois() { //testa se encontra palavra de cima para baixo
        //depois de encontrar a primeira e falhar

        char[][] array2DA = {
                {'v', 'c', 'a', 'e', 'l'},
                {'m', 'a', 'm', 'k', 'y'},
                {'i', 'j', 'a', 'b', 't'},
                {'l', 'j', 'b', 'n', 'i'},
                {'c', 'n', 'v', 'o', 'm'},
        };
        String palavra = "ama";
        boolean expected = true;

        boolean result = ExercicioDezoito.baixoCima(array2DA, palavra);

        assertEquals(expected, result);
    }

    @Test
    void baixoCimaTesteTres() { //testa se encontra palavra de cima para baixo e fora dos limites

        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'a', 'y'},
                {'i', 'j', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };
        String palavra = "climvlo";
        boolean expected = false;

        boolean result = ExercicioDezoito.baixoCima(array2DA, palavra);

        assertEquals(expected, result);
    }

    @Test
    void direitaCimaTesteUm() { //testa se encontra palavra de cima para baixo

        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'a', 'y'},
                {'i', 'n', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };
        String palavra = "nba";
        boolean expected = true;

        boolean result = ExercicioDezoito.direitaCima(array2DA, palavra);

        assertEquals(expected, result);
    }

    @Test
    void direitaCimaTesteDois() { //testa se encontra palavra na diagonal direita para cima e
        // controla os limites

        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'a', 'y'},
                {'i', 'n', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };
        String palavra = "nbayt";
        boolean expected = false;

        boolean result = ExercicioDezoito.direitaCima(array2DA, palavra);

        assertEquals(expected, result);
    }

    @Test
    void direitaCimaTesteTres() { //testa se encontra palavra na diagonal direita para cima e
        // controla os limites

        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'a', 'y'},
                {'i', 'n', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };
        String palavra = "caaal";
        boolean expected = true;

        boolean result = ExercicioDezoito.direitaCima(array2DA, palavra);

        assertEquals(expected, result);
    }


    @Test
    void direitaCimaTesteQuatro() { //testa se não encontra palavra na diagonal direita para cima e
        // controla os limites

        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'a', 'y'},
                {'i', 'n', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };
        String palavra = "martim";
        boolean expected = false;

        boolean result = ExercicioDezoito.direitaCima(array2DA, palavra);

        assertEquals(expected, result);
    }

    @Test
    void direitaBaixoTesteUm() { //testa se encontra palavra na diagonal direita para baixo e
        // controla os limites

        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'a', 'y'},
                {'i', 'n', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };
        String palavra = "anm";
        boolean expected = true;

        boolean result = ExercicioDezoito.direitaBaixo(array2DA, palavra);

        assertEquals(expected, result);
    }

    @Test
    void direitaBaixoTesteDois() { //testa se encontra palavra na diagonal direita para baixo e
        // controla os limites

        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'a', 'y'},
                {'i', 'n', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };
        String palavra = "vianm";
        boolean expected = true;

        boolean result = ExercicioDezoito.direitaBaixo(array2DA, palavra);

        assertEquals(expected, result);
    }

    @Test
    void direitaBaixoTesteTres() { //testa se encontra palavra na diagonal direita para baixo e
        // controla os limites

        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'a', 'y'},
                {'i', 'n', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };
        String palavra = "vianmk";
        boolean expected = false;

        boolean result = ExercicioDezoito.direitaBaixo(array2DA, palavra);

        assertEquals(expected, result);
    }

    @Test
    void esquerdaCimaTesteUm() { //testa se encontra palavra na diagonal esquerda para cima e
        // controla os limites

        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'a', 'y'},
                {'i', 'n', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };
        String palavra = "aiv";
        boolean expected = true;

        boolean result = ExercicioDezoito.esquerdaCima(array2DA, palavra);

        assertEquals(expected, result);
    }

    @Test
    void esquerdaCimaTesteDois() { //testa se encontra palavra na diagonal esquerda para cima e
        // controla os limites

        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'a', 'y'},
                {'i', 'n', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };
        String palavra = "mnaiv";
        boolean expected = true;

        boolean result = ExercicioDezoito.esquerdaCima(array2DA, palavra);

        assertEquals(expected, result);
    }

    @Test
    void esquerdaCimaTesteTres() { //testa se encontra palavra na diagonal esquerda para cima e
        // controla os limites

        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'a', 'y'},
                {'i', 'n', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };
        String palavra = "mnaivk";
        boolean expected = false;

        boolean result = ExercicioDezoito.esquerdaCima(array2DA, palavra);

        assertEquals(expected, result);
    }

    @Test
    void esquerdaCimaTesteQuatro() { //testa se encontra palavra na diagonal esquerda para cima e
        // controla os limites

        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'a', 'y'},
                {'i', 'n', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };
        String palavra = "nm";
        boolean expected = true;

        boolean result = ExercicioDezoito.esquerdaCima(array2DA, palavra);

        assertEquals(expected, result);
    }

    @Test
    void esquerdaBaixoTesteUm() { //testa se encontra palavra na diagonal esquerda para baixo e
        // controla os limites

        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'a', 'y'},
                {'i', 'n', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };
        String palavra = "laaac";
        boolean expected = true;

        boolean result = ExercicioDezoito.esquerdaBaixo(array2DA, palavra);

        assertEquals(expected, result);
    }

    @Test
    void esquerdaBaixoTesteDois() { //testa se encontra palavra na diagonal esquerda para baixo e
        // controla os limites

        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'a', 'y'},
                {'i', 'n', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };
        String palavra = "laaack";
        boolean expected = false;

        boolean result = ExercicioDezoito.esquerdaBaixo(array2DA, palavra);

        assertEquals(expected, result);
    }

    @Test
    void esquerdaBaixoTesteTres() { //testa se encontra palavra na diagonal esquerda para baixo e
        // controla os limites

        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'a', 'y'},
                {'i', 'n', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };
        String palavra = "sii";
        boolean expected = true;

        boolean result = ExercicioDezoito.esquerdaBaixo(array2DA, palavra);

        assertEquals(expected, result);
    }

    @Test
    void esquerdaBaixoTesteQuatro() { //testa se encontra palavra na diagonal esquerda para baixo e
        // controla os limites

        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'a', 'y'},
                {'i', 'n', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };
        String palavra = "mimi";
        boolean expected = false;

        boolean result = ExercicioDezoito.esquerdaBaixo(array2DA, palavra);

        assertEquals(expected, result);
    }

    @Test
    void verificaDirecaoTesteEsquerdaDireita() {
        int coordenadaLinhaInicio = 0;
        int coordenadaColunaInicio = 0;
        int coordenadaLinhaFim = 0;
        int coordenadaColunaFim = 3;

        String expected = "daEsquerdaParaDireita";

        String result = ExercicioDezoito.verificaDirecao(coordenadaLinhaInicio, coordenadaColunaInicio, coordenadaLinhaFim, coordenadaColunaFim);

        assertEquals(expected, result);
    }

    @Test
    void verificaDirecaoTesteDireitaEsquerda() {
        int coordenadaLinhaInicio = 0;
        int coordenadaColunaInicio = 4;
        int coordenadaLinhaFim = 0;
        int coordenadaColunaFim = 0;

        String expected = "daDireitaParaEsquerda";

        String result = ExercicioDezoito.verificaDirecao(coordenadaLinhaInicio, coordenadaColunaInicio, coordenadaLinhaFim, coordenadaColunaFim);

        assertEquals(expected, result);
    }

    @Test
    void verificaDirecaoTesteCimaBaixo() {
        int coordenadaLinhaInicio = 0;
        int coordenadaColunaInicio = 0;
        int coordenadaLinhaFim = 3;
        int coordenadaColunaFim = 0;

        String expected = "deCimaParaBaixo";

        String result = ExercicioDezoito.verificaDirecao(coordenadaLinhaInicio, coordenadaColunaInicio, coordenadaLinhaFim, coordenadaColunaFim);

        assertEquals(expected, result);
    }

    @Test
    void verificaDirecaoTesteBaixoCima() {
        int coordenadaLinhaInicio = 3;
        int coordenadaColunaInicio = 0;
        int coordenadaLinhaFim = 0;
        int coordenadaColunaFim = 0;

        String expected = "deBaixoParaCima";

        String result = ExercicioDezoito.verificaDirecao(coordenadaLinhaInicio, coordenadaColunaInicio, coordenadaLinhaFim, coordenadaColunaFim);

        assertEquals(expected, result);
    }

    @Test
    void verificaDirecaoTesteBaixoDireita() {
        int coordenadaLinhaInicio = 0;
        int coordenadaColunaInicio = 1;
        int coordenadaLinhaFim = 4;
        int coordenadaColunaFim = 5;

        String expected = "paraBaixoDireita";

        String result = ExercicioDezoito.verificaDirecao(coordenadaLinhaInicio, coordenadaColunaInicio, coordenadaLinhaFim, coordenadaColunaFim);

        assertEquals(expected, result);
    }

    @Test
    void verificaDirecaoTesteBaixoEsquerda() {
        int coordenadaLinhaInicio = 0;
        int coordenadaColunaInicio = 3;
        int coordenadaLinhaFim = 3;
        int coordenadaColunaFim = 0;

        String expected = "paraBaixoEsquerda";

        String result = ExercicioDezoito.verificaDirecao(coordenadaLinhaInicio, coordenadaColunaInicio, coordenadaLinhaFim, coordenadaColunaFim);

        assertEquals(expected, result);
    }

    @Test
    void verificaDirecaoTesteCimaDireita() {
        int coordenadaLinhaInicio = 2;
        int coordenadaColunaInicio = 1;
        int coordenadaLinhaFim = 0;
        int coordenadaColunaFim = 3;

        String expected = "paraCimaDireita";

        String result = ExercicioDezoito.verificaDirecao(coordenadaLinhaInicio, coordenadaColunaInicio, coordenadaLinhaFim, coordenadaColunaFim);

        assertEquals(expected, result);
    }


    @Test
    void verificaDirecaoTesteCimaEsquerda() {
        int coordenadaLinhaInicio = 3;
        int coordenadaColunaInicio = 3;
        int coordenadaLinhaFim = 0;
        int coordenadaColunaFim = 0;

        String expected = "paraCimaEsquerda";

        String result = ExercicioDezoito.verificaDirecao(coordenadaLinhaInicio, coordenadaColunaInicio, coordenadaLinhaFim, coordenadaColunaFim);

        assertEquals(expected, result);
    }

    @Test
    void verificaDirecaoTesteErro() {
        int coordenadaLinhaInicio = 0;
        int coordenadaColunaInicio = 1;
        int coordenadaLinhaFim = 2;
        int coordenadaColunaFim = 2;

        String expected = "Erro";

        String result = ExercicioDezoito.verificaDirecao(coordenadaLinhaInicio, coordenadaColunaInicio, coordenadaLinhaFim, coordenadaColunaFim);

        assertEquals(expected, result);
    }

    /*
        @Test
        void verificaarrayComparacaoTesteUm() {
            char[][] array2DA = {
                    {'v', 'c', 's', 'e', 'l'},
                    {'m', 'i', 'm', 'i', 'y'},
                    {'i', 'n', 'a', 'a', 't'},
                    {'l', 'a', 'b', 'n', 'i'},
                    {'c', 'n', 'a', 'o', 'm'},
            };

            int coordenadaLinhaInicio = 1;
            int coordenadaColunaInicio = 0;
            int coordenadaLinhaFim = 1;
            int coordenadaColunaFim = 3;

            char[] expeted = {'m', 'i', 'm', 'i'};

            char[] result = ExercicioDezoito.arrayComparacao(array2DA, coordenadaLinhaInicio, coordenadaColunaInicio, coordenadaLinhaFim, coordenadaColunaFim);

            assertArrayEquals(expeted, result);
        }

        @Test
        void verificaarrayComparacaoTesteDois() {
            char[][] array2DA = {
                    {'v', 'c', 's', 'e', 'l'},
                    {'m', 'i', 'm', 'i', 'y'},
                    {'i', 'n', 'a', 'a', 't'},
                    {'l', 'a', 'b', 'n', 'i'},
                    {'c', 'n', 'a', 'o', 'm'},
            };

            int coordenadaLinhaInicio = 1;
            int coordenadaColunaInicio = 3;
            int coordenadaLinhaFim = 1;
            int coordenadaColunaFim = 0;

            char[] expeted = {'i', 'm', 'i', 'm'};

            char[] result = ExercicioDezoito.arrayComparacao(array2DA, coordenadaLinhaInicio, coordenadaColunaInicio, coordenadaLinhaFim, coordenadaColunaFim);

            assertArrayEquals(expeted, result);
        }

        @Test
        void verificaarrayComparacaoTesteTres() {
            char[][] array2DA = {
                    {'v', 'c', 's', 'e', 'l'},
                    {'m', 'i', 'm', 'i', 'y'},
                    {'i', 'n', 'a', 'a', 't'},
                    {'l', 'a', 'b', 'n', 'i'},
                    {'c', 'n', 'a', 'o', 'm'},
            };

            int coordenadaLinhaInicio = 3;
            int coordenadaColunaInicio = 2;
            int coordenadaLinhaFim = 0;
            int coordenadaColunaFim = 2;

            char[] expeted = {'b', 'a', 'm', 's'};

            char[] result = ExercicioDezoito.arrayComparacao(array2DA, coordenadaLinhaInicio, coordenadaColunaInicio, coordenadaLinhaFim, coordenadaColunaFim);

            assertArrayEquals(expeted, result);
        }
    */
    @Test
    void verificaConfirmaPalavraEntreCoordenadasTesteUm() {//daEsquerdaParaDireita
        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'i', 'y'},
                {'i', 'n', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };

        int coordenadaLinhaInicio = 1;
        int coordenadaColunaInicio = 0;
        int coordenadaLinhaFim = 1;
        int coordenadaColunaFim = 4;

        String palavra = "mimi";

        boolean expected = true;

        boolean result = ExercicioDezoito.confirmaPalavraEntreCoordenadas(array2DA, palavra, coordenadaLinhaInicio, coordenadaColunaInicio, coordenadaLinhaFim, coordenadaColunaFim);

        assertEquals(expected, result);
    }

    @Test
    void verificaConfirmaPalavraEntreCoordenadasTesteDois() {//daDireitaParaEsquerda
        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'i', 'y'},
                {'i', 'n', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };

        int coordenadaLinhaInicio = 2;
        int coordenadaColunaInicio = 3;
        int coordenadaLinhaFim = 2;
        int coordenadaColunaFim = 0;

        String palavra = "aani";

        boolean expected = true;

        boolean result = ExercicioDezoito.confirmaPalavraEntreCoordenadas(array2DA, palavra, coordenadaLinhaInicio, coordenadaColunaInicio, coordenadaLinhaFim, coordenadaColunaFim);

        assertEquals(expected, result);
    }


    @Test
    void verificaConfirmaPalavraEntreCoordenadasTesteTres() {//deCimaParaBaixo
        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'i', 'y'},
                {'i', 'n', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };

        int coordenadaLinhaInicio = 0;
        int coordenadaColunaInicio = 1;
        int coordenadaLinhaFim = 3;
        int coordenadaColunaFim = 1;

        String palavra = "cina";

        boolean expected = true;

        boolean result = ExercicioDezoito.confirmaPalavraEntreCoordenadas(array2DA, palavra, coordenadaLinhaInicio, coordenadaColunaInicio, coordenadaLinhaFim, coordenadaColunaFim);

        assertEquals(expected, result);
    }

    @Test
    void verificaConfirmaPalavraEntreCoordenadasTesteQuatro() {//deBaixoParaCima
        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'i', 'y'},
                {'i', 'n', 'a', 'a', 't'},
                {'l', 'a', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };

        int coordenadaLinhaInicio = 3;
        int coordenadaColunaInicio = 4;
        int coordenadaLinhaFim = 0;
        int coordenadaColunaFim = 4;

        String palavra = "ityl";

        boolean expected = true;

        boolean result = ExercicioDezoito.confirmaPalavraEntreCoordenadas(array2DA, palavra, coordenadaLinhaInicio, coordenadaColunaInicio, coordenadaLinhaFim, coordenadaColunaFim);

        assertEquals(expected, result);
    }

    @Test
    void verificaConfirmaPalavraEntreCoordenadasTesteCinco() {//paraBaixoDireita
        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'i', 'y'},
                {'a', 'a', 'a', 't', 't'},
                {'l', 'n', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };

        int coordenadaLinhaInicio = 1;
        int coordenadaColunaInicio = 1;
        int coordenadaLinhaFim = 3;
        int coordenadaColunaFim = 3;

        String palavra = "ian";

        boolean expected = true;

        boolean result = ExercicioDezoito.confirmaPalavraEntreCoordenadas(array2DA, palavra, coordenadaLinhaInicio, coordenadaColunaInicio, coordenadaLinhaFim, coordenadaColunaFim);

        assertEquals(expected, result);
    }

    @Test
    void verificaConfirmaPalavraEntreCoordenadasTesteSeis() {//paraBaixoEsquerda
        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'i', 'y'},
                {'a', 'a', 'a', 'b', 't'},
                {'l', 'b', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };

        int coordenadaLinhaInicio = 1;
        int coordenadaColunaInicio = 3;
        int coordenadaLinhaFim = 3;
        int coordenadaColunaFim = 1;

        String palavra = "iab";

        boolean expected = true;

        boolean result = ExercicioDezoito.confirmaPalavraEntreCoordenadas(array2DA, palavra, coordenadaLinhaInicio, coordenadaColunaInicio, coordenadaLinhaFim, coordenadaColunaFim);

        assertEquals(expected, result);
    }

    @Test
    void verificaConfirmaPalavraEntreCoordenadasTesteSete() {//paraCimaDireita
        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'i', 'm', 'i', 'y'},
                {'a', 'i', 'a', 't', 't'},
                {'l', 'b', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };

        int coordenadaLinhaInicio = 3;
        int coordenadaColunaInicio = 1;
        int coordenadaLinhaFim = 1;
        int coordenadaColunaFim = 3;

        String palavra = "bai";

        boolean expected = true;

        boolean result = ExercicioDezoito.confirmaPalavraEntreCoordenadas(array2DA, palavra, coordenadaLinhaInicio, coordenadaColunaInicio, coordenadaLinhaFim, coordenadaColunaFim);

        assertEquals(expected, result);
    }

    @Test
    void verificaConfirmaPalavraEntreCoordenadasTesteOito() {//paraCimaEsquerda
        char[][] array2DA = {
                {'v', 'c', 's', 'e', 'l'},
                {'m', 'c', 'm', 'a', 'y'},
                {'a', 'a', 'a', 'a', 't'},
                {'l', 'b', 'b', 'n', 'i'},
                {'c', 'n', 'a', 'o', 'm'},
        };

        int coordenadaLinhaInicio = 2;
        int coordenadaColunaInicio = 3;
        int coordenadaLinhaFim = 0;
        int coordenadaColunaFim = 1;

        String palavra = "amc";

        boolean expected = true;

        boolean result = ExercicioDezoito.confirmaPalavraEntreCoordenadas(array2DA, palavra, coordenadaLinhaInicio, coordenadaColunaInicio, coordenadaLinhaFim, coordenadaColunaFim);

        assertEquals(expected, result);
    }

    @Test
    void matrizMascaraEsquerdaDireitaTesteUm() {
        char[][] array2DA = {
                {'v', 'z', 'z', 'z', 'l'},
                {'m', 'c', 'm', 'a', 'y'},
                {'a', 'a', 'o', 'a', 't'},
                {'l', 'b', 'b', 'n', 'i'},
                {'c', 'a', 'a', 'o', 'm'},
        };

        String palavra = "aom";

        int[][] expected = {
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0},
                {0, 0, 1, 1, 1},
        };

        int[][] result = ExercicioDezoito.matrizMascaraEsquerdaDireita(array2DA, palavra);

        assertArrayEquals(expected, result);
    }

    @Test
    void matrizMascaraCimaBaixoTesteUm() {
        char[][] array2DA = {
                {'v', 'z', 'z', 'z', 'l'},
                {'m', 'c', 'm', 'a', 'y'},
                {'a', 'a', 'o', 'a', 't'},
                {'l', 'b', 'b', 'n', 'i'},
                {'c', 'a', 'a', 'o', 'm'},
        };

        String palavra = "moba";

        int[][] expected = {
                {0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 1, 0, 0},
        };

        int[][] result = ExercicioDezoito.matrizMascaraCimaBaixo(array2DA, palavra);

        assertArrayEquals(expected, result);
    }

    @Test
    void matrizMascaraDireitaEsquerdaTesteUm() {
        char[][] array2DA = {
                {'v', 'z', 'z', 'z', 'l'},
                {'m', 'c', 'm', 'a', 'y'},
                {'a', 'a', 'o', 'a', 't'},
                {'l', 'b', 'b', 'n', 'i'},
                {'c', 'a', 'a', 'o', 'm'},
        };

        String palavra = "aoaa";

        int[][] expected = {
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0},
                {1, 1, 1, 1, 0},
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0},
        };

        int[][] result = ExercicioDezoito.matrizMascaraDireitaEsquerda(array2DA, palavra);

        assertArrayEquals(expected, result);
    }

    @Test
    void matrizMascaraBaixoCimaTesteUm() {
        char[][] array2DA = {
                {'v', 'z', 'z', 'z', 'l'},
                {'m', 'c', 'm', 'a', 'y'},
                {'a', 'a', 'o', 'a', 't'},
                {'l', 'b', 'b', 'n', 'i'},
                {'c', 'a', 'a', 'o', 'm'},
        };

        String palavra = "abom";

        int[][] expected = {
                {0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 1, 0, 0},
        };

        int[][] result = ExercicioDezoito.matrizMascaraBaixoCima(array2DA, palavra);

        assertArrayEquals(expected, result);
    }

    @Test
    void matrizMascaraDireitaCimaTesteUm() {
        char[][] array2DA = {
                {'v', 'z', 'z', 'z', 'l'},
                {'m', 'c', 'm', 'a', 'y'},
                {'a', 'a', 'o', 'a', 't'},
                {'l', 'b', 'b', 'n', 'i'},
                {'c', 'a', 'a', 'o', 'm'},
        };

        String palavra = "boal";

        int[][] expected = {
                {0, 0, 0, 0, 1},
                {0, 0, 0, 1, 0},
                {0, 0, 1, 0, 0},
                {0, 1, 0, 0, 0},
                {0, 0, 0, 0, 0},
        };

        int[][] result = ExercicioDezoito.matrizMascaraDireitaCima(array2DA, palavra);

        assertArrayEquals(expected, result);
    }

    @Test
    void matrizMascaraDireitaBaixoTesteUm() {
        char[][] array2DA = {
                {'v', 'z', 'z', 'z', 'l'},
                {'m', 'c', 'm', 'a', 'y'},
                {'a', 'a', 'o', 'a', 't'},
                {'l', 'b', 'b', 'n', 'i'},
                {'c', 'a', 'a', 'o', 'n'},
        };

        String palavra = "conn";

        int[][] expected = {
                {0, 0, 0, 0, 0},
                {0, 1, 0, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 0, 1, 0},
                {0, 0, 0, 0, 1},
        };

        int[][] result = ExercicioDezoito.matrizMascaraDireitaBaixo(array2DA, palavra);

        assertArrayEquals(expected, result);
    }

    @Test
    void matrizMascaraEsquerdaCimaTesteUm() {
        char[][] array2DA = {
                {'v', 'z', 'z', 'z', 'l'},
                {'m', 'c', 'm', 'a', 'y'},
                {'a', 'a', 'o', 'a', 't'},
                {'l', 'b', 'b', 'n', 'i'},
                {'c', 'a', 'a', 'o', 'n'},
        };

        String palavra = "nnocv";

        int[][] expected = {
                {1, 0, 0, 0, 0},
                {0, 1, 0, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 0, 1, 0},
                {0, 0, 0, 0, 1},
        };

        int[][] result = ExercicioDezoito.matrizMascaraEsquerdaCima(array2DA, palavra);

        assertArrayEquals(expected, result);
    }

    @Test
    void matrizMascaraEsquerdaBaixoTesteUm() {
        char[][] array2DA = {
                {'v', 'z', 'z', 'm', 'l'},
                {'m', 'c', 'm', 'a', 'y'},
                {'a', 'a', 'o', 'a', 't'},
                {'l', 'b', 'b', 'n', 'i'},
                {'c', 'a', 'a', 'o', 'n'},
        };

        String palavra = "mal";

        int[][] expected = {
                {0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 1, 0, 0, 0},
                {1, 0, 0, 0, 0},
                {0, 0, 0, 0, 0},
        };

        int[][] result = ExercicioDezoito.matrizMascaraEsquerdaBaixo(array2DA, palavra);

        assertArrayEquals(expected, result);
    }

    @Test
    void somaDuasMatrizesMascarasTesteUm() {
        char[][] array2DA = {
                {'v', 'z', 'z', 'm', 'l'},
                {'m', 'c', 'm', 'a', 'y'},
                {'a', 'm', 'o', 'a', 't'},
                {'l', 'b', 'b', 'n', 'i'},
                {'c', 'a', 'a', 'o', 'n'},
        };

        String palavra1 = "mob";
        String palavra2 = "aob";

        int[][] expected = {
                {0, 0, 0, 0, 0},
                {0, 0, 1, 1, 0},
                {0, 0, 2, 0, 0},
                {0, 1, 1, 0, 0},
                {0, 0, 0, 0, 0},
        };

        int[][] result = ExercicioDezoito.somaDuasMatrizesMascaras(array2DA, palavra1, palavra2);

        assertArrayEquals(expected, result);
    }

    @Test
    void coordenadasEmQueDuasPalavrasCruzamTesteUm() {//testa vertical/horizontal
        char[][] array2DA = {
                {'v', 'z', 'z', 'm', 'l'},
                {'m', 'c', 'm', 'a', 'y'},
                {'a', 'a', 'o', 'a', 't'},
                {'l', 'b', 'b', 'n', 'i'},
                {'c', 'a', 'a', 'o', 'n'},
        };

        String palavra1 = "moba";
        String palavra2 = "aoa";

        int[] expected = {2, 2};


        int[] result = ExercicioDezoito.coordenadasEmQueDuasPalavrasCruzam(array2DA, palavra1, palavra2);

        assertArrayEquals(expected, result);
    }

    @Test
    void coordenadasEmQueDuasPalavrasCruzamTesteDois() { // testa quando duas palvra se sobrepõem
        //por alguma razão
        char[][] array2DA = {
                {'v', 'z', 'z', 'm', 'l'},
                {'m', 'c', 'm', 'a', 'y'},
                {'a', 'm', 'o', 'a', 't'},
                {'l', 'b', 'b', 'n', 'i'},
                {'c', 'a', 'a', 'o', 'n'},
        };

        String palavra1 = "moba";
        String palavra2 = "oba";

        int[] expected = null;


        int[] result = ExercicioDezoito.coordenadasEmQueDuasPalavrasCruzam(array2DA, palavra1, palavra2);

        assertArrayEquals(expected, result);
    }

    @Test
    void coordenadasEmQueDuasPalavrasCruzamTesteTres() {

        char[][] array2DA = {
                {'v', 'z', 'z', 'm', 'l'},
                {'m', 'c', 'm', 'a', 'y'},
                {'a', 'm', 'o', 'a', 't'},
                {'l', 'b', 'b', 'n', 'i'},
                {'c', 'a', 'a', 'o', 'n'},
        };

        String palavra1 = "oal";
        String palavra2 = "zat";

        int[] expected = {1,3};


        int[] result = ExercicioDezoito.coordenadasEmQueDuasPalavrasCruzam(array2DA, palavra1, palavra2);

        assertArrayEquals(expected, result);
    }


    @Test
    void contaOcorrenciasDeUmNumeroTesteUm() {

        int[][] array2DA = {
                {0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0},
                {0, 1, 2, 1, 0},
                {0, 0, 1, 0, 0},
                {0, 0, 1, 0, 0},
        };

        int numero =2;

        int expected=1;

        int result = ExercicioDezoito.contaOcorrenciasDeUmNumero(array2DA,numero);

        assertEquals(expected, result);
    }





}



