package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioTresTest {

    @Test
    void sumElementsArrayTest() {
        int[] sequence = {1,2,3,4};
        int expected = 10;

        int result = ExercicioTres.sumElementsArray(sequence);

        assertEquals(expected,result);
    }

    @Test
    void sumElementsArrayTestTwo() {
        int[] sequence = {0,2,0,4};
        int expected = 6;

        int result = ExercicioTres.sumElementsArray(sequence);

        assertEquals(expected,result);
    }

    @Test
    void sumElementsArrayTestThree() {
        int[] sequence = {0,0,0,0};
        int expected = 0;

        int result = ExercicioTres.sumElementsArray(sequence);

        assertEquals(expected,result);
    }

    @Test
    void sumElementsArrayTestFour() {
        int[] sequence = {0,3,-1,0};
        int expected = 2;

        int result = ExercicioTres.sumElementsArray(sequence);

        assertEquals(expected,result);
    }
}