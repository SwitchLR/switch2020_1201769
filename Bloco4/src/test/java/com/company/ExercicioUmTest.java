package com.company;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioUmTest {

    @org.junit.jupiter.api.Test
    void contaDigitosTeste() {

        int numero = 12345;
        int expected = 5;

        int result = ExercicioUm.contaDigitos(numero);

        assertEquals(expected,result);
    }
}