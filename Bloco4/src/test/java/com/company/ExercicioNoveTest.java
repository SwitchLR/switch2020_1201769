package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioNoveTest {

    @Test
    void inverteArrayTest() {
        int[] sequencia = {1, 2, 3};
        int[] expected = {3, 2, 1};

        int[] result = ExercicioNove.inverteArray(sequencia);

        assertArrayEquals(expected, result);

    }

    @Test
    void inverteArrayTestTwo() {
        int[] sequencia = {0, 2, 10, 31, 0};
        int[] expected = {0, 31, 10, 2, 0};

        int[] result = ExercicioNove.inverteArray(sequencia);

        assertArrayEquals(expected, result);
    }
    @Test
    void inverteArrayTestThree() {
        int[] sequencia = {0, 2, 2, 0};
        int[] expected = {0, 2, 2, 0};

        int[] result = ExercicioNove.inverteArray(sequencia);

        assertArrayEquals(expected, result);
    }

    @Test
    void verificaCapicuaTest() {
        int[] sequencia = {0, 2, 10, 31, 0};
        boolean expected = false;

        boolean result = ExercicioNove.verificaCapicua(sequencia);

        assertEquals(expected, result);
    }

    @Test
    void verificaCapicuaTestTwo() {
        int[] sequencia = {0, 2, 10, 31, 0};
        boolean expected = false;

        boolean result = ExercicioNove.verificaCapicua(sequencia);

        assertEquals(expected, result);
    }

    @Test
    void verificaCapicuaTestThree() {
        int[] sequencia = {0, 2, 2, 0};
        boolean expected = true;

        boolean result = ExercicioNove.verificaCapicua(sequencia);

        assertEquals(expected, result);
    }
}