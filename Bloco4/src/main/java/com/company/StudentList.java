package com.company;

public class StudentList {
    //Atributes
    private Student[] students;

    //Constructor
    public StudentList() { // creates an empty StudentList
        this.students = new Student[0];
    }


    public StudentList(Student[] students) {// Creates an new StudentList and populates it with input students
        if (students == null)
            throw new IllegalArgumentException("Students array shouldnot be null");
        this.students = copyStudentsFromArray(students, students.length);
    }

    //Operations

    public void sortByNumberAsc() { // sorts the list using student's number (US01)

        Student tempStudent = null;
        //sort the students in ascending order using two for loops
        for (int index1 = 0; index1 < this.students.length; index1++) {
            for (int index2 = index1 + 1; index2 < this.students.length; index2++) {
                if (this.students[index1].compareToByNumber(this.students[index2]) > 0) {
                    //swap elements if not in order
                    tempStudent = this.students[index1];
                    this.students[index1] = this.students[index2];
                    this.students[index2] = tempStudent;
                }
            }
        }
    }

    public void sortByGradeDesc() {
        Student tempStudent = null;
        //sort the students in ascending order using two for loops
        for (int index1 = 0; index1 < this.students.length; index1++) {
            for (int index2 = index1 + 1; index2 < this.students.length; index2++) {
                if (this.students[index1].compareToByGrade(this.students[index2]) < 0) {
                    //swap elements if not in order
                    tempStudent = this.students[index1];
                    this.students[index1] = this.students[index2];
                    this.students[index2] = tempStudent;
                }
            }
        }
    }


    public Student[] toArray() { // returns a copy of students stored in the list as an Array
        return this.copyStudentsFromArray(this.students, this.students.length);
    }

    /*  private Student[] copyStudentsFromArray(Student[] students, int size) {
          Student[] copyArray = new Student[size];
          for (int count = 0; (count < size) && (count < students.length); count++) {
              copyArray[count] = students[count];
          }
          return copyArray;
      }
      */
    private Student[] copyStudentsFromArray(Student[] students, int size) {
        return this.copyStudentsFromArray(students, 0, size);
    }

    private Student[] copyStudentsFromArray(Student[] students, int start, int size) {
        Student[] copyArray = new Student[size];
        int count = 0;

        for (int idx = start; (count < size) && (idx < students.length); idx++) {
            copyArray[idx - start] = students[idx];
            count++;
        }
        return copyArray;
    }

    public boolean add(Student student) {
        if (student == null)
            return false;
        if (this.exists(student))
            return false;
        this.students = copyStudentsFromArray(this.students, this.students.length + 1);
        this.students[this.students.length - 1] = student;
        return true;
    }

    public boolean remove(Student student) {
        if (student == null)
            return false;
        int idx = this.getIndexOf(student);
        if (idx < 0)
            return false;
        Student[] leftSide = this.copyStudentsFromArray(this.students, idx);
        Student[] rightSide = this.copyStudentsFromArray(this.students, idx + 1, this.students.length - idx - 1);
        this.students = join(leftSide, rightSide);
        return true;
    }

    private Student[] join(Student[] students1, Student[] students2) {
        int size = students1.length + students2.length;
        Student [] copyArray = new Student[size];

        for ( int idx =0; (idx< students1.length); idx ++){
            copyArray[idx]= students1[idx];
        }
        for (int idx =0; (idx < students2.length); idx++){
            copyArray[idx+students1.length]= students2[idx];
        }
        return copyArray;
    }


}
