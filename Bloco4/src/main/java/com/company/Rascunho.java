/*
package com.company;


public class Student {
//------------atributos


    private int number;
    private String name;
    private int grade;


//------------ Métodos construtores


    public Student(int number, String name) {
        this.number = number;
        setName(name);
    }

    public Student(int number, String name, int grade) { //neste caso temos dois construtores
        //podemos ter um ou mais cosntrutores
        this.number = number;
        this.name = name;
        this.grade = grade;
    }

    //------------ Getters e Setters

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (isNameValid(name))
            this.name = name;
        else
            throw new IllegalArgumentException("invalid name");
    }


    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }


    //------------ métodos negócio

    private boolean isNameValid(String name) {
        if (name != null && name.length() > 5)
            return true;
        else
            return false;
    }

}
/*
    public static boolean sortStudentsByNumberAsc(Student[] students) {
        if (students == null)
            return false;
        Student tempStudent = null;

        for (int index1 = 0; index1 < students.length; index1++) {
            for (int index2 = index1 + 1; index2 < students.length; index2++) {
                if (students[index1].number > students[index2].number) {
                    tempStudent = students[index1];
                    students[index1] = students[index2];
                    students[index2] = tempStudent;
                }
            }
        }
        return true;
    }

    public static boolean sortStudentsByGradesDesc(Student[] students) {
        if (students == null)
            return false;
        Student tempStudent = null;

        for (int index1 = 0; index1 < students.length; index1++) {
            for (int index2 = index1 + 1; index2 < students.length; index2++) {
                if (students[index1].grade < students[index2].grade) {
                    tempStudent = students[index1];
                    students[index1] = students[index2];
                    students[index2] = tempStudent;
                }
            }
        }
        return true;
    }


    @Test
    void sortStudentsByNumberAscWithSeveralElementsIncorrectlyOrdered() {
        //Arrange
        Student student1 = new Student();
        student1.number = 12000145;
        student1.name = "Sampaio";
        student1.grade = 11;

        Student student2 = new Student();
        student2.number = 1200054;
        student2.name = "Moreira";
        student2.grade = 11;

        Student student3 = new Student();
        student3.number = 1200086;
        student3.name = "Silva";
        student3.grade = 20;

        Student[] students = {student1, student2, student3}; // Unordered
        Student[] expected = {student2, student3, student1}; // Ordered

        //Act
        boolean result = Student.sortStudentsByNumberAsc(students);

        //Assert
        assertTrue(result);
        assertArrayEquals(expected, students);
    }

    @Test
    void sortStudentsByGradeDesWithSeveralElementsIncorrectlyOrdered() {
        //Arrange
        Student student1 = new Student();
        student1.number = 1200145;
        student1.name = "Sampaio";
        student1.grade = 15;

        Student student2 = new Student();
        student2.number = 1200054;
        student2.name = "Moreira";
        student2.grade = 20;

        Student student3 = new Student();
        student3.number = 1200086;
        student3.name = "Silva";
        student3.grade = 10;

        Student[] students = {student1, student2, student3}; // Unordered
        Student[] expected = {student2, student1, student3}; // Ordered

        //Act
        boolean result = Student.sortStudentsByGradesDesc(students);

        //Assert
        assertTrue(result);
        assertArrayEquals(expected, students);
    }

 */