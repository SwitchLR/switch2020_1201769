package com.company;

public class ExercicioOito {
    public static void main(String[] args) {
        //exercicio8();
    }

    public static int[] getVariousMultiplesInInterval(int n, int m, int[] multiples) {
        int variousMultiplesIndex = 0;
        int[] variousMultiples = new int[15];

        for (int i = n; i <= m; i++) { // percorre o intervalo de numeros
            int counter = 0; //conta o numero de multiplos

            for (int j = 0; j < multiples.length; j++) { //percorre o array
                if (verifyMultiple(i, multiples[j])) { // confirma se o o numero é multiplo de um dos valores no array
                    counter++;
                }
            }
            if (counter == multiples.length) { // se o numero for multiplo de todos os numeros do array
                variousMultiples[variousMultiplesIndex] = i; // guarda o numero no novo array
                variousMultiplesIndex++;
            }
        }
        return ExercicioSeis.getFirstNNumbers(variousMultiples,variousMultiplesIndex); //trunca o array
    }

    public static boolean verifyMultiple(int n1, int n2) {
        boolean multiple = false;
        if (n1 % n2 == 0) {
            multiple = true;
        }
        return multiple;
    }
}
