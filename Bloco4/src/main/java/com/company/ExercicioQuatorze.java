package com.company;

public class ExercicioQuatorze {
    public static void main(String[] args) {
        //exercicio14();
    }

    public static boolean verificaMatrizRectuangulo(int[][] array2D) {
        boolean matrizRectangulo = false;
        //confirma se as linhas têm todas o mesmo tamanho e se não é um quadrado
        if (ExercicioDoze.confirmaNumeroColunas(array2D) >= 0 && !ExercicioTreze.verificaMatrizQuadrada(array2D)) {
            matrizRectangulo = true;
        }
        return matrizRectangulo;
    }
}
