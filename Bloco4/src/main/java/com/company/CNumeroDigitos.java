package com.company;

public class CNumeroDigitos {
    private int numero = 0;


    //construtor
    public CNumeroDigitos(int numero) {

        this.numero = numero;
    }

    public int contaDigitos() {
        int contaDigitos = 0;
        while (numero > 0) {
            numero = numero / 10;
            contaDigitos++;
        }
        return contaDigitos;
    }
}
