package com.company;

public class ExercicioAulaTresNovembro {
    public static void main(String[] args) {
        //    exercicioAula();
    }

    public static int[] creatCopy(int[] original, int size) {
        if ((size < 0) || (original == null))
            return null;
        if (size > original.length)
            size = original.length;
        int[] clone = new int[size];
        for (int index = 0; index < size; index++){
            clone[index]=original[index];
        }
        return clone;
    }

    
}