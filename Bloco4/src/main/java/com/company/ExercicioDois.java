package com.company;
import java.util.Scanner;

public class ExercicioDois {
    public static void main(String[] args) {
        //exercicio2();
    }

    public static int[] arrayDigits(int number) {

        int[] arraySeparatedDigit = new int[ExercicioUm.contaDigitos(number)];

        for (int i = 0; i < arraySeparatedDigit.length; i++) {
            int digit = number % 10;
            arraySeparatedDigit[arraySeparatedDigit.length - i - 1] = digit;
            number /= 10;
        }
        return arraySeparatedDigit;
    }
}