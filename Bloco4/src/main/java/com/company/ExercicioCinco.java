package com.company;

public class ExercicioCinco {
    public static void main(String[] args) {
        //exercicio5();
    }

    public static int getEvenSum(int number){

        int[] arrayDigits = ExercicioDois.arrayDigits(number);

        int [] evenArray = ExercicioQuatro.getEvenArray(arrayDigits);

        int evenSum = ExercicioTres.sumElementsArray(evenArray);

        return evenSum;

    }
}
