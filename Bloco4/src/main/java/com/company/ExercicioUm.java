package com.company;

import java.util.Scanner;


public class ExercicioUm {
    public static void main(String[] args) {
        exercicio1();
    }

    public static void exercicio1() {
        Scanner ler = new Scanner(System.in);
        System.out.println("Indique uma sequencia de numeros");

        int sequencia = ler.nextInt();

        System.out.println("A sequencia tem " + contaDigitos(sequencia) + " digitos.");
    }

    public static int contaDigitos(int numero) {
        int contaDigitos = 0;
        int digito;

        while (numero > 0) {
            digito = numero % 10;
            numero = numero / 10;
            contaDigitos++;
        }
        return contaDigitos;
    }
}
