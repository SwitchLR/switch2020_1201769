package com.company;

public class ExercicioDezasseis {
    public static void main(String[] args) {
        //exercicio16();
    }
    public static int determinanteLaPlace(int[][] array2D) {

        int determinante = 0;
        if (array2D.length == 1) { //determinante para matrizes de ordem 1
            determinante = array2D[0][0];
        } else if (array2D.length == 2) { //determinante para matrizes de ordem 2
            determinante = array2D[0][0] * array2D[1][1] - array2D[0][1] * array2D[1][0];
        } else if (array2D.length == 3) { //determinante para matrizes de ordem 3
            determinante = array2D[0][0] * (array2D[1][1] * array2D[2][2] - array2D[1][2] * array2D[2][1])
                    - array2D[0][1] * (array2D[1][0] * array2D[2][2] - array2D[1][2] * array2D[2][0])
                    + array2D[0][2] * (array2D[1][0] * array2D[2][1] - array2D[1][1] * array2D[2][0])
            ;
        } else { //para matrizes de ordem>3
            for (int c = 0; c < array2D.length; c++) {//percorre os elementos da coluna
                if (array2D[0][c] != 0) { // se o elemento for =0 não é preciso fazer o calculo e "escolhe" a primeira linha
                    int[][] temporaria = new int[array2D.length - 1][array2D.length - 1]; //nova matriz temporária com menos uma linha e menos uma coluna que a matriz original
                    int iTemp = 0;  //iterador da linha matriz temporaria
                    int jTemp = 0;  //iterador da coluna matriz temporaria
                    for (int row = 1; row < array2D.length; row++) { //começa sempre na segunda linha porque escolhemos a primeira para percorrer
                        for (int collumn = 0; collumn < array2D.length; collumn++) {
                            if (collumn != c) { //para "excluir" a coluna que estamos a analisar
                                temporaria[iTemp][jTemp] = array2D[row][collumn];//preenche a matriz temporária
                                jTemp++;//incrementamos a coluna para preencher a linnha toda até ao fim
                            }
                        }
                        iTemp++; // depois de preencher a linha toda, mudamos a linha da matriz temporaria
                        jTemp = 0; //para começar a preencher a linha seguinte da matriz temporaria desde a primeira coluna
                    }
                    determinante += Math.pow(-1, c) * array2D[0][c] * determinanteLaPlace(temporaria);//soma o novo determinante ao determinante anterior
                }
            }
        }
        return determinante;
    }
}
