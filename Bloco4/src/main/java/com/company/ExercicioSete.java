package com.company;

public class ExercicioSete {
    public static void main(String[] args) {
        //exercicio7();
    }

    public static int[] getMultipleOfThree(int n, int m) {

        int multipleIndex = 0;
        int[] multipleOfThreeArray = new int[countMultipleOfThree(n, m)]; //para saber o tamanho do array fiz um método separado
        int[] emptyArray = new int [0];

        if (n > m) {
            for (int i = m; i < n; i++) {
                if (i % 3 == 0) {
                    multipleOfThreeArray[multipleIndex] = i;
                    multipleIndex++;
                }
            }
        } else if (m > n) {
            for (int i = m; i < n; i++) {
                if (i % 3 == 0) {
                    multipleOfThreeArray[multipleIndex] = i;
                    multipleIndex++;
                }
            }
        }
        return multipleOfThreeArray;
    }

    public static int countMultipleOfThree(int n, int m) {    ////metodo para contar o número de multiplos de 3
        int countMultipleOfThree = 0;

        if (n > m) {
            for (int i = m; i < n; i++) {
                if (i % 3 == 0) {
                    countMultipleOfThree++;
                }
            }
        }else if (m > n) {
            for (int i = n; i < m; i++) {
                if (i % 3 == 0) {
                    countMultipleOfThree++;
                }
            }
        }else if (n==m){
            return -1;
        }

        return countMultipleOfThree;
    }
}

