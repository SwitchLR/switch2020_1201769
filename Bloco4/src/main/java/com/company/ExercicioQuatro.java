package com.company;

public class ExercicioQuatro {
    public static void main(String[] args) {
        //exercicio4();
    }

    public static int[] getEvenArray(int[] sequence) {

        int n = sequence.length - countOddArray(sequence);
        int[] evenArray = new int[n];
        int evenIndex = 0;

        for (int i = 0; i < sequence.length; i++) {
            if (sequence[i] % 2 == 0) {
                evenArray[evenIndex] = sequence[i];
                evenIndex++;
            }
        }
        return evenArray;
    }

    public static int countOddArray(int[] sequence) {
        int countOdd = 0;
        for (int i = 0; i < sequence.length; i++) {
            if (sequence[i] % 2 != 0) {
                countOdd++;
            }
        }
        return countOdd;
    }
}


