package com.company;

public class ExercicioOnze {
    public static void main(String[] args) {
        //exercicio11();
    }

    //considerando que no caso de o tamanho dos arrays ser diferente, faz o produto escalar até à dimensão mais pequena
    public static int produtoEscalarDoisArrays(int[] array1, int[] array2) {
        int produtoEscalar = 0;
        if (array1.length <= array2.length) {
            for (int i = 0; i < array1.length; i++) {
                produtoEscalar += (array1[i] * array2[i]);
            }
        } else {
            for (int i = 0; i < array2.length; i++) {
                produtoEscalar += (array1[i] * array2[i]);
            }
        }
        return produtoEscalar;
    }
}
