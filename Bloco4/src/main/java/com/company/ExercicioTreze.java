package com.company;

public class ExercicioTreze {
    public static void main(String[] args) {
        //exercicio13();
    }

    public static boolean verificaMatrizQuadrada(int[][] array2D) {
        boolean matrizQuadrada = false;
        if (ExercicioDoze.confirmaNumeroColunas(array2D) == array2D.length) { //confirma se numero de colunas é igual ao numero de linhas
            matrizQuadrada = true;
        }
        return matrizQuadrada;
    }
}
