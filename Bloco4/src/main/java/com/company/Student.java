package com.company;

public class Student {

//atributtes

    private int number;
    private String name;
    private int grade;

    //constructor
    public Student(int number, String name) {
        //used to initialize attributes
        if (!this.isValidNumber(number))
            throw new IllegalArgumentException("Student number needs to be 7 digits number (e.g. 1190001");
        if (!this.isValidName(name))
            throw new IllegalArgumentException("Student name cannot be shorter than 5 chars");
        this.number = number;
        this.name = name.trim();
        this.grade = -1; //-1 means student hasn't been evaluated
    }

//operations

    public boolean isApproved() {
        return (this.grade >= 10);
    }

    private boolean isValidNumber(int number) {
        if (number != Math.abs(number))
            return false;
        String strNumber = Integer.toString(number);
        return (strNumber.length() == 7);
    }

    private boolean isValidName(String name) {
        if (name == null)
            return false;
        name = name.trim();
        return !(name.length() < 5);
    }

    private boolean isValidGrade(int grade) {
        return (grade >= 0 && grade <= 20);
    }

    private boolean isEvaluated() {
        return grade >= 0;
    }

    public void doEvaluation(int grade) {
        if (!this.isEvaluated() && isValidGrade(grade))
            this.grade = grade;
    }

    public int getNumber() {
        return this.number;
    }

    public int compareToByNumber(Student other) {
        if (this.number < other.number)
            return -1; // less than other
        if (this.number > other.number)
            return 1; // greates than other
        return 0; //both have the same value
    }

    public int compareToByGrade(Student other) {
        if (this.grade < other.grade)
            return -1; // less than other
        if (this.grade > other.grade)
            return 1; // greates than other
        return 0; //both have the same value
    }

    public static boolean sortStudentsByNumberAsc(Student[] students) {
        if (students == null)
            return false;
        Student tempStudent = null;
        for (int index1 = 0; index1 < students.length; index1++) {
            for (int index2 = index1 + 1; index2 < students.length; index2++) {
                if (students[index1].getNumber() > (students[index2].getNumber())) {
                    //swap elements if not in order
                    tempStudent = students[index1];
                    students[index1] = students[index2];
                    students[index2] = tempStudent;
                }
            }
        }
        return true;
    }

    public static void checkingObjectEquality() {
        Student st1 = new Student(1200327, "Paulo");
        st1.doEvaluation(19);

        Student st2 = st1;

        Student st3 = new Student(1200327, "Paulo");
        st1.doEvaluation(19);

        Student st4 = new Student(1200327, "Ritinha");
        st4.doEvaluation(20);

        System.out.println("st1 is equal to st2: " + st1.equals(st2));
        System.out.println("st1 is equal to st3: " + st1.equals(st3));
        System.out.println("sti is equal to st4: " + st1.equals(st4));

    }

    @Override
    public boolean equals(Object o) {
        if (this == 0) return true;
        if (o ==null || getClass() !=o.getClass()) return false;
        Student student = Student(o);
        return  this.number == student.number;
    }


}