package com.company;

public class ExercicioDez {
    public static void main(String[] args) {
        //exercicio10();
    }

    //a)
    public static int menorValorArray(int[] array) {
        int menorValor = array[0];
        for (int index = 1; index < array.length; index++) {
            if (array[index] <= menorValor) {
                menorValor = array[index];
            }
        }
        return menorValor;
    }

    //b)
    public static int maiorValorArray(int[] array) {
        int maiorValor = array[0];
        for (int index = 0; index < array.length; index++) {
            if (array[index] >= maiorValor) {
                maiorValor = array[index];
            }
        }
        return maiorValor;
    }

    //c)
    public static double valorMedioArray(int[] array) {
        double valorTotal = 0;
        double contaValores = 0;

        for (int index = 0; index < array.length; index++) {
            valorTotal += array[index];
            contaValores++;
        }
        return valorTotal / contaValores;
    }

    //d)
    public static int produtoArray(int[] array) {
        int produto = 1;
        for (int index = 0; index < array.length; index++) {
            produto *= array[index];
        }
        return produto;
    }

    //e)
    public static int[] naoRepetidosArray(int[] array) {
        int[] naoRepetidos = new int[array.length];
        int contaNaoRepetidos = 0;
        int j = 0;

        //se um valor do array original não existir no array naoRepetidos, guarda-o
        for (int index = 0; index < array.length; index++) {
            if (encontraValorArray(array[index], naoRepetidos) == false) {
                naoRepetidos[j] = array[index];
                j++;
                contaNaoRepetidos++;
            }
        }
        //este if serve apenas para controlar o facto de o array de naoReptidos inicializar cheio de '0'
        //se houver um '0' no array original coloca-o no final do novo array
        if (encontraValorArray(0, array)) {
            contaNaoRepetidos++;
            naoRepetidos[contaNaoRepetidos] = 0;
        }
        return ExercicioSeis.getFirstNNumbers(naoRepetidos, contaNaoRepetidos);
    }

    //procura um valor num array
    public static boolean encontraValorArray(int n, int[] array) {
        boolean encontraValor = false;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == n) {
                encontraValor = true;
            }
        }
        return encontraValor;
    }

    //f) já tinha feito este método no exercicio 9;
    public static int[] inverteArray(int[] sequencia) {
        int arrayInvertido[] = new int[sequencia.length];
        for (int i = 0; i < sequencia.length; i++) {
            arrayInvertido[i] = sequencia[sequencia.length - 1 - i];
        }
        return arrayInvertido;
    }

    //g)
    public static int[] verificaPrimosArray(int[] array) {

        int[] primosArray = new int[array.length];
        int k = 0;
        int contaPrimos = 0;
        for (int i = 0; i < array.length; i++) {
            if (numeroPrimo(array[i])) {
                primosArray[k] = array[i];
                k++;
                contaPrimos++;
            }
        }
        return ExercicioDez.naoRepetidosArray(ExercicioSeis.getFirstNNumbers(primosArray, contaPrimos));
    }

    public static boolean numeroPrimo(int numero) {
        if (numero == 0 || numero == 1) {
            return false;
        }
        for (int i = 2; i < numero; i++) {
            if (numero % i == 0) {
                return false;
            }
        }
        return true;
    }
}




