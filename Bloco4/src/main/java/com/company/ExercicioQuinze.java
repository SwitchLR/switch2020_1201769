package com.company;

public class ExercicioQuinze {
    public static void main(String[] args) {
        //exercicio15();
    }

    // a) elemento de menor valor numa matriz
    public static int procuraMenorValorMatriz(int array2D[][]) {
        int menorValor = array2D[0][0];
        for (int row = 0; row < array2D.length; row++) {
            for (int collumn = 0; collumn < array2D[row].length; collumn++) {
                if (array2D[row][collumn] < menorValor) {
                    menorValor = array2D[row][collumn];
                }
            }
        }
        return menorValor;
    }

    // b) elemento de maior valor numa matriz
    public static int procuraMaiorValorMatriz(int array2D[][]) {
        int maiorValor = array2D[0][0];
        for (int row = 0; row < array2D.length; row++) {
            for (int collumn = 0; collumn < array2D[row].length; collumn++) {
                if (array2D[row][collumn] > maiorValor) {
                    maiorValor = array2D[row][collumn];
                }
            }
        }
        return maiorValor;
    }

    // c) valor medio dos elementos de uma matriz
    public static double calculaValorMedioMatriz(int array2D[][]) {
        int somaElementosMatriz = 0;
        int contaElementosMatriz = 0;
        for (int row = 0; row < array2D.length; row++) {
            for (int collumn = 0; collumn < array2D[row].length; collumn++) {
                somaElementosMatriz += array2D[row][collumn];
                contaElementosMatriz++;
            }
        }
        return somaElementosMatriz / contaElementosMatriz;
    }

    // d) produto dos elementos de uma matriz
    public static int calculaProdutoMatriz(int array2D[][]) {
        int produtoMatriz = 1;
        for (int row = 0; row < array2D.length; row++) {
            for (int collumn = 0; collumn < array2D[row].length; collumn++) {
                produtoMatriz *= array2D[row][collumn];
            }
        }
        return produtoMatriz;
    }

    // e) conjunto de elementos não repetidos

    public static int[] conjuntosNaoRepetidosMatriz(int array2D[][]) {

        int[] conjuntoNaoRepetidos = new int[calculaTamanhoMatriz(array2D)];//cria array com o tamanho da Matriz
        int indexNaoRepetidos = 0;
        int contaNaoRepetidos = 0; //controla os numeros unicos, para truncar o novo array

        //procurar em cada posicao da matriz se o valor já se encontra no conjunto de naoRepetidos
        for (int row = 0; row < array2D.length; row++) {
            for (int collumn = 0; collumn < array2D[row].length; collumn++) {
                if (!ExercicioDez.encontraValorArray(array2D[row][collumn], conjuntoNaoRepetidos)) {
                    conjuntoNaoRepetidos[indexNaoRepetidos] = array2D[row][collumn];
                    indexNaoRepetidos++;
                    contaNaoRepetidos++;
                }
            }
        }
        //procurar o caso expecifico do '0' na matriz original, para truncar menos um '0' no conjuntoNaoRepetidos e
        // coloca-lo no final do novo array
        for (int row = 0; row < array2D.length; row++) { // está a procurar linha a linha porque basta encontrar um '0'
            if (ExercicioDez.encontraValorArray(0, array2D[row])) {
                contaNaoRepetidos++; //para "guardar" um espaço no novo array
                conjuntoNaoRepetidos[contaNaoRepetidos] = 0; //coloca na ultima posição do novo array o '0'
                row = array2D.length; // equivalente a fazer um break;
            }
        }
        return ExercicioSeis.getFirstNNumbers(conjuntoNaoRepetidos, contaNaoRepetidos);//para truncar o conjuntoNaoRepetidos
    }

    //calcula o tamanho de uma matriz
    public static int calculaTamanhoMatriz(int[][] array2D) {
        int tamanhoMatriz = 0;
        for (int row = 0; row < array2D.length; row++) {
            tamanhoMatriz += array2D[row].length;
        }
        return tamanhoMatriz;
    }

    //f) elementos primos de uma matriz
    public static int[] conjuntoPrimosMatriz(int[][] array2D) {
        int[] conjuntoPrimos = new int[calculaTamanhoMatriz(array2D)];
        int contaPrimos = 0;
        int j = 0;
        for (int row = 0; row < array2D.length; row++) {
            for (int collumn = 0; collumn < array2D[row].length; collumn++) {
                if (ExercicioDez.numeroPrimo(array2D[row][collumn])) { // verifica se o numero é primo
                    conjuntoPrimos[j] = array2D[row][collumn];
                    j++;
                    contaPrimos++;
                }
            }
        }
        //trunca o novo array e retira os repetidos
        return ExercicioDez.naoRepetidosArray(ExercicioSeis.getFirstNNumbers(conjuntoPrimos, contaPrimos));
    }

    //g) diagonal principal de uma matriz
    public static int[] conjuntoDiagonalPrincipalMatriz(int[][] array2D) {
        int[] conjuntoDiagonalPrincipal = new int[0];
        int j = 0;
        //if para matrizes quadradas
        if (ExercicioTreze.verificaMatrizQuadrada(array2D)) {
            conjuntoDiagonalPrincipal = new int[array2D.length];
            for (int row = 0; row < array2D.length; row++) {
                for (int collumn = 0; collumn < array2D[row].length; collumn++) {
                    if (row == collumn) {
                        conjuntoDiagonalPrincipal[j] = array2D[row][collumn];
                        j++;
                    }
                }
            }
            return conjuntoDiagonalPrincipal;
        }
        //if para matriz rectangular
        else if (ExercicioQuatorze.verificaMatrizRectuangulo(array2D)) {
            //depois de saber que é rectangulo verifica se é um rectangulo com mais linha que colunas ou vice-versa
            //altera o tamanho do conjuntoDiagonalPrincipal
            if (array2D.length > array2D[0].length) {
                conjuntoDiagonalPrincipal = new int[array2D[0].length];
                for (int row = 0; row < array2D[0].length; row++) {
                    for (int collumn = 0; collumn < array2D[row].length; collumn++) {
                        if (row == collumn) {
                            conjuntoDiagonalPrincipal[j] = array2D[row][collumn];
                            j++;
                        }
                    }
                }
                return conjuntoDiagonalPrincipal;
            }
            if (array2D[0].length > array2D.length) {
                conjuntoDiagonalPrincipal = new int[array2D.length];
                for (int row = 0; row < array2D.length; row++) {
                    for (int collumn = 0; collumn < array2D.length; collumn++) {
                        if (row == collumn) {
                            conjuntoDiagonalPrincipal[j] = array2D[row][collumn];
                            j++;
                        }
                    }
                }
                return conjuntoDiagonalPrincipal;
            }
        } else {
            return null; //para as situações em que a matriz não é quadrado nem rectangulo
        }
        return conjuntoDiagonalPrincipal;
    }

    // h) diagonal secundaria de uma matriz
    public static int[] conjuntoDiagonalSecundariaMatriz(int[][] array2D) {
        int[] conjuntoDiagonalSecundaria = new int[array2D.length];
        int j = 0;
        //if para matrizes quadradas
        if (ExercicioTreze.verificaMatrizQuadrada(array2D)) {
            for (int row = 0; row < array2D.length; row++) {
                conjuntoDiagonalSecundaria[j] = array2D[row][array2D.length - 1 - row];
                j++;
            }
        }
        //if para matriz rectangulo
        else if (ExercicioQuatorze.verificaMatrizRectuangulo(array2D)) {
            if (array2D.length > array2D[0].length) {
                conjuntoDiagonalSecundaria = new int[array2D[0].length];
                for (int row = 0; row < array2D[0].length; row++) {
                    conjuntoDiagonalSecundaria[j] = array2D[row][array2D.length - 1 - row];
                    j++;
                }
            } else if (array2D.length < array2D[0].length) {
                conjuntoDiagonalSecundaria = new int[array2D.length];
                for (int row = 0; row < array2D.length; row++) {
                    conjuntoDiagonalSecundaria[j] = array2D[row][array2D[0].length - 1 - row];
                    j++;
                }
            }
        } else return null;

        return conjuntoDiagonalSecundaria;
    }

    //i) matriz identidade
    //matriz com '0' excepto a diagonal principal só com '1'
    public static boolean verificaMatrizIdentidade(int[][] array2D) {
        if (ExercicioTreze.verificaMatrizQuadrada(array2D)) {
            for (int row = 0; row < array2D.length; row++) {
                for (int collumn = 0; collumn < array2D[0].length; collumn++) {
                    if (row == collumn && array2D[row][collumn] != 1 && row != collumn && array2D[row][collumn] != 0)
                        return false;
                }
            }
        }
        return true;
    }

    //j matriz inversa
    public static int[][] criaMatrizInversa(int[][] array2D) {


        int[][] matrizInversa = new int[array2D.length][array2D[0].length]; //dimensionada a matrizInversa

        if (ExercicioTreze.verificaMatrizQuadrada(array2D)) { // verifica se a matriz é quadrada
            int determinanteMatriz = ExercicioDezasseis.determinanteLaPlace(array2D); // faz o calculo do determinante

            int[][] matrizCofatores = new int[array2D.length][array2D[0].length]; // cria a matrizCofactores
            matrizCofatores = ExercicioQuinze.criaMatrizCofactores(array2D);

            int[][] matrizCofatoresTransposta = new int[array2D.length][array2D[0].length]; //faz a matrizTransposta da matriz cofactores
            matrizCofatoresTransposta = ExercicioQuinze.criaMatrizTransposta(matrizCofatores);

            matrizInversa = ExercicioDezassete.calculaMatrizPorConstante(matrizCofatoresTransposta, 1 / determinanteMatriz);//faz a matriz inversa
        } else return null;
        return matrizInversa;
    }


    public static int[][] criaMatrizCofactores(int[][] array2D) {
        int[][] matrizCofactores = new int[array2D.length][array2D[0].length];
        if (ExercicioTreze.verificaMatrizQuadrada(array2D)) {
            for (int row = 0; row < array2D.length; row++) {
                for (int collumn = 0; collumn < array2D[0].length; collumn++) {
                    matrizCofactores[row][collumn] = (int) (Math.pow(-1, (row + collumn)) * ExercicioDezasseis.determinanteLaPlace(ExercicioQuinze.removeLinhaEColunaMatriz(array2D, row, collumn)));
                }
            }
        } else return null;
        return matrizCofactores;
    }

    public static int[][] removeLinhaEColunaMatriz(int[][] array2D, int linhaRemovida, int colunaRemovida) {
        int[][] removeLinhaEColuna = new int[array2D.length - 1][array2D[0].length - 1];
        removeLinhaEColuna = ExercicioQuinze.removeColunaMatrizQuadrada(ExercicioQuinze.removeLinhaMatrizQuadrada(array2D, linhaRemovida), colunaRemovida);

        return removeLinhaEColuna;
    }


    public static int[][] removeLinhaMatrizQuadrada(int[][] array2D, int linhaRemovida) {
        int[][] removeLinha = new int[array2D.length - 1][array2D[0].length];
        int i = 0;
        int j = 0;
        for (int row = 0; row < array2D.length; row++) {
            if (row != linhaRemovida) {
                for (int collumn = 0; collumn < array2D[0].length; collumn++) {
                    removeLinha[i][j] = array2D[row][collumn];
                    j++;
                }
                j = 0;
                i++;
            }
        }
        return removeLinha;
    }

    public static int[][] removeColunaMatrizQuadrada(int[][] array2D, int colunaRemovida) {
        int[][] matrizColunaRemovida = new int[array2D.length][array2D[0].length - 1];
        int i = 0;
        int j = 0;
        for (int row = 0; row < array2D.length; row++) {
            for (int collumn = 0; collumn < array2D[0].length; collumn++) {
                if (collumn != colunaRemovida) {
                    matrizColunaRemovida[i][j] = array2D[row][collumn];
                    j++;
                }
            }
            j = 0;
            i++;
        }
        return matrizColunaRemovida;
    }


    //k) matriz transposta

    public static int[][] criaMatrizTransposta(int[][] array2D) {
        //as colunas passam a linhas
        if (ExercicioTreze.verificaMatrizQuadrada(array2D)) {
            int[][] matrizTransposta = new int[array2D.length][array2D[0].length];
            for (int row = 0; row < array2D.length; row++) {
                for (int collumn = 0; collumn < array2D[0].length; collumn++) {
                    matrizTransposta[row][collumn] = array2D[collumn][row];
                }
            }
            return matrizTransposta;
        } else if (ExercicioQuatorze.verificaMatrizRectuangulo(array2D)) {
            int[][] matrizTransposta = new int[array2D[0].length][array2D.length];//troca o numero linhas com o numero colunas
            for (int row = 0; row < array2D[0].length; row++) {
                for (int collumn = 0; collumn < array2D.length; collumn++) {
                    matrizTransposta[row][collumn] = array2D[collumn][row]; //troca os valores colunas/linha
                }
            }
            return matrizTransposta;
        }
        return null;
    }

}



























