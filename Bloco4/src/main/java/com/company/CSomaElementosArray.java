package com.company;

public class CSomaElementosArray {
    private int[] sequence;

    public CSomaElementosArray(int[] sequencia) {

        this.sequence = sequencia;
    }

    public int sumElementsArray() {
        int sumElements = 0;

        for (int i = 0; i < sequence.length; i++) {
            sumElements += sequence[i];
        }
        return sumElements;
    }

}
