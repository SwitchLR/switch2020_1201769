package com.company;

public class ExercicioDezassete {
    public static void main(String[] args) {
        //exercicio17();
    }

    // a) multiplicar uma matriz por uma constante
    public static int[][] calculaMatrizPorConstante(int[][] array2D, int constante) {
        int[][] matrizPorConstante = new int[array2D.length][array2D[0].length]; // cria uma nova matriz
        // com o mesmo tamanho que a matriz original

        for (int row = 0; row < array2D.length; row++) {
            for (int collumn = 0; collumn < array2D[0].length; collumn++) { //percorre cada posição da matriz
                matrizPorConstante[row][collumn] = (array2D[row][collumn]) * constante; //à mesma posição na matriz nova atribui
                // o valor da matriz original multiplicado pela constante
            }
        }
        return matrizPorConstante;
    }

    //b) adição de matrizes
    public static int[][] calculaAdicaoMatrizes(int[][] array2DA, int[][] array2DB) {
        if (array2DA.length == array2DB.length && array2DA[0].length == array2DB[0].length) {//as matrizes têm que ter a mesma ordem
            int[][] adicaoMatrizes = new int[array2DA.length][array2DA[0].length];
                for(int row=0;row< array2DA.length;row++){
                    for(int collum=0; collum< array2DA[0].length;collum++){
                        adicaoMatrizes[row][collum]=array2DA[row][collum]+array2DB[row][collum];
                    }
                }
        return adicaoMatrizes;
        }
        return null;
    }

    // c) multiplicar duas matrizes
    public static int[][] calculaProdutoMatrizes(int[][] array2DA, int array2DB[][]) {
        if (array2DA[0].length == array2DB.length) { //o numero de colunas da 1ª tem de ser igual ao numero
            // ao numero de linhas da 2ª(propriedade)
            //ficamos com uma matriz com n.º linhas de array2DA e numero de colunas de array2DB
            int[][] produtoMatrizes = new int[array2DA.length][array2DB[0].length];
            for (int row = 0; row < array2DA.length; row++) {
                for (int collumn = 0; collumn < array2DB[0].length; collumn++) {
                    for (int j = 0; j < array2DA[0].length; j++) //ciclo "extra" para iterar nas colunas de A e
                        // nas linhas de B em simultaneo mas dentro da mesma posição da nova matriz
                        produtoMatrizes[row][collumn] += array2DA[row][j] * array2DB[j][collumn];
                }
            }
            return produtoMatrizes;
        }
        return null;
    }

    //percebi mal o que era necessário para o calculo do produto de matrizes,
    //mas vou manter este metodo para alguma eventualidade
    public static int[][] criaMaiorMatrizPossivel(int[][] array2DA, int array2DB[][]) {
        int[][] maiorMatriz = new int[0][0]; //para iniciar a nova matriz

        if (array2DA.length >= array2DB.length) { // se o A tem mais linhas ou o mesmo numero de linhas
            if (array2DA[0].length >= array2DB[0].length) { // e se o A tem mais colunas
                maiorMatriz = new int[array2DA.length][array2DA[0].length]; // fica com tamanho de A
            } else maiorMatriz = new int[array2DA.length][array2DB[0].length]; //se A tem mais linhas mas menos colunas
        } else { // se o B tem mais linhas
            if (array2DB[0].length > array2DA[0].length) { // e se B tem mais colunas
                maiorMatriz = new int[array2DB.length][array2DB[0].length];
            } else maiorMatriz = new int[array2DB.length][array2DA[0].length];
        }
        return maiorMatriz;
    }
}