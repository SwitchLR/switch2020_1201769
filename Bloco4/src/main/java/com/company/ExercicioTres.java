package com.company;

public class ExercicioTres {
    public static void main(String[] args) {
        //exercicio3();
    }

    public static int sumElementsArray(int[] sequence) {
        int sumElements = 0;

        for (int i = 0; i < sequence.length; i++) {
            sumElements += sequence[i];
        }
        return sumElements;
    }
}
