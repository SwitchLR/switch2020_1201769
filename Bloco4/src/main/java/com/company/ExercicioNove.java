package com.company;

public class ExercicioNove {
    public static void main(String[] args) {
        //exercicio9();
    }

    public static int[] inverteArray(int[] sequencia) {
        int arrayInvertido[] = new int[sequencia.length];
        for (int i = 0; i < sequencia.length; i++) {
            arrayInvertido[i] = sequencia[sequencia.length - 1 - i];
        }
        return arrayInvertido;
    }

    public static boolean verificaCapicua(int[] sequencia) {
        boolean capicua = false;
        if (comparaDoisArrays(sequencia, inverteArray(sequencia))) {
            capicua = true;
        }
        return capicua;
    }

    public static boolean comparaDoisArrays(int[] array1, int[] array2) {
        if (array1.length != array2.length) {
            return false;
        }
        boolean igualValor = false;
        int i = 0;
        while (array1[i] == array2[i] && i < array1.length - 1) {
            igualValor = true;
            i++;
        }
        return igualValor;
    }
}

