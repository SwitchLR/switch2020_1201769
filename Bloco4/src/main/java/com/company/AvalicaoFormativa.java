package com.company;

public class AvalicaoFormativa {
    public static void main(String[] args) {
        //avaliacao();
    }

    public static double grauSemelhancaPalavras(String p1, String p2) {

        int correspondentes = verificaCorrespondentes(p1, p2);

        if (correspondentes == 0) {
            return 0;
        } else {
            int t = (contaTransposições(transposicoes(p1, p2),transposicoes(p2,p1)))/2;
            return ((1 / 3) * ((correspondentes / p1.length()) + (correspondentes / p2.length()) + ((correspondentes - t) / correspondentes)));
        }
    }

    public static int maiorPalavra(int tamanho1, int tamanho2) { //função que retorna o tamanho da palavra maior
        if (tamanho1 > tamanho2) {
            return tamanho1;
        } else return tamanho2;
    }
    public static int menorPalavra(int tamanho1, int tamanho2) { //função que retorna o tamanho da palavra mais pequena
        if (tamanho1 < tamanho2) {
            return tamanho1;
        } else return tamanho2;
    }

    public static int verificaCorrespondentes(String p1, String p2) { //função que conta o numero de correspondentes
        int correspondentes = 0; //inicia variavel

        char[] palavra1 = p1.toCharArray(); // coloca a palavra num array de caracteres
        char[] palavra2 = p2.toCharArray();

        int distancia = ((maiorPalavra(palavra1.length, palavra2.length)) / 2) - 1; //variavel que indica a distancia maxima
        // entre os caracateres que estamos a comparar

        for (int i = 0; i < palavra1.length; i++) { //faz o ciclo no primeiro array
            int jInicio = i - distancia; // variaveis que funcionam como iteradores no segundo array
            int jFim = i + distancia;

            if (dentroLimites(jInicio, jFim, palavra2.length)) { // verifica se o j está dentro dos limites do segundo array
                for (int j = jInicio; j < jFim; j++) {
                    if (palavra1[i] == palavra2[j]) //se a letra for igual conta o correspondente
                        correspondentes++;
                }
            } else if (palavra1[i] == palavra2[i]) //
                correspondentes++;
        }
        return correspondentes;

    }

    public static boolean dentroLimites(int jInicio, int jFim, int tamanho) { //função apenas para controlar limites
        if (jInicio >= 0 && jFim < tamanho)
            return true;

        else return false;
    }

    public static char[] transposicoes(String p1, String p2) { // metodo semelhante ao anterior
        // mas onde passamos o caracter para um array para posteriormente contar as transposições
        char[] palavra1 = p1.toCharArray();
        char[] palavra2 = p2.toCharArray();

        char[] transposicaoP1 = new char[menorPalavra(palavra1.length, palavra2.length)];

        int distancia = ((maiorPalavra(palavra1.length, palavra2.length)) / 2) - 1;
        int index = 0;
        for (int i = 0; i <menorPalavra(palavra1.length, palavra2.length); i++) {
            int jInicio = i - distancia;
            int jFim = i + distancia;

            if (dentroLimites(jInicio, jFim, palavra2.length)) {
                for (int j = jInicio; j < jFim; j++) {
                    if (palavra1[i] == palavra2[j]) {
                        transposicaoP1[index] = palavra1[i];
                        index++;
                    }
                }
            } else if (palavra1[i] == palavra2[i]) {
                transposicaoP1[index] = palavra1[i];
                index++;
            }
        }
        return transposicaoP1;
    }

    public static int contaTransposições(char[] transposicao1,char[] transposicao2){ // metodo para contra
        //as transposições entre dois arrays
        int transposicoes=0;

        for (int i = 0; i < transposicao1.length; i++) {
            if (transposicao1[i]!=transposicao2[i]){
                transposicoes++;
            }
        }
        return transposicoes;
    }



}
