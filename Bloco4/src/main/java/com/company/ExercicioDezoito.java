package com.company;

public class ExercicioDezoito {

    public static void main(String[] args) {
        //exercicio18();
    }

    public static int[] verificaErros(char[][] array2D) {
        if (array2D == null || array2D.length == 0) {

        }
        return null;
    }

    //a)
    public static int[][] criaMatrizMascara(char[][] array2D, char letra) {
        int[][] matrizMascara = new int[array2D.length][array2D[0].length];
        for (int row = 0; row < array2D.length; row++) {
            for (int collumn = 0; collumn < array2D[0].length; collumn++) {
                if (array2D[row][collumn] == letra) {
                    matrizMascara[row][collumn] = 1;
                } else matrizMascara[row][collumn] = 0;
            }
        }
        return matrizMascara;
    }

    //************************************************************************************************

    //b)

    //Metodo principal em que retorna se a palavra existe na sopa de letras e em que sentido

    public static String procuraPalavraMatriz(char[][] array2D, String palavra) {
        if (esquerdaDireita(array2D, palavra))
            return "A palavra existe no sentido; Esquerda->Direita";

        if (cimaBaixo(array2D, palavra))
            return "A palavra existe no sentido; Cima->Baixo";

        if (direitaEsquerda(array2D, palavra))
            return "A palavra existe no sentido: Direita->Esquerda";

        if (baixoCima(array2D, palavra))
            return "A palavra existe no sentido: Baixo->Cima";

        if (direitaCima(array2D, palavra))
            return "A palavra existe no sentido: Direito->Cima";

        if (direitaBaixo(array2D, palavra))
            return "A palavra existe no sentido: Direito->Baixo";

        if (esquerdaCima(array2D, palavra))
            return "A palavra existe no sentido: Esquerda->Cima";

        if (esquerdaBaixo(array2D, palavra))
            return "A palavra existe no sentido: Esquerda->Baixo";


        return "A palavra não existe";
    }

    //-----------------------------------------------------------------------------

    //este metodo faz apenas uma verificação simples no caso da palavra ser maior que os limites da sopa de letras
    //por exemplo numa matriz4x4 procurar a palavra "switch"

    public static boolean dentroDosLimites(char[][] array2D, String palavra) {
        if (palavra.length() <= array2D.length && palavra.length() <= array2D[0].length) {
            return true;
        } else return false;
    }

    //-------------------------------------------------------------------------------------------------------------
    //conjunto de oito metodos que em cada um verifica se a palavra existe numa determinada direcção

    public static boolean esquerdaDireita(char[][] sopaLetras, String palavra) { // metodo para procurar palavra no sentido esquerda -> direita
        boolean palavraEsquerdaDireita = false; // variavel que funciona como flag
        char primeiraLetra = palavra.charAt(0); // só para ficar mais legivel que vou procurar a primeira letra da String
        int controlaPalavra = 0;                // variavel para controlar o tamanho da palavra
        int jTemp = 0;                          // iterador para usar depois de encontrar a primeira letra sem alterar o 'i' original
        if (ExercicioDezoito.dentroDosLimites(sopaLetras, palavra)) { //metodo para determinar se a palavra tem um tamanho superior ao possivel
            for (int i = 0; i < sopaLetras.length; i++) {
                for (int j = 0; j < sopaLetras[0].length; j++) {

                    if (sopaLetras[i][j] == primeiraLetra) { //procura a primeira letra da String
                        controlaPalavra = 0; //reinicia o variavel de controlo no caso de ter encontrado apenas parte da String
                        jTemp = j;
                        for (int indexPalavra = 0; indexPalavra < palavra.length() && palavraEsquerdaDireita == false && jTemp < sopaLetras[0].length; indexPalavra++) {
                            if (sopaLetras[i][jTemp] == palavra.charAt(indexPalavra)) {
                                controlaPalavra++;
                                jTemp++;
                            }
                        }
                        if (controlaPalavra == palavra.length()) // pára os ciclos quando encontra a palavra completa
                            return true;
                    }
                }
            }
        }
        return false;
    }

    //-------------------------------------------------------------------------------------------------------------

    public static boolean cimaBaixo(char[][] sopaLetras, String palavra) {
        boolean palavraCimaBaixo = false;
        char primeiraLetra = palavra.charAt(0);
        int controlaPalavra = 0;
        int iTemp = 0;            //variavel
        if (ExercicioDezoito.dentroDosLimites(sopaLetras, palavra)) {
            for (int i = 0; i < sopaLetras.length; i++) {
                for (int j = 0; j < sopaLetras[0].length; j++) {
                    if (sopaLetras[i][j] == primeiraLetra) {
                        controlaPalavra = 0;
                        iTemp = i;
                        for (int indexPalavra = 0; indexPalavra < palavra.length() && palavraCimaBaixo == false && iTemp < sopaLetras.length; indexPalavra++) {
                            if (sopaLetras[iTemp][j] == palavra.charAt(indexPalavra)) {
                                controlaPalavra++;
                                iTemp++;
                            }

                        }
                        if (controlaPalavra == palavra.length())
                            return true;
                    }
                }
            }
        }
        return false;
    }
    //-------------------------------------------------------------------------------------------------------------

    public static boolean direitaEsquerda(char[][] sopaLetras, String palavra) {
        boolean palavraDireitaEsquerda = false;
        char primeiraLetra = palavra.charAt(0);
        int controlaPalavra = 0;
        int jTemp = 0;
        if (ExercicioDezoito.dentroDosLimites(sopaLetras, palavra)) {
            for (int i = 0; i < sopaLetras.length; i++) {
                for (int j = 0; j < sopaLetras[0].length; j++) {
                    if (sopaLetras[i][j] == primeiraLetra) {
                        controlaPalavra = 0;
                        jTemp = j;
                        for (int indexPalavra = 0; indexPalavra < palavra.length() && palavraDireitaEsquerda == false && jTemp >= 0; indexPalavra++) {
                            if (sopaLetras[i][jTemp] == palavra.charAt(indexPalavra)) {
                                controlaPalavra++;
                                jTemp--;
                            }
                        }
                        if (controlaPalavra == palavra.length())
                            return true;
                    }
                }
            }
        }
        return false;
    }

    //-------------------------------------------------------------------------------------------------------------

    public static boolean baixoCima(char[][] sopaLetras, String palavra) {
        boolean palavraBaixoCima = false;
        char primeiraLetra = palavra.charAt(0);
        int controlaPalavra = 0;
        int iTemp = 0;
        if (ExercicioDezoito.dentroDosLimites(sopaLetras, palavra)) {
            for (int i = 0; i < sopaLetras.length; i++) {
                for (int j = 0; j < sopaLetras[0].length; j++) {
                    if (sopaLetras[i][j] == primeiraLetra) {
                        controlaPalavra = 0;
                        iTemp = i;
                        for (int indexPalavra = 0; indexPalavra < palavra.length() && palavraBaixoCima == false && iTemp >= 0; indexPalavra++) {
                            if (sopaLetras[iTemp][j] == palavra.charAt(indexPalavra)) {
                                controlaPalavra++;
                                iTemp--;
                            }
                            if (controlaPalavra == palavra.length())
                                return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    //-------------------------------------------------------------------------------------------------------------

    public static boolean direitaCima(char[][] sopaLetras, String palavra) {
        boolean palavraDireitaCima = false;
        char primeiraLetra = palavra.charAt(0);
        int controlaPalavra = 0;
        int iTemp = 0, jTemp = 0;
        if (ExercicioDezoito.dentroDosLimites(sopaLetras, palavra)) {
            for (int i = 0; i < sopaLetras.length; i++) {
                for (int j = 0; j < sopaLetras[0].length; j++) {
                    if (sopaLetras[i][j] == primeiraLetra) {
                        controlaPalavra = 0;
                        iTemp = i;
                        jTemp = j;
                        for (int indexPalavra = 0; indexPalavra < palavra.length() && palavraDireitaCima == false && iTemp >= 0 && jTemp < sopaLetras[0].length; indexPalavra++) {
                            if (sopaLetras[iTemp][jTemp] == palavra.charAt(indexPalavra)) {
                                controlaPalavra++;
                                iTemp--;
                                jTemp++;
                            }
                            if (controlaPalavra == palavra.length())
                                return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    //-------------------------------------------------------------------------------------------------------------

    public static boolean direitaBaixo(char[][] sopaLetras, String palavra) {
        boolean palavraDireitaBaixo = false;
        char primeiraLetra = palavra.charAt(0);
        int controlaPalavra = 0;
        int iTemp = 0, jTemp = 0;
        if (ExercicioDezoito.dentroDosLimites(sopaLetras, palavra)) {
            for (int i = 0; i < sopaLetras.length; i++) {
                for (int j = 0; j < sopaLetras[0].length; j++) {
                    if (sopaLetras[i][j] == primeiraLetra) {
                        controlaPalavra = 0;
                        iTemp = i;
                        jTemp = j;
                        for (int indexPalavra = 0; indexPalavra < palavra.length() && palavraDireitaBaixo == false && iTemp < sopaLetras.length && jTemp < sopaLetras[0].length; indexPalavra++) {
                            if (sopaLetras[iTemp][jTemp] == palavra.charAt(indexPalavra)) {
                                controlaPalavra++;
                                iTemp++;
                                jTemp++;
                            }
                            if (controlaPalavra == palavra.length())
                                return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    //-------------------------------------------------------------------------------------------------------------

    public static boolean esquerdaCima(char[][] sopaLetras, String palavra) {
        boolean palavraEsquerdaCima = false;
        char primeiraLetra = palavra.charAt(0);
        int controlaPalavra = 0;
        int iTemp = 0, jTemp = 0;
        if (ExercicioDezoito.dentroDosLimites(sopaLetras, palavra)) {
            for (int i = 0; i < sopaLetras.length; i++) {
                for (int j = 0; j < sopaLetras[0].length; j++) {
                    if (sopaLetras[i][j] == primeiraLetra) {
                        controlaPalavra = 0;
                        iTemp = i;
                        jTemp = j;
                        for (int indexPalavra = 0; indexPalavra < palavra.length() && palavraEsquerdaCima == false && iTemp >= 0 && jTemp >= 0; indexPalavra++) {
                            if (sopaLetras[iTemp][jTemp] == palavra.charAt(indexPalavra)) {
                                controlaPalavra++;
                                iTemp--;
                                jTemp--;
                            }
                            if (controlaPalavra == palavra.length())
                                return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    //-------------------------------------------------------------------------------------------------------------

    public static boolean esquerdaBaixo(char[][] sopaLetras, String palavra) {
        boolean palavraEsquerdaBaixo = false;
        char primeiraLetra = palavra.charAt(0);
        int controlaPalavra = 0;
        int iTemp = 0, jTemp = 0;
        if (ExercicioDezoito.dentroDosLimites(sopaLetras, palavra)) {
            for (int i = 0; i < sopaLetras.length; i++) {
                for (int j = 0; j < sopaLetras[0].length; j++) {
                    if (sopaLetras[i][j] == primeiraLetra) {
                        controlaPalavra = 0;
                        iTemp = i;
                        jTemp = j;
                        for (int indexPalavra = 0; indexPalavra < palavra.length() && palavraEsquerdaBaixo == false && iTemp < sopaLetras.length && jTemp >= 0; indexPalavra++) {
                            if (sopaLetras[iTemp][jTemp] == palavra.charAt(indexPalavra)) {
                                controlaPalavra++;
                                iTemp++;
                                jTemp--;
                            }
                            if (controlaPalavra == palavra.length())
                                return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    //***************************************************************************************************************
    //***************************************************************************************************************

    //c) metodo que verifica se entre dois pontos existe uma palavra
    //começa por verificar num metodo auxiliar a direção através das coordenadas, depois de confirmada a direção,
    //corre a matriz apenas entre as coordenadas dadas e verifica se existe a palavra

    public static boolean confirmaPalavraEntreCoordenadas(char[][] sopaLetras, String palavra, int coordenadaLinhaInicio, int coordenadaColunaInicio, int coordenadaLinhaFim, int coordenadaColunaFim) {

        String direcao = verificaDirecao(coordenadaLinhaInicio, coordenadaColunaInicio, coordenadaLinhaFim, coordenadaColunaFim);

        int indexPalavra = 0; //iteredor para cada letra da palavra
        int contaLetras = 0;  //contador para verificar se o tamanho da palavra é igual ao numero de letras encontradas

        if (direcao == "daEsquerdaParaDireita") { // condição em função da direcção corre a matriz entre as coordenadas dadas
            for (int i = coordenadaLinhaInicio; i <= coordenadaLinhaFim; i++) {
                for (int j = coordenadaColunaInicio; j <= coordenadaColunaFim; j++) {
                    if (palavra.charAt(indexPalavra) == sopaLetras[i][j]) {
                        indexPalavra++;
                        contaLetras++;
                    }
                    if (contaLetras == palavra.length())
                        return true;
                }
            }
        }

        if (direcao == "daDireitaParaEsquerda") {
            for (int i = coordenadaLinhaInicio; i <= coordenadaLinhaFim; i++) {
                for (int j = coordenadaColunaInicio; j >= coordenadaColunaFim; j--) {
                    if (palavra.charAt(indexPalavra) == sopaLetras[i][j]) {
                        indexPalavra++;
                        contaLetras++;
                    }
                    if (contaLetras == palavra.length())
                        return true;
                }
            }
        }

        if (direcao == "deCimaParaBaixo") {
            for (int i = coordenadaLinhaInicio; i <= coordenadaLinhaFim; i++) {
                for (int j = coordenadaColunaInicio; j <= coordenadaColunaFim; j++) {
                    if (palavra.charAt(indexPalavra) == sopaLetras[i][j]) {
                        indexPalavra++;
                        contaLetras++;
                    }
                    if (contaLetras == palavra.length())
                        return true;
                }
            }
        }

        if (direcao == "deBaixoParaCima") {
            for (int i = coordenadaLinhaInicio; i >= coordenadaLinhaFim; i--) {
                for (int j = coordenadaColunaInicio; j <= coordenadaColunaFim; j++) {
                    if (palavra.charAt(indexPalavra) == sopaLetras[i][j]) {
                        indexPalavra++;
                        contaLetras++;
                    }
                    if (contaLetras == palavra.length())
                        return true;
                }
            }
        }
        //----------------------------------------------------------------------------
        //Diagonais
        //Nas diagonais coloqei as variaveis auxiliares iTemp e jTemp para o caso encontrar a palavra pelo "meio"
        //exemplo encontra a palavra "vai" nas coordenadas (0.2)-(1.3)-(1.4) e não em (0.2)-(1.3)-(2.4)
        if (direcao == "paraBaixoDireita") {
            int iTemp = coordenadaLinhaInicio, jTemp = coordenadaColunaInicio;
            for (int i = coordenadaLinhaInicio; i <= coordenadaLinhaFim; i++) {
                for (int j = coordenadaColunaInicio; j <= coordenadaColunaFim; j++) {
                    if (palavra.charAt(indexPalavra) == sopaLetras[iTemp][jTemp]) {
                        indexPalavra++;
                        contaLetras++;
                        iTemp++;
                        jTemp++;
                    }
                    if (contaLetras == palavra.length())
                        return true;
                }
            }
        }

        if (direcao == "paraBaixoEsquerda") {
            int iTemp = coordenadaLinhaInicio, jTemp = coordenadaColunaInicio;
            for (int i = coordenadaLinhaInicio; i <= coordenadaLinhaFim; i++) {
                for (int j = coordenadaColunaInicio; j >= coordenadaColunaFim; j--) {
                    if (palavra.charAt(indexPalavra) == sopaLetras[iTemp][jTemp]) {
                        indexPalavra++;
                        contaLetras++;
                        iTemp++;
                        jTemp--;
                    }
                    if (contaLetras == palavra.length())
                        return true;
                }
            }
        }

        if (direcao == "paraCimaDireita") {
            int iTemp = coordenadaLinhaInicio, jTemp = coordenadaColunaInicio;
            for (int i = coordenadaLinhaInicio; i >= coordenadaLinhaFim; i--) {
                for (int j = coordenadaColunaInicio; j <= coordenadaColunaFim; j++) {
                    if (palavra.charAt(indexPalavra) == sopaLetras[iTemp][jTemp]) {
                        indexPalavra++;
                        contaLetras++;
                        iTemp--;
                        jTemp++;
                    }
                    if (contaLetras == palavra.length())
                        return true;
                }
            }
        }

        if (direcao == "paraCimaEsquerda") {
            int iTemp = coordenadaLinhaInicio, jTemp = coordenadaColunaInicio;
            for (int i = coordenadaLinhaInicio; i >= coordenadaLinhaFim; i--) {
                for (int j = coordenadaColunaInicio; j >= coordenadaColunaFim; j--) {
                    if (palavra.charAt(indexPalavra) == sopaLetras[iTemp][jTemp]) {
                        indexPalavra++;
                        contaLetras++;
                        iTemp--;
                        jTemp--;
                    }
                    if (contaLetras == palavra.length())
                        return true;
                }
            }
        }
        return false;
    }

    //-----------------------------------------------------------------------------------------------
    //metodo auxiliar em que apenas com as coordenadas verifica a direção da eventual palavra
    //fiz metodo do tipo String para ser mais intuitivo em termos de leitura

    public static String verificaDirecao(int coordenadaLinhaInicio, int coordenadaColunaInicio, int coordenadaLinhaFim, int coordenadaColunaFim) {
        if (coordenadaLinhaInicio == coordenadaLinhaFim && coordenadaColunaInicio < coordenadaColunaFim)
            return "daEsquerdaParaDireita";
        if (coordenadaLinhaInicio == coordenadaLinhaFim && coordenadaColunaInicio > coordenadaColunaFim)
            return "daDireitaParaEsquerda";
        if (coordenadaLinhaInicio < coordenadaLinhaFim && coordenadaColunaInicio == coordenadaColunaFim)
            return "deCimaParaBaixo";
        if (coordenadaLinhaInicio > coordenadaLinhaFim && coordenadaColunaInicio == coordenadaColunaFim)
            return "deBaixoParaCima";

        if ((coordenadaLinhaFim - coordenadaLinhaInicio) == (coordenadaColunaFim - coordenadaColunaInicio) && coordenadaLinhaFim > coordenadaLinhaInicio)
            return "paraBaixoDireita";
        if ((coordenadaLinhaFim - coordenadaLinhaInicio) == -(coordenadaColunaFim - coordenadaColunaInicio) && coordenadaLinhaFim > coordenadaLinhaInicio)
            return "paraBaixoEsquerda";
        if (-(coordenadaLinhaFim - coordenadaLinhaInicio) == (coordenadaColunaFim - coordenadaColunaInicio) && coordenadaLinhaFim < coordenadaLinhaInicio)
            return "paraCimaDireita";
        if (-(coordenadaLinhaFim - coordenadaLinhaInicio) == -(coordenadaColunaFim - coordenadaColunaInicio) && coordenadaLinhaFim < coordenadaLinhaInicio)
            return "paraCimaEsquerda";

        return "Erro";
    }

    //****************************************************************************************************
    //****************************************************************************************************

    //d)
    //metodo "principal" em que se procura o numero '2' na matriz soma de duas matrizes, por é a posição onde se cruzam
    //as duas palavras


    public static int[] coordenadasEmQueDuasPalavrasCruzam(char[][] sopaLetras, String palavra1, String palavra2) {
        int[][] matrizesSomadas = new int[sopaLetras.length][sopaLetras[0].length]; // declaração da nova matriz
        int[] cordenadasCruzamento = new int[2]; // cria um array com 2 posições para colocar as coordenadas

        matrizesSomadas = ExercicioDezoito.somaDuasMatrizesMascaras(sopaLetras, palavra1, palavra2); //vai buscar o metodo auxiliar e
        // cria a soma das matrizes mascara de cada palavra

        if (ExercicioDezoito.contaOcorrenciasDeUmNumero(matrizesSomadas, 2) != 1) //metodo para retornar 'null' quando duas palavras se sobrepõem,
            //se houver mais do que um '2' na soma das duas matrizes máscara.
            return null;

        for (int row = 0; row < matrizesSomadas.length; row++) {
            for (int collumn = 0; collumn < matrizesSomadas[0].length; collumn++) {
                if (matrizesSomadas[row][collumn] == 2) { //percorre a nova matriz até encontrar um '2'
                    cordenadasCruzamento[0] = row;  //atribui o valor da linha à primeira posição do novo array
                    cordenadasCruzamento[1] = collumn; //atribui o valor da "coluna" à segunda posição do novo array
                }
            }
        }
        return cordenadasCruzamento;
    }

    //-----------------------------------------------------------------------------------------------
    //metodo para contar o numero de ocorrencias de um numero numa matriz
    public static int contaOcorrenciasDeUmNumero(int[][] matrizMascara, int numero) {
        int somaOcorrencias = 0;

        for (int row = 0; row < matrizMascara.length; row++) {
            for (int collumn = 0; collumn < matrizMascara[0].length; collumn++) {
                if (matrizMascara[row][collumn] == numero)
                    somaOcorrencias++;
            }
        }
        return somaOcorrencias;
    }

    //-----------------------------------------------------------------------------------------------
    //metodo auxiliar em que se cria uma matriz máscara para cada uma das palavras, e retorna a soma das duas

    public static int[][] somaDuasMatrizesMascaras(char[][] sopaLetras, String palavra1, String palavra2) {

        int[][] mascaraPalavra1 = new int[sopaLetras.length][sopaLetras[0].length];
        int[][] mascaraPalavra2 = new int[sopaLetras.length][sopaLetras[0].length];

        if (ExercicioDezoito.esquerdaDireita(sopaLetras, palavra1))
            mascaraPalavra1 = ExercicioDezoito.matrizMascaraEsquerdaDireita(sopaLetras, palavra1);
        if (ExercicioDezoito.cimaBaixo(sopaLetras, palavra1))
            mascaraPalavra1 = ExercicioDezoito.matrizMascaraCimaBaixo(sopaLetras, palavra1);
        if (ExercicioDezoito.direitaEsquerda(sopaLetras, palavra1))
            mascaraPalavra1 = ExercicioDezoito.matrizMascaraDireitaEsquerda(sopaLetras, palavra1);
        if (ExercicioDezoito.baixoCima(sopaLetras, palavra1))
            mascaraPalavra1 = ExercicioDezoito.matrizMascaraBaixoCima(sopaLetras, palavra1);
        if (ExercicioDezoito.direitaCima(sopaLetras, palavra1))
            mascaraPalavra1 = ExercicioDezoito.matrizMascaraDireitaCima(sopaLetras, palavra1);
        if (ExercicioDezoito.direitaBaixo(sopaLetras, palavra1))
            mascaraPalavra1 = ExercicioDezoito.matrizMascaraDireitaBaixo(sopaLetras, palavra1);
        if (ExercicioDezoito.esquerdaCima(sopaLetras, palavra1))
            mascaraPalavra1 = ExercicioDezoito.matrizMascaraEsquerdaCima(sopaLetras, palavra1);
        if (ExercicioDezoito.esquerdaBaixo(sopaLetras, palavra1))
            mascaraPalavra1 = ExercicioDezoito.matrizMascaraEsquerdaBaixo(sopaLetras, palavra1);


        if (ExercicioDezoito.esquerdaDireita(sopaLetras, palavra2))
            mascaraPalavra2 = ExercicioDezoito.matrizMascaraEsquerdaDireita(sopaLetras, palavra2);
        if (ExercicioDezoito.cimaBaixo(sopaLetras, palavra2))
            mascaraPalavra2 = ExercicioDezoito.matrizMascaraCimaBaixo(sopaLetras, palavra2);
        if (ExercicioDezoito.direitaEsquerda(sopaLetras, palavra2))
            mascaraPalavra2 = ExercicioDezoito.matrizMascaraDireitaEsquerda(sopaLetras, palavra2);
        if (ExercicioDezoito.baixoCima(sopaLetras, palavra2))
            mascaraPalavra2 = ExercicioDezoito.matrizMascaraBaixoCima(sopaLetras, palavra2);
        if (ExercicioDezoito.direitaCima(sopaLetras, palavra2))
            mascaraPalavra2 = ExercicioDezoito.matrizMascaraDireitaCima(sopaLetras, palavra1);
        if (ExercicioDezoito.direitaBaixo(sopaLetras, palavra2))
            mascaraPalavra2 = ExercicioDezoito.matrizMascaraDireitaBaixo(sopaLetras, palavra2);
        if (ExercicioDezoito.esquerdaCima(sopaLetras, palavra2))
            mascaraPalavra2 = ExercicioDezoito.matrizMascaraEsquerdaCima(sopaLetras, palavra2);
        if (ExercicioDezoito.esquerdaBaixo(sopaLetras, palavra2))
            mascaraPalavra2 = ExercicioDezoito.matrizMascaraEsquerdaBaixo(sopaLetras, palavra2);


        return ExercicioDezassete.calculaAdicaoMatrizes(mascaraPalavra1, mascaraPalavra2);
    }

    //-----------------------------------------------------------------------------------------------
    // conjunto de oito metodos muito semelhante ao utilizado na alinea b) mas onde se cria uma matriz mascara se encontrar
    //a palavra

    public static int[][] matrizMascaraEsquerdaDireita(char[][] sopaLetras, String palavra) {
        boolean palavraEsquerdaDireita = false;
        char primeiraLetra = palavra.charAt(0);
        int controlaPalavra = 0;
        int jTemp = 0;
        if (ExercicioDezoito.dentroDosLimites(sopaLetras, palavra)) {
            for (int i = 0; i < sopaLetras.length; i++) {
                for (int j = 0; j < sopaLetras[0].length; j++) {
                    if (sopaLetras[i][j] == primeiraLetra) {
                        controlaPalavra = 0; // faz reset ao controlador do tamanho da palavra
                        jTemp = j;  // iterador temporario volta ao valor de 'j'
                        int[][] matrizMascaraPalavra = new int[sopaLetras.length][sopaLetras[0].length]; //depois de encontrar a primeira letra cria uma matriz
                        //
                        for (int indexPalavra = 0; indexPalavra < palavra.length() && palavraEsquerdaDireita == false && jTemp < sopaLetras[0].length; indexPalavra++) {
                            if (sopaLetras[i][jTemp] == palavra.charAt(indexPalavra)) {
                                controlaPalavra++;
                                matrizMascaraPalavra[i][jTemp] = 1; //para cada letra da palavra coloca '1' na matriz mascara
                                jTemp++;
                            }
                        }
                        if (controlaPalavra == palavra.length())
                            return matrizMascaraPalavra; // retorna a matriz mascara assim que encontra a palavra completa
                    }
                }
            }
        }
        return null;
    }

    //----------------------------------------------------------------------------------------------

    public static int[][] matrizMascaraCimaBaixo(char[][] sopaLetras, String palavra) {
        boolean palavraCimaBaixo = false;
        char primeiraLetra = palavra.charAt(0);
        int controlaPalavra = 0;
        int iTemp = 0;            //variavel
        if (ExercicioDezoito.dentroDosLimites(sopaLetras, palavra)) {
            for (int i = 0; i < sopaLetras.length; i++) {
                for (int j = 0; j < sopaLetras[0].length; j++) {
                    if (sopaLetras[i][j] == primeiraLetra) {
                        controlaPalavra = 0;
                        iTemp = i;
                        int[][] matrizMascaraPalavra = new int[sopaLetras.length][sopaLetras[0].length];
                        for (int indexPalavra = 0; indexPalavra < palavra.length() && palavraCimaBaixo == false && iTemp < sopaLetras.length; indexPalavra++) {
                            if (sopaLetras[iTemp][j] == palavra.charAt(indexPalavra)) {
                                controlaPalavra++;
                                matrizMascaraPalavra[iTemp][j] = 1;
                                iTemp++;
                            }
                        }
                        if (controlaPalavra == palavra.length())
                            return matrizMascaraPalavra;
                    }
                }
            }
        }
        return null;
    }
    //----------------------------------------------------------------------------------------------

    public static int[][] matrizMascaraDireitaEsquerda(char[][] sopaLetras, String palavra) {
        boolean palavraDireitaEsquerda = false;
        char primeiraLetra = palavra.charAt(0);
        int controlaPalavra = 0;
        int jTemp = 0;
        if (ExercicioDezoito.dentroDosLimites(sopaLetras, palavra)) {
            for (int i = 0; i < sopaLetras.length; i++) {
                for (int j = 0; j < sopaLetras[0].length; j++) {
                    if (sopaLetras[i][j] == primeiraLetra) {
                        controlaPalavra = 0;
                        jTemp = j;
                        int[][] matrizMascaraPalavra = new int[sopaLetras.length][sopaLetras[0].length];
                        for (int indexPalavra = 0; indexPalavra < palavra.length() && palavraDireitaEsquerda == false && jTemp >= 0; indexPalavra++) {
                            if (sopaLetras[i][jTemp] == palavra.charAt(indexPalavra)) {
                                controlaPalavra++;
                                matrizMascaraPalavra[i][jTemp] = 1;
                                jTemp--;
                            }
                        }
                        if (controlaPalavra == palavra.length())
                            return matrizMascaraPalavra;
                    }
                }
            }
        }
        return null;
    }

    //--------------------------------------------------------------------------------------------
    public static int[][] matrizMascaraBaixoCima(char[][] sopaLetras, String palavra) {
        boolean palavraBaixoCima = false;
        char primeiraLetra = palavra.charAt(0);
        int controlaPalavra = 0;
        int iTemp = 0;
        if (ExercicioDezoito.dentroDosLimites(sopaLetras, palavra)) {
            for (int i = 0; i < sopaLetras.length; i++) {
                for (int j = 0; j < sopaLetras[0].length; j++) {
                    if (sopaLetras[i][j] == primeiraLetra) {
                        controlaPalavra = 0;
                        iTemp = i;
                        int[][] matrizMascaraPalavra = new int[sopaLetras.length][sopaLetras[0].length];
                        for (int indexPalavra = 0; indexPalavra < palavra.length() && palavraBaixoCima == false && iTemp >= 0; indexPalavra++) {
                            if (sopaLetras[iTemp][j] == palavra.charAt(indexPalavra)) {
                                controlaPalavra++;
                                matrizMascaraPalavra[iTemp][j] = 1;
                                iTemp--;
                            }
                            if (controlaPalavra == palavra.length())
                                return matrizMascaraPalavra;
                        }
                    }
                }
            }
        }
        return null;
    }

    //--------------------------------------------------------------------------------------------

    public static int[][] matrizMascaraDireitaCima(char[][] sopaLetras, String palavra) {
        boolean palavraDireitaCima = false;
        char primeiraLetra = palavra.charAt(0);
        int controlaPalavra = 0;
        int iTemp = 0, jTemp = 0;
        if (ExercicioDezoito.dentroDosLimites(sopaLetras, palavra)) {
            for (int i = 0; i < sopaLetras.length; i++) {
                for (int j = 0; j < sopaLetras[0].length; j++) {
                    if (sopaLetras[i][j] == primeiraLetra) {
                        controlaPalavra = 0;
                        iTemp = i;
                        jTemp = j;
                        int[][] matrizMascaraPalavra = new int[sopaLetras.length][sopaLetras[0].length];
                        for (int indexPalavra = 0; indexPalavra < palavra.length() && palavraDireitaCima == false && iTemp >= 0 && jTemp < sopaLetras[0].length; indexPalavra++) {
                            if (sopaLetras[iTemp][jTemp] == palavra.charAt(indexPalavra)) {
                                controlaPalavra++;
                                matrizMascaraPalavra[iTemp][jTemp] = 1;
                                iTemp--;
                                jTemp++;
                            }
                            if (controlaPalavra == palavra.length())
                                return matrizMascaraPalavra;
                        }
                    }
                }
            }
        }
        return null;
    }

    //--------------------------------------------------------------------------------------------

    public static int[][] matrizMascaraDireitaBaixo(char[][] sopaLetras, String palavra) {
        boolean palavraDireitaBaixo = false;
        char primeiraLetra = palavra.charAt(0);
        int controlaPalavra = 0;
        int iTemp = 0, jTemp = 0;
        if (ExercicioDezoito.dentroDosLimites(sopaLetras, palavra)) {
            for (int i = 0; i < sopaLetras.length; i++) {
                for (int j = 0; j < sopaLetras[0].length; j++) {
                    if (sopaLetras[i][j] == primeiraLetra) {
                        controlaPalavra = 0;
                        iTemp = i;
                        jTemp = j;
                        int[][] matrizMascaraPalavra = new int[sopaLetras.length][sopaLetras[0].length];
                        for (int indexPalavra = 0; indexPalavra < palavra.length() && palavraDireitaBaixo == false && iTemp < sopaLetras.length && jTemp < sopaLetras[0].length; indexPalavra++) {
                            if (sopaLetras[iTemp][jTemp] == palavra.charAt(indexPalavra)) {
                                controlaPalavra++;
                                matrizMascaraPalavra[iTemp][jTemp] = 1;
                                iTemp++;
                                jTemp++;
                            }
                            if (controlaPalavra == palavra.length())
                                return matrizMascaraPalavra;
                        }
                    }
                }
            }
        }
        return null;
    }

    //--------------------------------------------------------------------------------------------

    public static int[][] matrizMascaraEsquerdaCima(char[][] sopaLetras, String palavra) {
        boolean palavraEsquerdaCima = false;
        char primeiraLetra = palavra.charAt(0);
        int controlaPalavra = 0;
        int iTemp = 0, jTemp = 0;
        if (ExercicioDezoito.dentroDosLimites(sopaLetras, palavra)) {
            for (int i = 0; i < sopaLetras.length; i++) {
                for (int j = 0; j < sopaLetras[0].length; j++) {
                    if (sopaLetras[i][j] == primeiraLetra) {
                        controlaPalavra = 0;
                        iTemp = i;
                        jTemp = j;
                        int[][] matrizMascaraPalavra = new int[sopaLetras.length][sopaLetras[0].length];
                        for (int indexPalavra = 0; indexPalavra < palavra.length() && palavraEsquerdaCima == false && iTemp >= 0 && jTemp >= 0; indexPalavra++) {
                            if (sopaLetras[iTemp][jTemp] == palavra.charAt(indexPalavra)) {
                                controlaPalavra++;
                                matrizMascaraPalavra[iTemp][jTemp] = 1;
                                iTemp--;
                                jTemp--;
                            }
                            if (controlaPalavra == palavra.length())
                                return matrizMascaraPalavra;
                        }
                    }
                }
            }
        }
        return null;
    }

    //--------------------------------------------------------------------------------------------

    public static int[][] matrizMascaraEsquerdaBaixo(char[][] sopaLetras, String palavra) {
        boolean palavraEsquerdaBaixo = false;
        char primeiraLetra = palavra.charAt(0);
        int controlaPalavra = 0;
        int iTemp = 0, jTemp = 0;
        if (ExercicioDezoito.dentroDosLimites(sopaLetras, palavra)) {
            for (int i = 0; i < sopaLetras.length; i++) {
                for (int j = 0; j < sopaLetras[0].length; j++) {
                    if (sopaLetras[i][j] == primeiraLetra) {
                        controlaPalavra = 0;
                        iTemp = i;
                        jTemp = j;
                        int[][] matrizMascaraPalavra = new int[sopaLetras.length][sopaLetras[0].length];
                        for (int indexPalavra = 0; indexPalavra < palavra.length() && palavraEsquerdaBaixo == false && iTemp < sopaLetras.length && jTemp >= 0; indexPalavra++) {
                            if (sopaLetras[iTemp][jTemp] == palavra.charAt(indexPalavra)) {
                                controlaPalavra++;
                                matrizMascaraPalavra[iTemp][jTemp] = 1;
                                iTemp++;
                                jTemp--;
                            }
                            if (controlaPalavra == palavra.length())
                                return matrizMascaraPalavra;
                        }
                    }
                }
            }
        }
        return null;
    }

    //---------------------------------------------------------------------------------------------------


}


