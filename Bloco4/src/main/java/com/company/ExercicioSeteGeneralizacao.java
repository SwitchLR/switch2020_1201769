package com.company;

public class ExercicioSeteGeneralizacao {
    public static void main(String[] args) {
        //exercicio7Generalizacao();
    }

    public static int[] getMultipleOfNumber(int n, int m, int multiple) {

        int multipleIndex = 0;
        int[] multipleOfNumber = new int[getTotalMultiplesOfNumber(n, m, multiple)]; //para saber o tamanho do array fiz um método separado
        int[] emptyArray = new int[0];

        if (n > m) {
            for (int i = m; i < n; i++) {
                if (i % multiple == 0) {
                    multipleOfNumber[multipleIndex] = i;
                    multipleIndex++;
                }
            }
        } else if (m > n) {
            for (int i = m; i < n; i++) {
                if (i % multiple == 0) {
                    multipleOfNumber[multipleIndex] = i;
                    multipleIndex++;
                }
            }
        }
        return multipleOfNumber;
    }

    public static int getTotalMultiplesOfNumber(int n, int m, int multiple) { //metodo para contar o número de multiplos de um numero
        int countMultipleOfNumber = 0;

        if (n > m) {
            for (int i = m; i < n; i++) {
                if (i % multiple == 0) {
                    countMultipleOfNumber++;
                }
            }
        } else if (m > n) {
            for (int i = n; i < m; i++) {
                if (i % multiple == 0) {
                    countMultipleOfNumber++;
                }
            }
        } else if (n == m) {
            return -1;
        }

        return countMultipleOfNumber;
    }
}
