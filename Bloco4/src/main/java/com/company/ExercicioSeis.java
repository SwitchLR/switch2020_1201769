package com.company;

public class ExercicioSeis {
    public static void main(String[] args) {
        //exercicio6();
    }

    public static int[] getFirstNNumbers(int[] arraySequence, int n){

        int[] firstNNumbers = new int[n];
        for (int i = 0; i<n;i++){ // só faz o ciclo até ao numero pedido
            firstNNumbers[i]=arraySequence[i]; // enquanto corre o ciclo faz cópia
        }
        return firstNNumbers;
    }
}
