package Controller;

import Exceptions.ColumnIsFullException;
import Exceptions.InvalidColumnException;
import Model.Game;
import View.GameView;

public class GameLauncher {
    GameView view;
    Game game;

    public GameLauncher() {
        view = new GameView();
        game = view.makeGame();
        boolean didGameEnd=false;
        while(true){
            if(getGame().checkIfGameEnded()){
                didGameEnd = true;
                break;
            }
            view.printBoard(getGame());
            int columnToInsertIn=-1;

            try{
                columnToInsertIn=view.playTurn(game.getTurnPlayer().getName());
            } catch (NumberFormatException e1){
                System.out.println("PLEASE ENTER A NUMBER");
            }
            try{
                if(getGame().insertDisc(columnToInsertIn)){
                    break;
                }
                getGame().endTurn();
            }catch ( ColumnIsFullException e){
                System.out.println("THIS COLUMN IS FULL");
            }
            catch (InvalidColumnException e2){
                System.out.println("PLEASE SPECIFY A CORRECT COLUMN TO INSERT IN");
            }
        }
        if (didGameEnd){
            System.out.println("GAME ENDED, THERE IS NO NUMBER");
        }else{
            view.printBoard(getGame());
            System.out.println("***************************************************************");
            System.out.println("***************************************************************");
            System.out.println("" + getGame().getTurnPlayer() + "HAS WOW!!!!!!!!!!!");
            System.out.println("***************************************************************");
            System.out.println("***************************************************************");
        }
    }

    private Game getGame() {
        return game;
    }

    public static void main(String[] args) {
        new GameLauncher();
    }



}
