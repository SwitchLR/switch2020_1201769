package Model;

import Exceptions.ColumnIsFullException;
import Exceptions.InvalidColumnException;

public class Game {

    //attributes   --------------------------
    Disc[][] board;
    Player player1;
    Player player2;
    final int numberOfRows = 6;
    final int numberOfColumns = 7;
    boolean turn;

    //constructor   --------------------------

    public Game(Player player1, Player player2) {
        this.player1 = player1;  // atribui os parametros às variaveis que temos
        this.player2 = player2;
        randomizePlayers();
        board = new Disc[numberOfRows][numberOfColumns];
    }

    //Methods   --------------------------------

    public boolean checkIfGameEnded() {
        for (int i = 0; i < numberOfColumns; i++) {
            if (board[0][i] == null) {
                return false;
            }
        }
        return true;
    }

    public void randomizePlayers() {
        double randomNumber = Math.random();
        Player tempPlayer;
        if (randomNumber >= 0 && randomNumber < 0.5) {
            tempPlayer = player1;
            player1 = player2;
            player2 = tempPlayer;
        }
    }

    public void endTurn() {
        turn = !turn;
    }

    public Player getTurnPlayer() {
        if (turn) {
            return player2;
        } else {
            return player1;
        }
    }

    public boolean checkWin(int rowNumber, int columnNumber, Player turnPlayer) {
        int count = 0;

        //Right
        for (int i = rowNumber; i < numberOfRows; i++) {
            if (board[i][columnNumber].getPlayer() == turnPlayer) {
                count++;
                if (count == 4) {
                    return true;
                }
            } else {
                break;
            }
        }

        count = 0;
        //Downwards
        for (int i = rowNumber; i < numberOfColumns; i++) {
            if (board[rowNumber][i] == null) {
                break;
            }
            if (board[rowNumber][i].getPlayer() == turnPlayer) {
                count++;
                if (count == 4) {
                    return true;
                }
            } else {
                break;
            }
        }
        //Left
        for (int i = columnNumber; i >= 0; i--) {
            if (board[rowNumber][i] == null) {
                break;
            }
            if (board[rowNumber][i].getPlayer() == turnPlayer) {
                count++;
                if (count == 4) {
                    return true;
                }
            } else {
                break;
            }
        }
        count = 0;
        //Downright
        for (int i = rowNumber, j = columnNumber; i < numberOfRows && j > numberOfColumns; i++, j++) {
            if (board[i][j] == null) {
                break;
            }
            if (board[i][j].getPlayer() == turnPlayer) {
                count++;
                if (count == 4) {
                    return true;
                }
            } else {
                break;
            }
        }
        //Downleft
        for (int i = rowNumber, j = columnNumber; i < numberOfRows && j >= 0; i++, j--) {
            if (board[i][j] == null) {
                break;
            }
            if (board[i][j].getPlayer() == turnPlayer) {
                count++;
                if (count == 4) {
                    return true;
                }
            } else {
                break;
            }
        }
        count = 0;
        //Upright
        for (int i = rowNumber, j = columnNumber; i >= 0 && j < numberOfColumns; i--, j++) {
            if (board[i][j] == null) {
                break;
            }
            if (board[i][j].getPlayer() == turnPlayer) {
                count++;
                if (count == 4) {
                    return true;
                }
            } else {
                break;
            }
        }
        count = 0;
        //Upleft
        for (int i = rowNumber, j = columnNumber; i >= 0 && j >= 0; i--, j--) {
            if (board[i][j] == null) {
                break;
            }
            if (board[i][j].getPlayer() == turnPlayer) {
                count++;
                if (count == 4) {
                    return true;
                }
            } else {
                break;
            }
        }

        return false;
    }

    public boolean insertDisc(int columnNumber) throws ColumnIsFullException,InvalidColumnException {
        boolean result = false;
        boolean finished = false;
        Disc disc = null;
        if (getTurnPlayer().getType() == PlayerType.PLAYERONE) {
            disc = new PlayerOneDisc(getTurnPlayer());
        } else {
            disc = new PlayerTwoDisc(getTurnPlayer());
        }
        if (columnNumber >= numberOfColumns || columnNumber < 0) {
            throw new InvalidColumnException();
        }
        if (board[0][columnNumber] != null) {
            throw new ColumnIsFullException();
        }
        for (int i = 0; i < numberOfRows - 1; i++) {
            if (board[i + 1][columnNumber] != null) {
                board[i][columnNumber] = disc;
                result = checkWin(i, columnNumber, getTurnPlayer());
                finished = true;
                break;
            }
        }
        if (!finished) {
            board[numberOfRows - 1][columnNumber] = disc;
            result = checkWin(numberOfRows - 1, columnNumber, getTurnPlayer());

        }
        return result;
    }

    public Disc[][] getBoard() {
        return board;
    }


    public void setBoard(Disc[][] board) {
        this.board = board;
    }

}
























