package com.company;

import java.util.Scanner;


public class ExercicioQuinze {
    public static void main(String[] args) {
        exercicio15();
    }

    public static void exercicio15() {

        Scanner ler = new Scanner(System.in);

        System.out.println("Indique uma nota de 0 a 20");
        int nota = ler.nextInt();

        if (nota >= 0 && nota <= 20) {
            if (nota >= 0 && nota < 5) {
                System.out.println("Mau");
            } else if (nota > 4 && nota < 10) {
                System.out.println("Medíocre");
            } else if (nota >= 10 && nota <= 13) {
                System.out.println("Suficiente");
            } else if (nota >= 14 && nota <= 17) {
                System.out.println("Bom");
            } else if (nota >= 18 && nota <= 20) {
                System.out.println("Muito Bom");
            }
        }else
            System.out.println("Valor de nota inválido");
    }
}
