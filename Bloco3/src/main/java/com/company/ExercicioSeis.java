package com.company;

import java.util.Scanner;

public class ExercicioSeis {
    public static void main(String[] args) {
        // Exercicio6();
    }


    public static double Exercicio6a(int numero) {

        int contaDigitos = 0;
        int digito;

        while (numero > 0) {
            digito = numero % 10;
            numero = numero / 10;
            contaDigitos++;
        }

        return contaDigitos;
    }

    public static double Exercicio6b(int numero) {

        int contaPares = 0;
        int digito = 1;

        while (numero > 0) {

            digito = numero % 10;
            numero = numero / 10;

            if (digito % 2 == 0) {
                contaPares++;
            } else ;
        }

        return contaPares;
    }

    public static double Exercicio6f(int numero) {

        int somaImpares = 0;
        int digito = 1;

        while (numero > 0) {

            digito = numero % 10;
            numero = numero / 10;

            if (digito % 2 != 0) {
                somaImpares = somaImpares + digito;
            } else ;
        }

        return somaImpares;
    }

    public static double Exercicio6h(int numero) {

        int somaPares = 0;
        int contaPares = 0;
        int digito = 1;

        while (numero > 0) {

            digito = numero % 10;
            numero = numero / 10;

            if (digito % 2 == 0) {
                somaPares += digito;
                contaPares++;
            } else ;
        }

        return somaPares / contaPares;
    }

    public static double Exercicio6j(int numero) {

        int digito = 0;

        while (numero > 0) {

            digito=digito*10;

            digito += numero % 10;
            numero = numero / 10;

        }

    return digito;
    }


}


