package com.company;
import java.util.Scanner;

public class ExercicioDezassete {
    public static void main(String[] args) {
        conjuntoCaes();
    }

    public static String exercicio17(double pesoKg, double quantidadeDiariaGramas) {

        if (pesoKg < 10 && pesoKg >= 0) {
            if (quantidadeDiariaGramas == 100) {
                return "Quantidade certa";
            } else return "Quantidade errada";
        } else if (pesoKg < 25 && pesoKg >= 0) {
            if (quantidadeDiariaGramas == 250) {
                return "Quantidade certa";
            } else return "Quantidade errada";

        } else if (pesoKg < 45 && pesoKg >= 0) {
            if (quantidadeDiariaGramas == 300) {
                return "Quantidade certa";
            } else return "Quantidade errada";

        } else if (pesoKg >= 45 && pesoKg >= 0) {
            if (quantidadeDiariaGramas == 500) {
                return "Quantidade certa";
            } else return "Quantidade errada";
        } else return "Peso inválido";

    }

    public static void conjuntoCaes() {

        double pesoKg=0;
        while(pesoKg>=0) {
            Scanner ler = new Scanner(System.in);
            System.out.println("Indique o peso do cão em Kg:");
            pesoKg = ler.nextDouble();

            System.out.println("Indique a quantiade de comida diária em gramas:");
            double quantidadeDiariaGramas = ler.nextDouble();

            System.out.println(exercicio17(pesoKg, quantidadeDiariaGramas));
        };
    }

}