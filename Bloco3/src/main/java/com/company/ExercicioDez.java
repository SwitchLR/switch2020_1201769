package com.company;

import java.util.Scanner;

public class ExercicioDez {
    public static void main(String[] args) {
        Exercicio10();
    }
    public static void Exercicio10(){

        Scanner ler = new Scanner(System.in);
        System.out.println("Indique o valor limite:");
        int limite = ler.nextInt();

        int produto=1;
        int maiorValor=1;
        while(produto<=limite){

            System.out.println("Indique um novo valor:");
            int inputValor = ler.nextInt();
            produto*=inputValor;

            if (inputValor>maiorValor){
                maiorValor=inputValor;
            }
        }
        System.out.println("Ultrapassou o valor limite e o maior valor indicado foi: " + maiorValor);
    }
}
