package com.company;

import java.util.Scanner;

public class ExercicioTres {
    public static void main(String[] args) {
        Exercicio3();
    }


    public static void Exercicio3() {

        Scanner ler = new Scanner(System.in);

        int numero = 0, qtdPares = 0, qtdImpares = 0, totalImpares = 0, qtdNumeros = 0;


        do {
            System.out.println("Indique um numero:");

            numero = ler.nextInt();

            if (numero < 0) {
                System.out.println("Stop");
            } else if (numero % 2 == 0) {
                qtdPares++;
                qtdNumeros++;
            } else {
                qtdImpares++;
                totalImpares = totalImpares + numero;
                qtdNumeros++;
            }
        } while (numero > 0);

        System.out.println("A percentagem de numeros pares é:" + (double) qtdPares / qtdNumeros * 100);
        System.out.println("A média dos numeros impares é: " + (double) totalImpares / qtdImpares);
    }

//---------------------------------------------------------------------------------------------------

    public static String Exercio3ParaTestes(int[] seqNumeros) {

        double qtdPares = 0, qtdImpares = 0, totalImpares = 0, qtdNumeros = 0;

        for (int i = 0; i < seqNumeros.length && (seqNumeros[i] > 0); i++) {
            if (seqNumeros[i] % 2 == 0) {
                qtdPares++;
                qtdNumeros++;
            } else {
                qtdImpares++;
                totalImpares = totalImpares + seqNumeros[i];
                qtdNumeros++;
            }

        }
        int percentagemPares = (int) (qtdPares/qtdNumeros*100);
        int mediaImpares = (int)(totalImpares / qtdImpares);
        String resultados = "Percentagem pares " + percentagemPares + "% Media Impares " + mediaImpares;
        return resultados;
    }

}