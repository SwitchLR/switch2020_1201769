package com.company;

import java.util.Scanner;

public class ExercicioQuatorze {
    public static void main(String[] args) {
        exercicio14();

    }

    public static void exercicio14() {
        Scanner ler = new Scanner(System.in);

        System.out.println("Escolha uma das seguintes opções: \n (1) Dolar \n (2) Libra \n (3) Iene \n (4) Coroa Sueca \n (5) Franco Suiço");
        int moeda = ler.nextInt();

        System.out.println("Indique o valor a cambiar: ");
        double valorMoeda = ler.nextDouble();

        double valorCambiado = 0;
        if (moeda > 0 && moeda <= 5) {
            if (moeda == 1) {
                valorCambiado = valorMoeda * 1.534;
            } else if (moeda == 2) {
                valorCambiado = valorMoeda * 0.774;
            } else if (moeda == 3) {
                valorCambiado = valorMoeda * 161.480;
            } else if (moeda == 4) {
                valorCambiado = valorMoeda * 9.593;
            } else if (moeda == 5) {
                valorCambiado = valorMoeda * 1.601;
            }
        } else
            System.out.println("Indique uma opção válida");

        System.out.println("O valor final é: " + valorCambiado);
    }

}
