package com.company;
import java.util.Scanner;


public class ExercicioTreze {
    public static void main(String[] args) {
        exercicio13();
    }

    public static void exercicio13(){

        Scanner ler = new Scanner(System.in);

        System.out.println("Indique o código de produto");
        int codigoProduto = ler.nextInt();

        if (codigoProduto ==1){
            System.out.println("Alimento não perecível");
        }else if (codigoProduto>=2 && codigoProduto<=4){
            System.out.println("Alimento perecível");
        }else if (codigoProduto==5 || codigoProduto==6){
            System.out.println("Vestuário");
        }else if (codigoProduto==7){
            System.out.println("Higiene pessoal");
        }else if (codigoProduto>=8 && codigoProduto<=15){
            System.out.println("Limpeza e utensílios de cozinha");
        }else System.out.println("Código Inválido");

    }
}
