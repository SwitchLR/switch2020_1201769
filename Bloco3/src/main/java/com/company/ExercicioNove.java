package com.company;

import java.util.Scanner;

public class ExercicioNove {
    public static void main(String[] args) {
        Exercicio9();
    }

    public static void Exercicio9() {
        Scanner ler = new Scanner(System.in);

        int horasExtras = 0;
        int somaSalarioFinal = 0;
        int contaFuncionario = 0;

        System.out.println("Vamos processar os salários da empresa \n (O valor -1 nas horas extra termina o processamento)");

        while (horasExtras > -1) {

            System.out.println("\n Indique o numero de horas extraorinárias de um novo funcionário: ");
            horasExtras = ler.nextInt();

            if (horasExtras > -1) {

                System.out.println("Indique o salário base:");
                int salarioBase = ler.nextInt();

                System.out.println("O salário final deste funcionário é:" + salarioFinal(horasExtras, salarioBase) + "€");
                somaSalarioFinal += salarioFinal(horasExtras, salarioBase);
                contaFuncionario++;

            } else System.out.println("Processamento terminado");
        }

        System.out.println("\n A media de salários pagos este mês é: " + somaSalarioFinal / contaFuncionario + "€");
    }

    public static double salarioFinal(int horasExtras, int salarioBase) {

        return (horasExtras * 0.02 * salarioBase) + salarioBase;
    }

}