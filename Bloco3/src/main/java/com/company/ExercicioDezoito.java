package com.company;

import java.util.Scanner;


public class ExercicioDezoito {
    public static void main(String[] args) {
        exercicio18();
    }

    public static void exercicio18(){
        Scanner ler = new Scanner(System.in);

        System.out.println("Indique o Numero de identificação:");
        int numeroID = ler.nextInt();

        System.out.println("Indique o numero de controlo");
        int numeroControlo = ler.nextInt();

        int novoNumero=acrescentaAlgarismo(numeroID,numeroControlo);
        if (somaPonderada(novoNumero)%11==0){
            System.out.println("O número é válido");

        }else System.out.println("Número inválido");

    }

    public static double somaPonderada(int numeroID) {
        int soma = 0;
        for (int i = 1; i < 10; i++) {
            soma += (numeroID % 10) * i;
            numeroID = numeroID / 10;
        }
        return soma;
    }


    public static int acrescentaAlgarismo(int numero, int novoAlgarismo) {

        int numeroFinal = 0;
        numeroFinal = (numero * 10) + novoAlgarismo;
        return numeroFinal;
    }
}


