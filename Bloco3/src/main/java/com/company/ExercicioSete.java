package com.company;

public class ExercicioSete {
    public static void main(String[] args) {
        //    Exercicio7();
    }

    public static double inverteDigitos(int numero) {

        int digito = 0;

        while (numero > 0) {
            digito = digito * 10;
            digito += numero % 10;
            numero = numero / 10;

        }
        return digito;
    }

    public static boolean verificaCapicua(int numero) {

        boolean capicua;

        if (numero == inverteDigitos(numero)) {
            capicua = true;
        } else capicua = false;
        return capicua;
    }

    public static double somaCuboDigitos(int numero) {
        int digito = 0;
        int somaCubos = 0;

        while (numero > 0) {

            digito = numero % 10;
            numero = numero / 10;
            somaCubos += Math.pow(digito, 3);

        }
        return somaCubos;
    }

    public static boolean amstrong(int numero) {

        if (numero == somaCuboDigitos(numero)) {
            return true;
        } else
            return false;

    }

    public static double primeiraCapicuaNumIntervalo(int num1, int num2) {

        int primeiraCapicua = 0;

        boolean stop = false;

        if (num1 > num2) {
            for (int i = num2; i <= num1 && stop == false; i++) {
                if (verificaCapicua(i) == true) {
                    primeiraCapicua = i;
                    stop = true;
                }
            }

        } else if (num2 > num1) {
            for (int i = num1; i <= num2 && stop == false; i++) {
                if (verificaCapicua(i) == true) {
                    primeiraCapicua = i;
                    stop = true;
                }
            }
        }
        return primeiraCapicua;
    }


/*
    public static int ex7CPrimeiraCapicua(int num1, int num2) {

        int capicua1 = -1;
        for (int i = num1; i <= num2; i++) {
            if (ex7ACapicua(i) && capicua1 == -1) {
                capicua1 = i;
            }
        }
        return capicua1;
    }
 */

    public static double maiorCapicuaNumIntervalo(int num1, int num2) {

        int maiorCapicua = 0;

        if (num1 > num2) {
            for (int i = num2; i <= num1; i++) {
                if (verificaCapicua(i) == true) {
                    maiorCapicua = i;
                }
            }

        } else if (num2 > num1) {
            for (int i = num1; i <= num2; i++) {
                if (verificaCapicua(i) == true) {
                    maiorCapicua = i;
                }
            }
        }
        return maiorCapicua;
    }

    public static double contaCapicuaNumIntervalo(int num1, int num2) {

        int contaCapicua = 0;

        if (num1 > num2) {
            for (int i = num2; i <= num1; i++) {
                if (verificaCapicua(i) == true) {
                    contaCapicua++;
                }
            }

        } else if (num2 > num1) {
            for (int i = num1; i <= num2; i++) {
                if (verificaCapicua(i) == true) {
                    contaCapicua++;
                }
            }
        }
        return contaCapicua++;
    }

    public static double primeiroArmstrongNumIntervalo(int num1, int num2) {

        boolean stop = false;
        int primeiroArmstrong = 0;

        if (num1 > num2) {
            for (int i = num2; i <= num1 && stop == false; i++) {
                if (amstrong(i) == true) {
                    primeiroArmstrong = i;
                    stop = true;
                }
            }
        }

        if (num2 > num1) {
            for (int i = num1; i <= num2 && stop == false; i++) {
                if (amstrong(i) == true) {
                    primeiroArmstrong = i;
                    stop = true;
                }
            }
        }
        return primeiroArmstrong;
    }

    public static double contaArmstrongNumIntervalo(int num1, int num2) {

        int contaArmstrong = 0;

        if (num1 > num2) {
            for (int i = num2; i <= num1; i++) {
                if (amstrong(i) == true) {
                    contaArmstrong++;
                }
            }
        }

        if (num2 > num1) {
            for (int i = num1; i <= num2; i++) {
                if (amstrong(i) == true) {
                    contaArmstrong++;
                }
            }
        }
        return contaArmstrong;
    }


}


