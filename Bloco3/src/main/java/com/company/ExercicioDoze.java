package com.company;

import java.util.Scanner;


public class ExercicioDoze {
    public static void main(String[] args) {
        exercicio12();

    }

    public static void exercicio12() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Indique o valor de a");
        int a = ler.nextInt();

        System.out.println("Indique o valor de b");
        int b = ler.nextInt();

        System.out.println("Indique o valor de c");
        int c = ler.nextInt();


        System.out.println(formulaResolvente(a, b, c));
    }

    public static String formulaResolvente(int a, int b, int c) {

        int raiz = (b * b) - (4 * a * c);
        double x = ((-b) + Math.sqrt(raiz)) / (2 * a);
        double y = ((-b) - Math.sqrt(raiz)) / (2 * a);

        if (raiz > 0) {
            return "os valores são " + x + " e " + y;
        } else if (raiz == 0) {
            return "só tem uma raiz " + x;
        } else
            return "Não tem raizes reais";

    }
}
