package com.company;

public class ExercicioDezasseis {
    public static void main(String[] args) {
        //exercicio16();
    }

    public static double exercicio16(double salarioBruto) {
        double salarioLiquido = 0;

        if (salarioBruto > 0) {

            if (salarioBruto <= 500) {
                salarioLiquido = salarioBruto * 0.9;
            }
            if (salarioBruto > 500 && salarioBruto <= 1000) {
                salarioLiquido = salarioBruto * 0.85;
            }
            if (salarioBruto > 1000) {
                salarioLiquido = salarioBruto * 0.8;
            }
            return salarioLiquido;
        } else return -1;
    }
}
