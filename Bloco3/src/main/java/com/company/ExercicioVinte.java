package com.company;

import java.util.Scanner;

public class ExercicioVinte {
    public static void main(String[] args) {
        exercicio20();
    }

    public static void exercicio20() {
        Scanner ler = new Scanner(System.in);

        System.out.println("Indique um numero a classificar");
        int numero = ler.nextInt();

        System.out.println("O numero é " + classifica(numero));

    }

    public static String classifica(int numero) {
        if (perfeito(numero) == true) {
            return "Perfeito";
        } else if (abundante(numero) == true) {
            return "Abundante";
        } else return "Reduzido";

    }

    public static boolean perfeito(int numero) {

        if (numero == somaDivisores(numero)) {
            return true;
        } else return false;
    }

    public static boolean abundante(int numero) {

        if (numero < somaDivisores(numero)) {
            return true;
        } else return false;
    }

    public static boolean reduzido(int numero) {

        if (numero > somaDivisores(numero)) {
            return true;
        } else return false;
    }


    public static double somaDivisores(int numero) {
        int somaDivisores = 0;
        for (int i = 1; i < numero - 1; i++) {
            if (numero % i == 0) {
                somaDivisores += i;
            }
        }
        return somaDivisores;
    }
}
