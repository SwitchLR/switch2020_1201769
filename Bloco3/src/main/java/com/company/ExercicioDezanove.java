package com.company;

import java.util.Scanner;


public class ExercicioDezanove {
    public static void main(String[] args) {
        exercicio19();
    }


    public static void exercicio19() {
        Scanner ler = new Scanner(System.in);
        System.out.println("Indique uma sequência de algarismos");
        int numero = ler.nextInt();
        System.out.println("o número separando impares de pares é: " + separaImparesPares(numero));

    }


    public static int separaImparesPares(int numero) {

        int pares = 0, impares = 0, contaPares = 0, contaImpares = 0;

        int quantidadeDigitos = (int) contaDigitos(numero);

        for (int i = 1; i <= quantidadeDigitos; i++) {
            int digito = numero % 10;
            numero = numero / 10;

            if (digito == 0) {
                pares = (pares * 10);
                contaImpares++;                             //para contar a possição do 0
            } else if (digito % 2 == 0) {
                pares = (pares * 10) + digito;
            } else {
                impares = (impares * 10) + digito;
                contaImpares++;
            }

        }
        return (int) ((impares * Math.pow(10, contaImpares)) + pares);
    }

    public static double contaDigitos(int numero) {

        int contaDigitos = 0;
        int digito;

        while (numero > 0) {
            digito = numero % 10;
            numero = numero / 10;
            contaDigitos++;
        }

        return contaDigitos;
    }
}
