package com.company;
import java.util.Scanner;


public class ExercicioOito {
    public static void main(String[] args) {
        Exercicio8();
    }

    public static void Exercicio8(){
        Scanner ler = new Scanner(System.in);
        System.out.println("Indique o valor limite:");
        int limite = ler.nextInt();

        int soma=0;
        int menorValor=limite;
        while(soma<=limite){


            System.out.println("Indique um novo valor:");
            int inputValor = ler.nextInt();
            soma+=inputValor;

            if (inputValor<menorValor){
                menorValor=inputValor;
            }
        }
        System.out.println("Ultrapassou o valor limite e o menor valor indicado foi: " + menorValor);
    }
}

