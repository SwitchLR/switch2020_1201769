package com.company;

public class ExercicioOnze {
    public static void main(String[] args) {
        //Exercicio11();
    }

    public static double Exercicio11(int num) {

        int contaManeiras = 0;

        if (num > 0 && num <= 20) {

            for (int i = 0; i <= 10; i++) {
                for (int j = 0; j <= 10; j++) {
                    if (i + j == num) {
                        contaManeiras++;
                    }
                }
            }
        }
        return contaManeiras/2;
    }
}