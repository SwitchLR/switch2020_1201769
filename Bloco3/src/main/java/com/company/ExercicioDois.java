package com.company;

import java.util.Scanner;

public class ExercicioDois {
    public static void main(String[] args) {
        Exercicio2();
    }

    public static void Exercicio2() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Indique o número de alunos: ");
        int numeroAlunos = ler.nextInt();

        int notaPositiva = 0, notaNegativa = 0;
        double valorNegativa = 0;

        for (int i = 0; i < numeroAlunos; i++) {

            System.out.println("Indique as notas: ");
            double nota = ler.nextDouble();

            if (nota >= 10) {
                notaPositiva++;
            } else {
                notaNegativa++;
                valorNegativa = (valorNegativa + nota);
            }

        }
        System.out.println("A percentagem de positivas é: " + (double) notaPositiva / numeroAlunos * 100 + "%");
        System.out.println("A média das notas negativas é: " + ((valorNegativa / notaNegativa)) + "valores.");
    }

    //---------------------------------------------------------------------------------
    //Algoritmo proposto por colega em que é possivel fazer testes

    public static String Exercicio2logic(int alunos, double[] notas) {
        double somaNegativos = 0;
        double numNegativos = 0;
        double numPositivos = 0;

        for (int i = 0; i <= alunos; i++) {
            double nota = notas[i];
            if (nota >= 10) {
                numPositivos++;
            } else {
                numNegativos++;
                somaNegativos += nota;
            }

        }
        int resPos = (int) (numPositivos/alunos*100);
        int resNeg = (int) (somaNegativos/numNegativos);
        String result = "Positivos " + resPos + " Negativos " + resNeg;
        return result;

    }

}