package com.company;

import java.util.Scanner;

public class ExercicioUm {
    public static void main(String[] args) {
       // Exercicio1();

    }

    public static void Exercicio1() {
        long res = 1;

        Scanner ler = new Scanner(System.in);
        System.out.println("Indique um número inteiro:");

        long num = ler.nextInt();

        for (long x = num; x > 0; x--) {
            res = res * x;
        }
        System.out.println("O resultado é: " + res);
    }

    public static long getResultado(int num) {
        int res = 1;

        for (int x = num; x > 0; x--) {
            res = res * x;
        }

        return res;

    }

}
