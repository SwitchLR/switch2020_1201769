package com.company;

public class ExercicioCinco {
    public static void main(String[] args) {
        //Exercicio5();
    }

    public static double Exercicio5a(double[] intervalo) {

        double somaPares = 0;

        for (int i = 0; i < intervalo.length; i++) {

            if (intervalo[i] % 2 == 0) {
                somaPares = somaPares + intervalo[i];
            } else ;

        }
        return somaPares;
    }

    public static double Exercicio5b(double[] intervalo) {

        double contaPares = 0;

        for (int i = 0; i < intervalo.length; i++) {

            if ((intervalo[i] % 2) == 0) {
                contaPares++;
            }

        }
        return contaPares;
    }

    public static double Exercicio5c(double[] intervalo) {

        double somaImpares = 0;

        for (int i = 0; i < intervalo.length; i++) {

            if (intervalo[i] % 2 != 0) {
                //somaImpares = somaImpares + intervalo[i];
                somaImpares += intervalo[i];
            } else ;

        }
        return somaImpares;
    }

    public static double Exercicio5d(double[] intervalo) {

        double contaImpares = 0;

        for (int i = 0; i < intervalo.length; i++) {

            if (intervalo[i] % 2 != 0) {
                contaImpares++;
            } else ;

        }
        return contaImpares;
    }

    public static double Exercicio5e(double[] intervalo, int multiplo) {

        double somaMultiplos = 0;

        for (int i = 0; i < intervalo.length; i++) {
            if (intervalo[i] % multiplo == 0) {
                somaMultiplos = somaMultiplos + intervalo[i];
            } else ;

        }
        return somaMultiplos;
    }

    public static double Exercicio5f(double[] intervalo, int multiplo) {

        double produtoMultiplos = 1;

        for (int i = 0; i < intervalo.length; i++) {
            if (intervalo[i] % multiplo == 0) {
                produtoMultiplos = produtoMultiplos * intervalo[i];
            } else ;

        }
        return produtoMultiplos;
    }

    public static double Exercicio5g(int limiteInf, int limiteSup, int multiplo) {

        int intervalo[] = new int[(limiteSup - limiteInf) + 1];
        intervalo[0] = limiteInf;

        // {0,0,0,0}
        // 3,0,0,0,
        // i=0 => 3,0,0,0
        // i=1 => 3,4,0,0
        // 1=2 => 3,4,5,0
        // i=3 => 3,4,5,6
        //método preencherVetorComIncrementoDeUmPorPosicao

        for (int i = 0; i < intervalo.length; i++) {
            intervalo[i] = intervalo[0] + i;
        }

        int somaMultiplos = 0, contaMultiplos = 0;

        for (int i = 0; i < intervalo.length; i++) {
            if (intervalo[i] % multiplo == 0) {
                somaMultiplos = somaMultiplos + intervalo[i];
                contaMultiplos++;
            }
        }
        return somaMultiplos / contaMultiplos;

    }

    public static double exercicio5h(double num1, double num2, int x, int y) {
        double somaMultiplos = 0;
        int contaMultiplos = 0;

        if (num1 > num2) {
            for (int i = 0; i <= num1; i++) {
                if (i % x == 0 || i % y == 0) {
                    somaMultiplos = somaMultiplos + i;
                    contaMultiplos++;
                } else ;
            }
        } else if (num2 > num1) {
            for (int i = 0; i <= num2; i++) {
                if (i % x == 0 || i % y == 0) {
                    somaMultiplos = somaMultiplos + i;
                    contaMultiplos++;
                } else ;

            }
        } else ;
        return somaMultiplos / contaMultiplos;
    }

}