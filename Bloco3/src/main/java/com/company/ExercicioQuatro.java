package com.company;

public class ExercicioQuatro {

    public static void main(String[] args) {
        //Exercicio4a();
    }

    public static double Exercicio4a(double[] sequencia) {

        double multiploTres = 0;

        for (int i = 0; i < sequencia.length; i++) {

            if (sequencia[i] % 3 == 0) {
                multiploTres++;
            } else
                ;

        }
        return multiploTres;
    }

    public static double Exercicio4b(double[] intervalo, int numero) {

        int contaMultiplos = 0;

        for (int i = 0; i < intervalo.length; i++) {
            if (intervalo[i] % numero == 0) {
                contaMultiplos++;
            } else ;
        }
        return contaMultiplos;
    }

    public static double Exercicio4c(double[] intervaloC) {

        int contaMultiplosTresCinco = 0;
        for (int i = 0; i < intervaloC.length; i++) {
            if (intervaloC[i] % 3 == 0 && intervaloC[i] % 5 == 0) {
                contaMultiplosTresCinco++;
            } else ;
        }
        return contaMultiplosTresCinco;
    }

    public static double Exercicio4d(double[] intervaloD, int numero1, int numero2) {

        int contaMultiplosDoisNumeros = 0;

        for (int i = 0; i < intervaloD.length; i++) {
            if (intervaloD[i] % numero1 == 0 && intervaloD[i] % numero2 == 0) {
                contaMultiplosDoisNumeros++;
            } else ;
        }
        return contaMultiplosDoisNumeros;
    }

    public static double Exercicio4e(double[] intervaloE, int numero1, int numero2) {

        double somaMultiplos = 0;

        for (int i = 0; i < intervaloE.length; i++) {
            if (intervaloE[i] % numero1 == 0 && intervaloE[i] % numero2 == 0) {
                somaMultiplos = (somaMultiplos + intervaloE[i]);
            } else ;
        }
        return somaMultiplos;
    }

}
