package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezasseisTest {

    @Test
    void exercicio16Teste() {
        double salarioBruto=501;
        double expected = 425.85;

        double result = ExercicioDezasseis.exercicio16(salarioBruto);

        assertEquals(expected,result,0.001);
    }
    @Test
    void exercicio16TesteDois() {
        double salarioBruto=1500;
        double expected = 1200;

        double result = ExercicioDezasseis.exercicio16(salarioBruto);

        assertEquals(expected,result,0.001);
    }
    @Test
    void exercicio16TesteTres() {
        double salarioBruto=-200;
        double expected = -1;

        double result = ExercicioDezasseis.exercicio16(salarioBruto);

        assertEquals(expected,result,0.001);
    }

}