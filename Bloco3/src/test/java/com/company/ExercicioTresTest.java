package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioTresTest {

    @Test
    void exercio3ParaTestesTeste1() {
        int[] seqNumeros = {2, 3, 4, 5};
        String expected = "Percentagem pares " + 50 + "% Media Impares " + 4;

        String result = ExercicioTres.Exercio3ParaTestes(seqNumeros);

        assertEquals(expected, result);
    }

    @Test
    void exercio3ParaTestesTeste2() {
        int[] seqNumeros = {2, 3, 4, 5, 18, 10, 12, 6};
        String expected = "Percentagem pares " + 75 + "% Media Impares " + 4;

        String result = ExercicioTres.Exercio3ParaTestes(seqNumeros);

        assertEquals(expected, result);
    }

    @Test
    void exercio3ParaTestesTeste3() {
        int[] seqNumeros = {2, 3, 4, 5, 18, 10, -12, 6};
        String expected = "Percentagem pares " + 66 + "% Media Impares " + 4;

        String result = ExercicioTres.Exercio3ParaTestes(seqNumeros);

        assertEquals(expected, result);
    }
}