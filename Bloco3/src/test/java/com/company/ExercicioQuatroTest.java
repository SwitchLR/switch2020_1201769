package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioQuatroTest {

    @Test
    void exercicio4aTest() {
        double[] sequencia = {2, 3, 4, 5, 6, 7, 8, 9};
        double expected = 3;

        double result = ExercicioQuatro.Exercicio4a(sequencia);

        assertEquals(expected, result);
    }

    @Test
    void exercicio4bTest() {
        double[] intervalo = {2, 3, 4, 5, 6, 7, 8, 9};
        int numero = 3;
        double expected = 3;

        double result = ExercicioQuatro.Exercicio4b(intervalo, numero);

        assertEquals(expected, result);
    }

    @Test
    void exercicio4cTest() {
        double[] intervaloC = {2, 3, 4, 5, 6, 7, 8, 9};
        double expected = 0;

        double result = ExercicioQuatro.Exercicio4c(intervaloC);

        assertEquals(expected, result);
    }

    @Test
    void exercicio4cTestUm() {
        double[] intervaloC = {2, 3, 4, 5, 6, 7, 8, 9, 15};
        double expected = 1;

        double result = ExercicioQuatro.Exercicio4c(intervaloC);

        assertEquals(expected, result);
    }

    @Test
    void exercicio4dTest() {
        double[] intervaloD = {2, 3, 4, 5, 6, 7, 8, 9, 15, 20};
        int numero1 = 2;
        int numero2 = 5;
        double expected = 1;

        double result = ExercicioQuatro.Exercicio4d(intervaloD, numero1, numero2);

        assertEquals(expected, result);
    }

    @Test
    void exercicio4eTest() {
        double[] intervaloE = {2, 7, 9, 15, 20, 30, 2, 78};
        int numero1 = 2;
        int numero2 = 5;
        double expected = 50;

        double result = ExercicioQuatro.Exercicio4e(intervaloE, numero1, numero2);

        assertEquals(expected, result);
    }


}