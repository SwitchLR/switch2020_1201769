package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDozeTest {

    @Test
    void exercicio12Teste() {
        int a = 4;
        int b = 8;
        int c = 4;
        String expected = "só tem uma raiz -1.0";

        String result = ExercicioDoze.formulaResolvente(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    void exercicio12TesteDois() {
        int a = 1;
        int b = 2;
        int c = -15;
        String expected = "os valores são " + 3.0 + " e " + -5.0;

        String result = ExercicioDoze.formulaResolvente(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    void exercicio12TesteTres() {
        int a = 1;
        int b = 2;
        int c = 3;
        String expected = "Não tem raizes reais";

        String result = ExercicioDoze.formulaResolvente(a, b, c);

        assertEquals(expected, result);
    }
}