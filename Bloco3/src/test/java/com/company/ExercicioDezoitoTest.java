package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezoitoTest {

    @Test
    void acrescentaAlgarismo() {
        int numero = 123456;
        int novoAlgarismo = 7;
        int expected = 1234567;

        int result = ExercicioDezoito.acrescentaAlgarismo(numero,novoAlgarismo);

        assertEquals(expected,result);

    }
    @Test
    void somaPonderadaTest() {
        int numeroID = 1234;
        int expected = 20;

        double result= ExercicioDezoito.somaPonderada(numeroID);


        assertEquals(expected,result);

    }
}