package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezanoveTest {

    @Test
    void separaImparesParesTest() {
       int numero= 1234;
       int expected = 3142;

       int result=ExercicioDezanove.separaImparesPares(numero);

       assertEquals(expected,result);
    }

    @Test
    void separaImparesParesTestDois() {
        int numero= 12340;
        int expected = 31042;

        int result=ExercicioDezanove.separaImparesPares(numero);

        assertEquals(expected,result);
    }

    @Test
    void separaImparesParesTestTres() {
        int numero= 12034;
        int expected = 31402;

        int result=ExercicioDezanove.separaImparesPares(numero);

        assertEquals(expected,result);
    }

    @Test
    void separaImparesParesTestQuatro() {
        int numero= 1200034;
        int expected = 3140002;

        int result=ExercicioDezanove.separaImparesPares(numero);

        assertEquals(expected,result);
    }

    @Test
    void separaImparesParesTestCinco() {
        int numero= 1020304;
        int expected = 3140020;

        int result=ExercicioDezanove.separaImparesPares(numero);

        assertEquals(expected,result);
    }

    @Test
    void separaImparesParesTestSeis() {
        int numero= 010203;
        int expected = 310002;

        int result=ExercicioDezanove.separaImparesPares(numero);

        assertEquals(expected,result);
    }

}