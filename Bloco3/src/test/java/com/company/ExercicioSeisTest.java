package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioSeisTest {

    @Test
    void exercicio6aTest() {
        int numero = 123456789;
        int expected = 9;

        double result = ExercicioSeis.Exercicio6a(numero);

        assertEquals(expected, result);
    }

    @Test
    void exercicio6bTest() {
        int numero = 123456789;
        int expected = 4;

        double result = ExercicioSeis.Exercicio6b(numero);

        assertEquals(expected, result);
    }

    @Test
    void exercicio6fTest() {
        int numero = 123456789;
        int expected = 25;

        double result = ExercicioSeis.Exercicio6f(numero);

        assertEquals(expected, result);
    }

    @Test
    void exercicio6hTest() {
        int numero = 123456789;
        int expected = 5;

        double result = ExercicioSeis.Exercicio6h(numero);

        assertEquals(expected, result);
    }

    @Test
    void exercicio6jTest() {
        int numero = 123456789;
        int expected = 987654321;

        double result = ExercicioSeis.Exercicio6j(numero);

        assertEquals(expected, result);
    }

}