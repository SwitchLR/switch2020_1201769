package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioSeteTest {

    @Test
    void inverteDigitosTest() {
        int numero = 1234;
        int expected = 4321;

        double result = ExercicioSete.inverteDigitos(numero);

        assertEquals(expected, result);
    }

    @Test
    void verificaCapicuaTest() {
        int numero = 1234;
        boolean expected = false;

        boolean result = ExercicioSete.verificaCapicua(numero);

        assertEquals(expected, result);
    }

    @Test
    void verificaCapicuaTest2() {
        int numero = 8448;
        boolean expected = true;

        boolean result = ExercicioSete.verificaCapicua(numero);

        assertEquals(expected, result);
    }

    @Test
    void somaCuboDigitos() {
        int numero = 1234;
        double expected = 100;

        double result = ExercicioSete.somaCuboDigitos(numero);

        assertEquals(expected, result);
    }

    @Test
    void amstrong() {
        int numero = 153;
        boolean expected = true;

        boolean result = ExercicioSete.amstrong(numero);

        assertEquals(expected, result);
    }

    @Test
    void amstrongTestDois() {
        int numero = 12345;
        boolean expected = false;

        boolean result = ExercicioSete.amstrong(numero);

        assertEquals(expected, result);
    }

    @Test
    void primeiraCapicuaNumIntevaloTest() {
        int num1 = 400;
        int num2 = 200;
        double expected = 202;

        double result = ExercicioSete.primeiraCapicuaNumIntervalo(num1, num2);

        assertEquals(expected, result);
    }


    @Test
    void primeiraCapicuaNumIntevaloTestDois() {
        int num1 = 200;
        int num2 = 400;
        double expected = 202;

        double result = ExercicioSete.primeiraCapicuaNumIntervalo(num1, num2);

        assertEquals(expected, result);
    }

    @Test
    void maiorCapicuaNumIntevaloTest() {
        int num1 = 200;
        int num2 = 400;
        double expected = 393;

        double result = ExercicioSete.maiorCapicuaNumIntervalo(num1, num2);

        assertEquals(expected, result);
    }

    @Test
    void maiorCapicuaNumIntevaloTestDois() {
        int num1 = 0;
        int num2 = 400;
        double expected = 393;

        double result = ExercicioSete.maiorCapicuaNumIntervalo(num1, num2);

        assertEquals(expected, result);
    }

    @Test
    void contaCapicuaNumIntevaloTestDois() {
        int num1 = 0;
        int num2 = 400;
        double expected = 49;

        double result = ExercicioSete.contaCapicuaNumIntervalo(num1, num2);

        assertEquals(expected, result);
    }

    @Test
    void primeiroArmstrongNumIntervaloTest() {
        int num1 = 100;
        int num2 = 400;
        double expected = 153;

        double result = ExercicioSete.primeiroArmstrongNumIntervalo(num1, num2);

        assertEquals(expected, result);
    }

    @Test
    void primeiroArmstrongNumIntervaloTestDois() {
        int num1 = 400;
        int num2 = 100;
        double expected = 153;

        double result = ExercicioSete.primeiroArmstrongNumIntervalo(num1, num2);

        assertEquals(expected, result);
    }

    @Test
    void contaArmstrongNumIntervaloTestDois() {
        int num1 = 400;
        int num2 = 100;
        double expected = 3;

        double result = ExercicioSete.contaArmstrongNumIntervalo(num1, num2);

        assertEquals(expected, result);
    }

}


