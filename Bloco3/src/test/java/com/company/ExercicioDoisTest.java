package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDoisTest {

    @Test
    void exercicio2logicTeste() {
        int alunos = 9;
        double [] notas = {12,13,4,17,13,12,5,7,2,1};

        String expected = "Positivos 55 Negativos 3";

        String result = ExercicioDois.Exercicio2logic(alunos,notas);

        assertEquals(expected,result);

    }
}