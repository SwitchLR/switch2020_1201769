package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioCincoTest {

    @Test
    void exercicio5aTeste() {
        double[] intervalo = {2, 4, 5, 6, 8, 11, 14};
        double expected = 34;

        double result = ExercicioCinco.Exercicio5a(intervalo);

        assertEquals(expected, result);

    }

    @Test
    void exercicio5bTeste() {
        double[] intervalo = {2, 4, 5, 6, 8, 11, 14};
        double expected = 5;

        double result = ExercicioCinco.Exercicio5b(intervalo);

        assertEquals(expected, result);

    }

    @Test
    void exercicio5cTeste() {
        double[] intervalo = {2, 4, 5, 6, 8, 11, 14};
        double expected = 16;

        double result = ExercicioCinco.Exercicio5c(intervalo);

        assertEquals(expected, result);

    }

    @Test
    void exercicio5dTeste() {
        double[] intervalo = {2, 4, 5, 6, 8, 11, 14};
        double expected = 2;

        double result = ExercicioCinco.Exercicio5d(intervalo);

        assertEquals(expected, result);

    }

    @Test
    void exercicio5eTeste() {
        double[] intervalo = {2, 4, 5, 6, 8, 11, 14};
        int multiplo = 2;
        double expected = 34;

        double result = ExercicioCinco.Exercicio5e(intervalo, multiplo);

        assertEquals(expected, result);

    }

    @Test
    void exercicio5eTesteDois() {
        double[] intervalo = {15, 2, 4, 5, 6, 8, 11, 14};
        int multiplo = 3;
        double expected = 21;

        double result = ExercicioCinco.Exercicio5e(intervalo, multiplo);

        assertEquals(expected, result);

    }

    @Test
    void exercicio5fTeste() {
        double[] intervalo = {15, 2, 4, 5, 6, 8, 11, 14};
        int multiplo = 3;
        double expected = 90;

        double result = ExercicioCinco.Exercicio5f(intervalo, multiplo);

        assertEquals(expected, result);

    }

    @Test
    void exercicio5gTeste() {
        int limitInf = 10;
        int limitSup = 20;
        int multiplo = 2;
        double expected = 15;

        double result = ExercicioCinco.Exercicio5g(limitInf, limitSup, multiplo);

        assertEquals(expected, result);

    }

    @Test
    void exercicio5hTeste() {
        double num1 = 10;
        double num2 = 20;
        int x = 2;
        int y = 5;
        double expected = 10;

        double result = ExercicioCinco.exercicio5h(num1, num2, x, y);

        assertEquals(expected, result);

    }

    @Test
    void exercicio5hTesteDois() {
        double num1 = 20;
        double num2 = 10;
        int x = 2;
        int y = 5;
        double expected = 10;

        double result = ExercicioCinco.exercicio5h(num1, num2, x, y);

        assertEquals(expected, result);

    }

}