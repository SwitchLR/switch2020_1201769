package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioOnzeTest {

    @Test
    void exercicio11Test() {
        int num =5;
        int expected = 3;

        int result = (int) ExercicioOnze.Exercicio11(num);

        assertEquals(expected,result);
    }

    @Test
    void exercicio11TestDois() {
        int num =15;
        int expected = 3;

        int result = (int) ExercicioOnze.Exercicio11(num);

        assertEquals(expected,result);
    }
}