package com.company;

import com.company.ExercicioUm;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioUmTest {

    @org.junit.jupiter.api.Test
    void getResultadoTestUm() {
        int num = 5;
        long expected = 120;

        long result = ExercicioUm.getResultado(num);

        assertEquals(expected,result);

    }
}