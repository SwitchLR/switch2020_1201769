package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioVinteTest {

    @Test
    void classificaTeste() {
        int numero = 12;
        String expected = "Abundante";

        String result = ExercicioVinte.classifica(numero);

        assertEquals(expected,result);

    }

    @Test
    void classificaTesteDois() {
        int numero = 84;
        String expected = "Abundante";

        String result = ExercicioVinte.classifica(numero);

        assertEquals(expected,result);

    }

    @Test
    void classificaTesteTres() {
        int numero = 2;
        String expected = "Reduzido";

        String result = ExercicioVinte.classifica(numero);

        assertEquals(expected,result);

    }

    @Test
    void classificaTesteQuatro() {
        int numero = 496;
        String expected = "Perfeito";

        String result = ExercicioVinte.classifica(numero);

        assertEquals(expected,result);

    }
    @Test
    void classificaTesteCinco() {
        int numero = 91;
        String expected = "Reduzido";

        String result = ExercicioVinte.classifica(numero);

        assertEquals(expected,result);

    }



}