package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezasseteTest {

    @Test
    void exercicio17Teste() {
        double pesoKg = 10;
        double quantidadeDiariaGramas= 250;
        String expected = "Quantidade certa";

        String result = ExercicioDezassete.exercicio17(pesoKg,quantidadeDiariaGramas);

        assertEquals(expected,result);
    }
    @Test
    void exercicio17TesteDois() {
        double pesoKg = -300;
        double quantidadeDiariaGramas= 100;
        String expected = "Peso inválido";

        String result = ExercicioDezassete.exercicio17(pesoKg,quantidadeDiariaGramas);

        assertEquals(expected,result);
    }
    @Test
    void exercicio17TesteTres() {
        double pesoKg = 60;
        double quantidadeDiariaGramas= 100;
        String expected = "Quantidade errada";

        String result = ExercicioDezassete.exercicio17(pesoKg,quantidadeDiariaGramas);

        assertEquals(expected,result);
    }

    @Test
    void exercicio17TesteQuatro() {
        double pesoKg = 26;
        double quantidadeDiariaGramas= 400;
        String expected = "Quantidade errada";

        String result = ExercicioDezassete.exercicio17(pesoKg,quantidadeDiariaGramas);

        assertEquals(expected,result);
    }
}