package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioTrezeTest {

    @Test
    void horasServicoTest() {
        double area = 10;
        double arvores = 2;
        double arbustos = 5;
        double expected = 2;

        double result = (int) ExercicioTreze.horasServico(area, arvores, arbustos);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void horasServicoTest2() {
        double area = 0;
        double arvores = 12;
        double arbustos = 25;
        double expected = 5;

        double result = (int) ExercicioTreze.horasServico(area, arvores, arbustos);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void horasServicoTest3() {
        double area = 0;
        double arvores = 0;
        double arbustos = 0;
        double expected = 0;

        double result = (int) ExercicioTreze.horasServico(area, arvores, arbustos);

        assertEquals(expected, result, 0.0001);
    }

    @Test
    void horasServicoTest4() {
        double horas = 2;
        double expected = 20;

        double result = ExercicioTreze.custoServico(horas);

        assertEquals(expected, result, 0.0001);

    }

}