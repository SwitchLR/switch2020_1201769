package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioNoveTest {

    @Test
    void atribuicaoTeste1() {
        //arrange
        int numero = 123;
        String expected = "Crescente";

        //act
        String result = ExercicioNove.atribuicao(numero);

        //assert
        assertEquals(expected, result);

    }

    @Test
    void atribuicaoTest2() {
        //arrange
        int numero = 321;
        String expected = "Não é Crescente";

        //act
        String result = ExercicioNove.atribuicao(numero);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void atribuicaoTeste3() {
        //arrange
        int numero = 0;
        String expected = "Não é Crescente";

        //act
        String result = ExercicioNove.atribuicao(numero);

        //assert
        assertEquals(expected, result);
    }
/*
    @Test
    void crescenteTest1() {
        //arrange
        int numero = 123;
        String expected = "Crescente";

    }
    @Test
    void crescenteTest1() {
        //arrange
        int numero = 123;
        String expected = "Crescente";

    }

*/
}