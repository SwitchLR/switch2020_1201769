package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioOitoTest {

    @Test
    void restoZeroTest1() {
        //arrange
        double numero1 = 8;
        double numero2 = 2;
        String expected = numero1 + " é multiplo de " + numero2;

        //act
        String result = ExercicioOito.restoZero(numero1, numero2);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void restoZeroTest2() {
        //arrange
        double numero1 = 2;
        double numero2 = 8;
        String expected = numero2 + " é multiplo de " + numero1;

        //act
        String result = ExercicioOito.restoZero(numero1, numero2);

        //assert
        assertEquals(expected, result);


    }

    @Test
    void restoZeroTest3() {
        //arrange
        double numero1 = 55;
        double numero2 = 2;
        String expected = "Os números não são multiplos nem divisores.";

        //act
        String result = ExercicioOito.restoZero(numero1, numero2);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void restoZeroTest4() {
        //arrange
        double numero1 = 0;
        double numero2 = 2;
        String expected = "Por favor escolha números diferentes de 0";

        //act
        String result = ExercicioOito.restoZero(numero1, numero2);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void restoZeroTest5() {
        //arrange
        double numero1 = 2;
        double numero2 = 2;
        String expected = " Os números são iguais.";

        //act
        String result = ExercicioOito.restoZero(numero1, numero2);

        //assert
        assertEquals(expected, result);
    }

}