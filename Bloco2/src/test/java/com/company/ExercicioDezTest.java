package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezTest {

    @Test
    void precoSaldoTest1() {
        double preco = 201;
        double expected = 80.4;

        double result = ExercicioDez.precoSaldo(preco);

        assertEquals(expected,result);
    }

    @Test
    void precoSaldoTest2() {
        double preco = 150;
        double expected = 90;

        double result = ExercicioDez.precoSaldo(preco);

        assertEquals(expected,result);
    }

    @Test
    void precoSaldoTest3() {
        double preco = 75;
        double expected = 52.5;

        double result = ExercicioDez.precoSaldo(preco);

        assertEquals(expected,result);
    }
    @Test
    void precoSaldoTest4() {
        double preco = 25;
        double expected = 20;

        double result = ExercicioDez.precoSaldo(preco);

        assertEquals(expected,result);
    }

    @Test
    void precoSaldoTest5() {
        double preco = 0;
        double expected = -1;

        double result = ExercicioDez.precoSaldo(preco);

        assertEquals(expected,result);
    }


}