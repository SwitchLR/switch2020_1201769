package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezasseteProfTest {

    @Test
    void exercicio17Test1() {
        int hp = 14;
        int mp= 30;
        int hd= 4;
        int md = 0;
        String expected = "chegada = 18:30 hoje";

        String result = ExercicioDezasseteProf.Exercicio17(hp,mp,hd,md);

        assertEquals(expected,result);

    }


    @Test
    void exercicio17Test2() {
        int hp = 22;
        int mp= 30;
        int hd= 4;
        int md = 0;
        String expected = "chegada = 2:30 amanhã";

        String result = ExercicioDezasseteProf.Exercicio17(hp,mp,hd,md);

        assertEquals(expected,result);

    }

}