package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioQuatroTest {

    @Test
    void getTotal() {
        //arrange
        double valor = 0;
        double expected = 0;

        //act
        double result = ExercicioQuatro.getTotal(valor);

        //assert
        assertEquals(expected,result,0.001);
    }
}