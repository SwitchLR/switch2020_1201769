package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioSeisTest {

    @Test
    void getConvertido() {
        //arrange
        int totalsecond = -2;
        String expected = "0:0:-2";

        //act
        String result = ExercicioSeis.getConvertido(totalsecond);

        //assert
        assertEquals(expected, result);

    }

    @Test
    void getCumprimento() {
        //arrange
        int totalSeconds = -2;
        String expected = "Boa noite";

        //act
        String result = ExercicioSeis.getCumprimento(totalSeconds);

        //assert
        assertEquals(expected, result);

    }
}