package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioOnzeTest {

    @Test
    void getClassificacaoTurmaTesteUm() {
        double aprovados = 0.99;
        String expected = "Turma Excelente";

        String result = ExercicioOnze.getClassificacaoTurma(aprovados);

        assertEquals(expected,result);
    }
    @Test
    void getClassificacaoTurmaTesteDois() {
        double aprovados = 0;
        String expected = "Turma má";

        String result = ExercicioOnze.getClassificacaoTurma(aprovados);

        assertEquals(expected,result);
    }

    @Test
    void getClassificacaoTurmaTesteTres() {
        double aprovados = 2;
        String expected = "valor inválido";

        String result = ExercicioOnze.getClassificacaoTurma(aprovados);

        assertEquals(expected,result);
    }

    @Test
    void getClassificacaoTurmaTesteQuatro() {
        double aprovados = 0.7;
        String expected = "Turma Boa";

        String result = ExercicioOnze.getClassificacaoTurma(aprovados);

        assertEquals(expected,result);
    }

    @Test
    void getClassificacaoTurmaTesteCinco() {
        double aprovados = 0.49999;
        String expected = "Turma fraca";

        String result = ExercicioOnze.getClassificacaoTurma(aprovados);

        assertEquals(expected,result);
    }

    @Test
    void getClassificacaoTurmaTesteSeis() {
        double aprovados = 0.51;
        String expected = "Turma razoável";

        String result = ExercicioOnze.getClassificacaoTurma(aprovados);

        assertEquals(expected,result);
    }


}