package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioUmTest {

    @Test
    void getMediaPesada() {
        //arrange
        int nota1 = 10;
        int nota2 = 12;
        int nota3 = 14;
        int peso1 = 1;
        int peso2 = 1;
        int peso3 = 1;
        double expected = 12;

        //act
        double result = ExercicioUm.getMediaPesada(nota1, nota2, nota3, peso1, peso2, peso3);

        //assert
        assertEquals(expected, result, 0.1);
    }
}