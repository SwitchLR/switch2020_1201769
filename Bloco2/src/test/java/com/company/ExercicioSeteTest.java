package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioSeteTest {

    @Test
    void custoMaoObra() {
        //arrange
        double area = 50;
        double salarioDia = 80;
        double expected = 250;

        //act
        double result = ExercicioSete.custoMaoObra(area, salarioDia);

        //assert
        assertEquals(expected, result, 0.001);

    }

    @Test
    void custoTinta() {
        //arrange
        double area = 100;
        double custoLitro = 50;
        double rendimentoTinta = 10;
        double expected = 500;

        //act
        double result = ExercicioSete.custoTinta(area, custoLitro, rendimentoTinta);

        //assert
        assertEquals(expected,result,0.001);
    }

    @Test
    void custoTotal() {
        //arrange
        double valorMO = 250;
        double valorTinta = 500;
        double expected = 750;

        //act
        double result = ExercicioSete.custoTotal(valorMO,valorTinta);

        //assert
        assertEquals(expected,result,0.001);


    }
}