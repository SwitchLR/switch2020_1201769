package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDozeTest {

    @Test
    void indicePoluicaoTesteUm() {
        double ip = 0;
        String expected = "Indice de Poluição: Aceitável.";

        String result = ExercicioDoze.indicePoluicao(ip);
    }

    @Test
    void indicePoluicaoTestDois() {
        double ip = 0.3;
        String expected = "Indice de Poluição: Grupo 1 suspende actividade.";

        String result = ExercicioDoze.indicePoluicao(ip);
    }

    @Test
    void indicePoluicaoTestTres() {
        double ip = 0.45;
        String expected = "Indice de Poluição: Grupo 1 e 2 suspendem actividade.";

        String result = ExercicioDoze.indicePoluicao(ip);
    }

    @Test
    void indicePoluicaoTestQuatro() {
        double ip = 2;
        String expected = "Indice de Poluição: Grupo 1, 2 e 3 suspendem actividade.";

        String result = ExercicioDoze.indicePoluicao(ip);
    }
}