package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercioCincoProfTest {

    @Test
    void oberVolumeCuboPositivo1() {
        double expeted = 2.828;
        double result = ExercioCincoProf.oberVolumeCubo(12);
        assertEquals(expeted, result, 0.001);
    }

    @Test
    void oberVolumeCuboPositivo2() {
        double expeted = 1;
        double result = ExercioCincoProf.oberVolumeCubo(6);
        assertEquals(expeted, result, 0.001);
    }

    @Test
    void oberVolumeCuboAreaNegativa() {
        double expeted = -1;
        double result = ExercioCincoProf.oberVolumeCubo(-35);
        assertEquals(expeted, result, 0.001);
    }

    @Test
    void oberVolumeCuboAreaZero() {
        double expeted = -1;
        double result = ExercioCincoProf.oberVolumeCubo(0);
        assertEquals(expeted, result, 0.001);
    }

    @Test
    public void obterClassificacaoCubo(){
        String expected = "pequeno";
        String result = ExercioCincoProf.obterClassificacaoCuboUsandoArea(2);
        assertEquals(expected,result);

    }

}