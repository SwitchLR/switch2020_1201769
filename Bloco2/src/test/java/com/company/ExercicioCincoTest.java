package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioCincoTest {

    @Test
    void getVolume() {
        //arrange
        double area = 2;
        double expected = 0.19245008972987523;

        //act
        double result = ExercicioCinco.getVolume(area);

        //assert
        assertEquals(expected,result,0.01);
    }

}