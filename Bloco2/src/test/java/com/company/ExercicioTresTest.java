package com.company;

import org.junit.jupiter.api.Test;

import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioTresTest {

    @Test
    void pontoPlanoTest() {
        int ponto1[] = new int[2];
        int ponto2[] = new int[2];

        ponto1[0] = 2;
        ponto1[1] = 3;

        ponto2[0] = 3;
        ponto2[1] = 4;

        double result = ExercicioTres.pontoPlanoTest(ponto1, ponto2);

        double expected = 1.4142135623730951;

        assertEquals(expected,result,0.01);
    }
}