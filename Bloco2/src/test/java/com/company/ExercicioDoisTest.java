package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDoisTest {

    @Test
    void getDigito3() {
        //arrange
        int numero = 123;
        int expected = 3;
        //act

        double result = ExercicioDois.getDigito3(numero);

        //assert
        assertEquals(expected,result,0.01);
    }

    @Test
    void getDigito2() {
        //arrange
        int numero = 123;
        int expected = 2;
        //act
        double result = ExercicioDois.getDigito2(numero);

        //assert
        assertEquals(expected,result,0.001);
    }

    @Test
    void getDigito1() {
        //arrange
        int numero = 123;
        int expected = 1;
        //act
        double result = ExercicioDois.getDigito1(numero);

        //assert
        assertEquals(expected,result,0.01);
    }

    @Test
    void parOuImpar() {
        //arrange
        int numero = 200;
        boolean expected = true;
        //act
        boolean result = ExercicioDois.parOuImpar(numero);

        //assert
        assertEquals(expected,result);
    }
}