package com.company;

import java.util.Scanner;

public class ExercioCincoProf {
    public static void main(String[] args) {
        exercicio5();
    }

    //a seguinte função não é alvo de teste
    public static void exercicio5() {
        //entrada de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza a area do cubo");

        //processamento
        double area = ler.nextDouble();
        double volume = oberVolumeCubo(area);
        String classificacao = obterClassificacaoCubo(volume);

        //saida de dados
        if (volume > 0) {
            System.out.println("O volume é: " + volume);
            System.out.println("É um cubo " + classificacao);
        } else {
            System.out.println("Errado para o calculo volume");
        }
    }

    public static String obterClassificacaoCubo(double volume) {
        if (volume < 0) {
            return "Area/Volume errados";
        }
        if (volume <= 1) {
            return "pequeno";
        } else if (volume > 1 && volume <= 2) {
            return "medio";
        } else {
            return "grande";
        }
    }

   //a seguinte função é alvo de teste
    public static double oberVolumeCubo(double area) {
        if (area > 0) {
            double aresta = Math.sqrt(area / 6);
            double volume = Math.pow(aresta, 3);
            return volume;
        } else {
            //retorna -1 se for area invalida
            return -1;
        }
    }
    public static String obterClassificacaoCuboUsandoArea(double area){
        double volume = oberVolumeCubo(area);
        return obterClassificacaoCubo(volume);
    }


}

