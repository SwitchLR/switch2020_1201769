package com.company;

import java.util.Scanner;

public class ExercicioSeis {
    public static void main(String[] args) {
        converteSegundos();
    }

    public static void converteSegundos() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Indique o total de segundos: ");
        int totalSeconds = ler.nextInt();

        if (totalSeconds >= 0 && totalSeconds <= 86400) {
            System.out.println(getConvertido(totalSeconds));
            System.out.println(getCumprimento(totalSeconds));
        } else
            System.out.println("Valor fora do intervalo!");
    }


    public static String getConvertido(int totalSeconds) {
        int hour = totalSeconds / 3600;
        int minute = (totalSeconds % 3600) / 60;
        int second = (totalSeconds % 3600) % 60;

        return hour + ":" + minute + ":" + second;
    }

    public static String getCumprimento(int totalSeconds) {
        if (totalSeconds >= 21600 && totalSeconds <= 43201) {
            return "Bom dia";
        } else if (totalSeconds > 43201 && totalSeconds <= 72001) {
            return "Boa tarde";
        } else
            return "Boa noite";
    }

}
