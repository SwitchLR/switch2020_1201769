package com.company;

public class ExercicioTreze {
    public static void main(String[] args) {
        //horasServico();
        //custoServico();
    }

    public static double horasServico(double area, double arvores, double arbustos) {

        double segundosGrama = area * 300;
        double segundosArvores = arvores * 600;
        double segundosArbustos = arbustos * 400;

        double horas = (int) Math.round((segundosGrama + segundosArvores + segundosArbustos) / 3600);

        return horas;
    }

    public static double custoServico(double horas) {

        double custo = 10 * horas;
        return custo;
    }

}