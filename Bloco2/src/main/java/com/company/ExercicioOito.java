package com.company;

import java.util.Scanner;

public class ExercicioOito {
    public static void main(String[] args) {

        multiplos();
    }

    public static void multiplos() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Indique o valor do primeiro numero:");
        double numero1 = ler.nextDouble();

        System.out.println("Indique o valor do segundo numero");
        double numero2 = ler.nextDouble();

        System.out.println(restoZero(numero1, numero2));

    }

    public static String restoZero(double numero1, double numero2) {

        if (numero1 == numero2) {
            return " Os números são iguais.";
        } else if (numero1 == 0 || numero2 == 0) {
            return "Por favor escolha números diferentes de 0";
        } else if ((numero1 % numero2) == 0) {
            return numero1 + " é multiplo de " + numero2;
        } else if ((numero2 % numero1) == 0) {
            return numero2 + " é multiplo de " + numero1;
        } else
            return "Os números não são multiplos nem divisores.";
    }
}