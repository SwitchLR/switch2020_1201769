package com.company;

import java.util.Scanner;

public class ExercicioUm {
    public static void main(String[] args) {
        notas();
    }

    public static double notas() {
        int nota1, nota2, nota3, peso1, peso2, peso3;
        double mediaPesada;

        Scanner ler = new Scanner(System.in);
        System.out.println("Indique o valor da 1ª nota:");
        nota1 = ler.nextInt();

        System.out.println("Indique o valor da 2ª nota:");
        nota2 = ler.nextInt();

        System.out.println("Indique o valor da 3ª nota:");
        nota3 = ler.nextInt();

        System.out.println("Indique o peso da 1ª nota:");
        peso1 = ler.nextInt();

        System.out.println("Indique o peso da 2ª nota:");
        peso2 = ler.nextInt();

        System.out.println("Indique o peso da 3ª nota:");
        peso3 = ler.nextInt();

        mediaPesada = getMediaPesada(nota1, nota2, nota3, peso1, peso2, peso3);

        System.out.println("A media é: " + mediaPesada);


        if (mediaPesada >= 8) {
            System.out.println("O aluno cumpre a nota mínima exigida.");
        } else {
            System.out.println("O aluno não cumpre a nota mínima exigida.");
        }
        return mediaPesada;

    }

    public static double getMediaPesada(int nota1, int nota2, int nota3, int peso1, int peso2, int peso3) {
        double mediaPesada;
        mediaPesada = ((nota1 * peso1) + (nota2 * peso2) + (nota3 * peso3)) / (peso1 + peso2 + peso3);
        return mediaPesada;
    }
}
