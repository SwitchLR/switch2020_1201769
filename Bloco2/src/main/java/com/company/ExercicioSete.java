package com.company;

import java.util.Scanner;

public class ExercicioSete {
    public static void main(String[] args) {
        custoPinturaEdificio();
    }

    public static void custoPinturaEdificio() {


        Scanner ler = new Scanner(System.in);
        System.out.println("Indique o área que pretende pintar:");
        double area = ler.nextDouble();

        System.out.println("Indique o custo do litro de tinta:");
        double custoLitro = ler.nextDouble();

        System.out.println("Indique o rendimento do litro de tinta:");
        double rendimentoTinta = ler.nextDouble();

        System.out.println("Indique o salário por dia de cada pintor:");
        double salarioDia = ler.nextDouble();


        if (area > 0) {
            System.out.println("O custo da mão de obra é: " + custoMaoObra(area, salarioDia) + "€");
            System.out.println("O custo da tinta é: " + custoTinta(area, custoLitro, rendimentoTinta) + "€");
            System.out.println("O custo total da obra é: " + custoTotal(custoMaoObra(area, salarioDia), custoTinta(area, custoLitro, rendimentoTinta)) + "€");

        } else
            System.out.println("O valor da área tem de ser superior a 0");

    }

    public static double custoMaoObra(double area, double salarioDia) {

        if (area > 0 && area < 100) {
            double valorMO = ((area / 2) * (salarioDia / 8));
            return valorMO;
        } else if (area >= 100 && area < 300) {
            double valorMO = ((area / (2 * 2)) * (salarioDia / 8));
            return valorMO;
        } else if (area >= 300 && area < 1000) {
            double valorMO = ((area / (2 * 3)) * (salarioDia / 8));
            return valorMO;
        } else {
            double valorMO = ((area / (2 * 4)) * (salarioDia / 8));
            return valorMO;
        }
    }

    public static double custoTinta(double area, double custoLitro, double rendimentoTinta) {
        if (custoLitro >= 0 && rendimentoTinta > 0) {
            double valorTinta = ((area / rendimentoTinta) * custoLitro);
            return valorTinta;
        } else
            return -1;

    }

    public static double custoTotal(double valorMO, double valorTinta) {
        double total = valorMO + valorTinta;
        return total;

    }
}


   /* alternativa

    public static double custoMaoObra(double area, double salarioDia) {
        int numeroTrabalhadores
        if (area > 0 && area < 100) {
            numeroTrabalhadores = 1;
        } else if (area >= 100 && area < 300) {
            numeroTrabalhadores = 2;
        } else if (area >= 300 && area < 1000) {
            numeroTrabalhores = 3;
        } else {
            numeroTrabalhadores = 4;
        }
        double valorMO = ((area / (2 * 4)) * (salarioDia / 8));
        return valorMO;
*/
