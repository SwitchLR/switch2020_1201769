package com.company;

import java.util.Scanner;

public class ExercicioQuatro {
    public static void main(String[] args) {
        valores();
    }

    public static void valores() {

        double valor, total;

        Scanner ler = new Scanner(System.in);
        System.out.println("Indique o valor da variável ");
        valor = ler.nextDouble();


        if (valor < 0) {
            System.out.println(" o valor é " + valor);

        } else if (valor == 0) {
            System.out.println(" o valor é " + valor);

        } else {
            total = ((Math.pow(valor, 2)) - (2 * valor));

            System.out.println(" o valor é " + total);
        }

    }

    public static double getTotal(double valor) {
        double total;
        total = ((Math.pow(valor, 2)) - (2 * valor));
        return total;
    }

}
