package com.company;

import java.util.Scanner;

public class ExercicioCinco {
    public static void main(String[] args) {
        volume();
    }

    public static void volume() {
        double area, aresta, volume;

        Scanner ler = new Scanner(System.in);
        System.out.println("introduza o valor da área");
        area = ler.nextDouble();

        if (area > 0) {
            aresta = Math.sqrt(area / 6);
            volume = Math.pow(aresta, 3);
            System.out.println("Volume do cubo = " + volume);
            if (volume <= 1) {
                System.out.println("cubo pequeno");
            } else if (volume > 1 && volume <= 2) {
                System.out.println("cubo médio");
            } else {
                System.out.println("cubo grande");
            }
        } else {
            System.out.println("Valor da área incorrecto");
        }

    }

    public static double getVolume(double area) {
        if (area < 0) {
            double aresta = Math.sqrt(area / 6);
            double volume = Math.pow(aresta, 3);
            return volume;
        } else { //no caso de a area ser < 0 retorna -1
            return -1;
        }
    }

    public static String getClassificacao(double volume) {

        if (volume <= 1) {
            return "cubo pequeno";
        } else if (volume > 1 && volume <= 2) {
            return "cubo médio";
        } else {
            return "cubo grande";
        }

    }
}

