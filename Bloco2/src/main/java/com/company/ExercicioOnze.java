package com.company;
import java.util.Scanner;

public class ExercicioOnze {
    public static void main(String[] args) {

        //classificacaoTurma();
        determinaLimite();
    }


/*
    public static void classificacaoTurma(){
        Scanner ler = new Scanner(System.in);
        System.out.println("Indique os alunos aprovados:");
        double aprovados = ler.nextDouble();

        if (aprovados<0 || aprovados >1){
            System.out.println("valor inválido");
        }else if(aprovados<0.2){
            System.out.println("Turma má");
        }else if(aprovados<0.5){
            System.out.println("Turma fraca");
        }else if(aprovados<0.7){
            System.out.println("Turma razoável");
        }else if(aprovados<0.9){
            System.out.println("Turma Boa");
        }else
            System.out.println("Turma Excelente");

    }
*/
    public static void determinaLimite(){
        Scanner ler = new Scanner(System.in);

        System.out.println("Indique o limite turma má");
        double turmaMa = ler.nextDouble();

        System.out.println("Indique o limite turma fraca");
        double turmaFraca = ler.nextDouble();

        System.out.println("Indique o limite turma razoável");
        double turmaRazoavel = ler.nextDouble();

        System.out.println("Indique o limite turma boa");
        double turmaBoa = ler.nextDouble();

        System.out.println("Indique o limite turma Excelente");
        double turmaExcelente = ler.nextDouble();

        System.out.println("Indique os alunos aprovados:");
        double aprovados = ler.nextDouble();

        if (aprovados<0 || aprovados >1){
            System.out.println("valor inválido");
        }else if(aprovados<turmaMa){
            System.out.println("Turma má");
        }else if(aprovados<turmaFraca){
            System.out.println("Turma fraca");
        }else if(aprovados<turmaRazoavel){
            System.out.println("Turma razoável");
        }else if(aprovados<turmaBoa){
            System.out.println("Turma Boa");
        }else
            System.out.println("Turma Excelente");

    }


   public static String getClassificacaoTurma(double aprovados){
       if (aprovados<0 || aprovados >1){
          return "valor inválido";
       }else if(aprovados<0.2){
           return "Turma má";
       }else if(aprovados<0.5){
           return "Turma fraca";
       }else if(aprovados<0.7){
           return "Turma razoável";
       }else if(aprovados<0.9){
           return "Turma Boa";
       }else
           return "Turma Excelente";

   }

}
