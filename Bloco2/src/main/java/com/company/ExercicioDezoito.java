package com.company;

public class ExercicioDezoito {

    public static void main(String[] args) {
        //exercicicio18();
    }


    public static String exercicio18(int hi, int mi, int si, int sproc) {

        int tempoInicial = hi * 3600 + mi * 60 + si;
        int tempoFinal = tempoInicial + sproc;

        int hf = tempoFinal / 3600;
        int mf = (tempoFinal % 3600) / 60;
        int sf = (tempoFinal % 3600) % 60;

        return hf + ":" + mf + ":" + sf;
    }
}


//  sproc -> segundos processamento

