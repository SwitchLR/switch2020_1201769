package com.company;

public class ExercicioDoze {
    public static void main(String[] args) {
        //indicePoluicao();
    }

    public static String indicePoluicao(double ip) {
        if (ip < 0.3) {
            return "Indice de Poluição: Aceitável.";
        }else if(ip<0.4){
            return "Indice de Poluição: Grupo 1 suspende actividade.";
        }else if(ip<0.5){
            return "Indice de Poluição: Grupo 1 e 2 suspendem actividade.";
        }else
            return  "Indice de Poluição: Grupo 1, 2 e 3 suspendem actividade.";

    }

}
