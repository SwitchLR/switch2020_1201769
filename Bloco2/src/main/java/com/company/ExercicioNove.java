package com.company;

import java.util.Scanner;

public class ExercicioNove {
    public static void main(String[] args) {
        crescente();
    }

    public static void crescente() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Indique um numero com 3 digitos:");
        int numero = ler.nextInt();

        if (numero >= 100 && numero <= 999) {
            System.out.println(atribuicao(numero));
        } else
            System.out.println("indique um número valido");
    }


    public static String atribuicao(int numero) {
        int resto2 = numero % 100;
        int resto3 = numero % 10;

        int digito1 = numero / 100;
        int digito2 = resto2 / 10;
        int digito3 = resto3;

        if (digito1 < digito2 && digito2 < digito3) {
            return "Crescente";
        } else
            return "Não é Crescente";

        //return digito1 + " " + digito2 + " " + digito3 + ".";
    }
}

  /*
    public static String verificaCrescente (){
        if (digito1<digito2 && digito2<digito3){
            return "Crescente";
        }else
            return "Não é Crescente";

    }

   */
