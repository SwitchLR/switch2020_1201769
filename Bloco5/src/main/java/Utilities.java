

public class Utilities {

    public static int sumRowArray(int[] array) {
        int sumRow = 0;
        for (int i = 0; i < array.length; i++) {
            sumRow += array[i];
        }
        return sumRow;
    }

    public static int highestArrayValue(int[] array) {
        int maximum = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] > maximum)
                maximum = array[i];
        }
        return maximum;
    }

    public static boolean squareMatrix(Vector[] matrix) {
        for (Vector vector : matrix) {
            if (vector.numberElements() != matrix.length)
                return false;
        }
        return true;
    }

    public static boolean rectangularMatrix(Vector[] matrix) {
        if (squareMatrix(matrix))
            return false;

        for (int i = 0; i < matrix.length - 1; i++) {
            if (matrix[i].numberElements() != matrix[i + 1].numberElements())
                return false;
        }
        return true;
    }

    public static int[][] transposedMatrix(int[][] array2D) {
        int[][] matrizTransposta = new int[array2D.length][array2D[0].length];
        for (int row = 0; row < array2D.length; row++) {
            for (int collumn = 0; collumn < array2D[0].length; collumn++) {
                matrizTransposta[row][collumn] = array2D[collumn][row];
            }
        }
        return matrizTransposta;
    }

    public static int countDigits(int number) {
        int count = 0;
        int digit;

        while (number > 0) {
            digit = number % 10;
            number = number / 10;
            count++;
        }

        return count;
    }

    public static double percentageOfEvenDigits(int numero) {
        double contaDigitos = 0;
        double contaPares = 0;
        double digito = 1;

        while (numero > 0) {
            digito = numero % 10;
            numero = numero / 10;
            contaDigitos++;
            if (digito % 2 == 0) {
                contaPares++;
            }
        }
        return contaPares / contaDigitos;
    }

    public static int[] inverteArray(int[] sequencia) {
        int arrayInvertido[] = new int[sequencia.length];
        for (int i = 0; i < sequencia.length; i++) {
            arrayInvertido[i] = sequencia[sequencia.length - 1 - i];
        }
        return arrayInvertido;
    }

    public static int[][] invertMatrixColumns(int[][] matrix) {
        int[][] invertedColumns = new int[matrix.length][matrix[0].length];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                invertedColumns[j][i] = matrix[matrix[i].length - 1 - j][i];
            }
        }
        return invertedColumns;
    }


    //verifica qual a linha com mais colunas numa matriz
    public static int highestRow(int[][] matrix) {
        int numColums = 0;
        for (int i = 0; i < matrix.length; i++) {
            if (matrix[i].length > numColums)
                numColums = matrix[i].length;
        }

        return numColums;
    }


    public static int[] naoRepetidosArray(int[] array) {
        int[] naoRepetidos = new int[array.length];
        int contaNaoRepetidos = 0;
        int j = 0;

        //se um valor do array original não existir no array naoRepetidos, guarda-o
        for (int index = 0; index < array.length; index++) {
            if (encontraValorArray(array[index], naoRepetidos) == false) {
                naoRepetidos[j] = array[index];
                j++;
                contaNaoRepetidos++;
            }
        }
        //este if serve apenas para controlar o facto de o array de naoReptidos inicializar cheio de '0'
        //se houver um '0' no array original coloca-o no final do novo array
        if (encontraValorArray(0, array)) {
            contaNaoRepetidos++;
            naoRepetidos[contaNaoRepetidos] = 0;
        }
        return getFirstNNumbers(naoRepetidos, contaNaoRepetidos);
    }

    //procura um valor num array
    public static boolean encontraValorArray(int n, int[] array) {
        boolean encontraValor = false;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == n) {
                encontraValor = true;
            }
        }
        return encontraValor;
    }

    public static int[] getFirstNNumbers(int[] arraySequence, int n) {

        int[] firstNNumbers = new int[n];
        for (int i = 0; i < n; i++) { // só faz o ciclo até ao numero pedido
            firstNNumbers[i] = arraySequence[i]; // enquanto corre o ciclo faz cópia
        }
        return firstNNumbers;
    }

    //passa os valores da coluna para um array




}
