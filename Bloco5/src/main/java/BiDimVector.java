import java.text.Bidi;
import java.util.Arrays;

public class BiDimVector {
    //attributes

    private Vector[] vectorBi;

    //constructor
    //a) Construtor público em que o array encapsulado fica vazio (i.e. sem valores).

    public BiDimVector() {
        Vector[] novoBiDimVector = new Vector[0];
        this.vectorBi = novoBiDimVector;
    }

    //b) Construtor público que permita inicializar o array encapsulado com alguns valores.
    public BiDimVector(Vector[] biVector) {
        if (!isValid(biVector)) {
            throw new IllegalArgumentException("O vector não pode ser null");
        }

        Vector[] novoBiDimVector = new Vector[biVector.length];
        for (int i = 0; i < biVector.length; i++) {
            novoBiDimVector[i] = biVector[i];
        }
        this.vectorBi = novoBiDimVector;
        //this.vectorBi = new Vector[biVector.length];
        //this.vectorBi = copyBiDimVectorOriginal(biVector);
    }

    public BiDimVector(int[][] biVector) {
        if (!isValidInt(biVector)) {
            throw new IllegalArgumentException("O vector não pode ser null");
        }
        Vector[] novoBiDimVector = new Vector[biVector.length];
        for (int i = 0; i < biVector.length; i++) {
            Vector aux = new Vector(biVector[i]);
            novoBiDimVector[i] = aux;
        }
        this.vectorBi = novoBiDimVector;
    }

    public int[][] toArray() {
        int[][] copyBiDimVector = new int[this.vectorBi.length][];
        for (int i = 0; i < this.vectorBi.length; i++) {
            Vector aux = new Vector(this.vectorBi[i].toArray());
            copyBiDimVector[i] = aux.toArray();
        }
        return copyBiDimVector;
    }

    public int[][] toMatrix() {
        int[][] matrix = new int[this.vectorBi.length][];

        for (int i = 0; i < this.vectorBi.length; i++) {
            matrix[i] = new int[this.vectorBi[i].numberElements()];
            for (int j = 0; j < this.vectorBi[i].numberElements(); j++) {
                matrix[i][j] = this.vectorBi[i].valueByPosition(j);
            }
        }
        return matrix;
    }

    public Vector[] copyBiDimVectorOriginal(Vector[] originalVector) {
        Vector[] copyMatrix = new Vector[originalVector.length];
        for (int i = 0; i < originalVector.length; i++) {
            copyMatrix[i] = originalVector[i];
        }
        return copyMatrix;
    }

    private Vector[] fromMatrixToArrayOfVector(int[][] matrix) {
        Vector[] vector = new Vector[matrix.length];
        for (int i = 0; i < matrix.length; i++) {
            vector[i] = new Vector(matrix[i]);
        }
        return vector;
    }

    public boolean isValid(Vector[] biVector) {
        if (biVector == null) {
            return false;
        }
        return true;
    }

    public boolean isValidInt(int[][] biVector) {
        if (biVector == null) {
            return false;
        }
        return true;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (obj instanceof BiDimVector == false)
            return false;

        BiDimVector outro = (BiDimVector) obj;
        if (this.vectorBi.length != outro.vectorBi.length)
            return false;

        boolean isEqual = true;
        for (int i = 0; i < this.vectorBi.length && isEqual; i++) {
            isEqual = this.vectorBi[i].equals(outro.vectorBi[i]);
        }
        return isEqual;
    }


    public Vector[] copyBiDimVector() {
        Vector[] copyMatrix = new Vector[this.vectorBi.length];
        for (int i = 0; i < this.vectorBi.length; i++) {
            copyMatrix[i] = this.vectorBi[i];
        }
        return copyMatrix;
    }

    public Vector[] toVector() {
        Vector[] copyMatrix = new Vector[this.vectorBi.length];
        for (int i = 0; i < copyMatrix.length; i++) {
            copyMatrix[i] = this.vectorBi[i];
        }
        return copyMatrix;
    }

    //c) Adicione um novo elemento (valor) ao array encapsulado numa determinada linha, criando assim
    //uma nova coluna nessa linha.

    public void addValueInSpecificLine(int line, int value) {
        if (line < 0 || line > this.vectorBi.length)
            throw new IllegalArgumentException("A linha escolhida está fora dos limites da matriz");

        this.vectorBi[line].addValue(value);
    }

    //d) Retire o primeiro elemento do array encapsulado com um determinado valor (percorrendo
    //primeiramente as linhas). A linha onde o elemento for retirado, ficará com menos uma coluna.


    public void removeValueFromVector(int value) {

        for (int i = 0; i < this.vectorBi.length; i++) {
            int dimension = this.vectorBi[i].numberElements();
            this.vectorBi[i].removeElement(value);
            if (this.vectorBi[i].numberElements() != dimension)
                i = this.vectorBi.length;
        }
    }

    //e) Retorne True caso a matriz esteja vazia e False em caso contrário.
    public boolean emptyMatrix() {
        boolean empty = true;
        for (int i = 0; i < this.vectorBi.length; i++) {
            if (this.vectorBi[i] != null)
                empty = false;
        }
        return empty;
    }

    //f) Retorne o maior elemento do array.

    public int maxValue() {
        int maximum;
        Vector maxValues = new Vector();
        for (Vector vector : this.vectorBi) {
            maxValues.addValue(vector.maxElement());
        }
        maximum = maxValues.maxElement();
        return maximum;
    }


    //g) Retorne o menor elemento do array.

    public int minValue() {
        int mininum;
        Vector minValues = new Vector();
        for (Vector vector : this.vectorBi) {
            minValues.addValue(vector.minElement());
        }
        mininum = minValues.minElement();
        return mininum;
    }

    //h) Retorne a média dos elementos do array.

    public int averageValue() {
        int sum = 0;
        int count = 0;
        for (Vector vector : this.vectorBi) {
            for (int value : vector.toArray()) {
                sum += value;
                count++;
            }
        }
        return sum / count;
    }

    // i) Retorne um vetor em que cada posição corresponde à soma dos elementos da linha homóloga
    //do array encapsulado.

    public Vector sumRowVector() {

        Vector sumRow = new Vector();
        for (Vector row : this.vectorBi) {
            int sum = Utilities.sumRowArray(row.toArray());
            sumRow.addValue(sum);
        }
        return sumRow;
    }

    // j) Retorne um vetor em que cada posição corresponde à soma dos elementos
    // da coluna homóloga do array encapsulado.

    public Vector sumColumnVector() {
        int sumColumn = 0;

        int[] aux = new int[columnLength()];
        //Vector sumColumnVector = new Vector();
        for (Vector row : this.vectorBi) {
            //row.toArray();
            for (int i = 0; i < row.numberElements(); i++) {
                aux[i] += row.toArray()[i];
            }
        }
        return new Vector(aux);
    }

    private int columnLength() {
        int columnLength = this.vectorBi[0].numberElements();

        for (Vector vector : this.vectorBi) {
            if (columnLength < vector.numberElements())
                columnLength = vector.numberElements();
        }
        return columnLength;
    }

    // k) Retorne o índice da linha do array com maior soma dos respetivos elementos. Deve ser usado o
    //método da alínea i).

    public int highestSumIndex() {
        int[] sumRows = sumRowVector().toArray();
        for (int i = 0; i < sumRows.length; i++) {
            if (sumRows[i] == Utilities.highestArrayValue(sumRows))
                return i;
        }
        return -1;
    }

    // l) Retorne True se o array encapsulado corresponde a uma matriz quadrada.

    public boolean squareArrayVector() {
        return Utilities.squareMatrix(this.vectorBi);
    }

    //m) Retorne True se o array encapsulado corresponde a uma matriz quadrada simétrica.

    public boolean symmetricalSquareArrayVector() {
        if (!squareArrayVector())
            return false;

        int[][] matrix = toMatrix();
        int[][] transposed = Utilities.transposedMatrix(matrix);

        return Arrays.deepEquals(matrix, transposed);
    }

    // n) Retorne a quantidade de elementos não nulos na diagonal principal da matriz (se quadrada) ou -
    // 1 (se não for quadrada).

    public int notZeroElementsInDiagonal() {
        if (!squareArrayVector())
            return -1;

        int count = 0;

        int[][] matrix = toMatrix();
        for (int i = 0; i < matrix.length; i++) {
            if (matrix[i][i] != 0)
                count++;
        }
        return count;
    }

    // o) Retorne True caso a diagonal principal e a secundária sejam iguais, i.e. tenham os mesmos
    //elementos e pela mesma ordem.

    public Vector mainDiagonal() {
        int[][] diagonalArray = toArray();
        Vector diagonal = new Vector();
        for (int i = 0; i < diagonalArray.length; i++) {
            diagonal.addValue(diagonalArray[i][i]);
        }
        return diagonal;
    }

    public Vector secondaryDiagonal() {
        int[][] secondaryDiagonalArray = toArray();
        Vector secondary = new Vector();
        for (int i = 0; i < secondaryDiagonalArray.length; i++) {
            secondary.addValue(secondaryDiagonalArray[i][secondaryDiagonalArray.length - i - 1]);
        }
        return secondary;
    }

    public boolean sameDiagonals() {
        if (!squareArrayVector() && !Utilities.rectangularMatrix(this.vectorBi))
            return false;

        return mainDiagonal().equals(secondaryDiagonal());
    }

    // p) Retorne um vetor com todos os elementos do array encapsulado cujo número de algarismos é
    // superior ao número médio de algarismos de todos os elementos do array.

    public Vector superiorToAverage() {
        Vector superior = new Vector();
        int average = averageValueBiDimVector();
        for (Vector row : this.vectorBi) {
            for (int i = 0; i < row.numberElements(); i++) {
                if (row.toArray()[i] > average)
                    superior.addValue(row.toArray()[i]);
            }
        }
        return superior;
    }

    private int averageValueBiDimVector() {
        int sum = 0;
        int count = 0;
        for (Vector row : this.vectorBi) {
            for (int i = 0; i < row.numberElements(); i++) {
                sum += row.toArray()[i];
                count++;
            }
        }
        return sum / count;
    }

    // q) Retorne um vetor com todos os elementos do array cuja percentagem de algarismos pares é
    // superior à média da percentagem de algarismos pares de todos os elementos do array.

    public Vector evenPercentageSuperiorToAverage() {
        Vector evenSuperior = new Vector();
        double evenAverage = evenAverageMatrix();
        for (Vector row : this.vectorBi) {
            for (int i = 0; i < row.numberElements(); i++) {
                if (Utilities.percentageOfEvenDigits(row.toArray()[i]) > evenAverage)
                    evenSuperior.addValue(row.toArray()[i]);
            }
        }
        return evenSuperior;
    }

    private double evenAverageMatrix() {
        double evenSum = 0;
        double count = 0;
        for (Vector row : this.vectorBi) {
            for (int i = 0; i < row.numberElements(); i++) {
                evenSum += row.countEvenDigits(row.toArray()[i]);
                count += Utilities.countDigits(row.toArray()[i]);
            }
        }
        return evenSum / count;
    }

    // r) Inverta a ordem dos valores de cada linha do array encapsulado.

    public void invertRow() {

        int[][] matrix = toMatrix();

        int[][] invertedRows = new int[this.vectorBi.length][];
        for (int i = 0; i < this.vectorBi.length; i++) {
            invertedRows[i] = Utilities.inverteArray(matrix[i]);
        }

        this.vectorBi = fromMatrixToArrayOfVector(invertedRows);
    }


    // s) Inverta a ordem dos valores de cada coluna do array encapsulado.

    public void invertColumnsVector() {
        int[][] matrix = toMatrix();

        int[][] invertedRows = Utilities.invertMatrixColumns(matrix);

        this.vectorBi = fromMatrixToArrayOfVector(invertedRows);
    }

    // t) Rode 90º os valores do array encapsulado.

    public void turnRightNinetyDegres() {
        int[][] matrix = toMatrix();

        int[][] transposedMatrix = Utilities.transposedMatrix(matrix);

        Vector[] transposedVectorArray = fromMatrixToArrayOfVector(transposedMatrix);

        Vector[] aux = new Vector[this.vectorBi.length];
        aux = invertElements(transposedVectorArray);
        this.vectorBi = aux;
    }

    public Vector[] invertElements(Vector[] vector) {
        Vector[] right = new Vector[vector.length];

        int count = 0;
        int countVector = 0;
        for (Vector row : vector) {
            Vector aux = new Vector(row.numberElements());
            for (int i = row.numberElements() - 1; i >= 0; i--) {
                aux.addToIndex(count, row.valueByPosition(i));
                count++;
            }
            count = 0;
            right[countVector] = aux;
            countVector++;
        }
        return right;
    }



    // u) Rode 180º os valores do array encapsulado.
    public void turnAround() {

        invertColumnsVector();
        invertElements();

    }

    public void invertElements() {

        int count = 0;
        int countVector = 0;
        for (Vector row : this.vectorBi) {
            Vector aux = new Vector(row.numberElements());
            for (int i = row.numberElements() - 1; i >= 0; i--) {
                aux.addToIndex(count, row.valueByPosition(i));
                count++;
            }
            count = 0;
            this.vectorBi[countVector] = aux;
            countVector++;
        }
    }

    // v) Rode -90º os valores do array encapsulado.

    public void turnLeftNinetyDegrees() {
        int[][] matrix = toMatrix();

        int[][] transposedMatrix = Utilities.transposedMatrix(matrix);

        Vector[] transposedVectorArray = fromMatrixToArrayOfVector(transposedMatrix);

        Vector[] invert = invertVector(transposedVectorArray);

        this.vectorBi = invert;
    }

    public Vector[] invertVector(Vector[] vector) {

        Vector[] aux = new Vector[vector.length];
        int count = 0;

        for (int i = vector.length - 1; i >= 0; i--) {
            aux[count] = vector[i];
            count++;
        }
        return aux;
    }

}



