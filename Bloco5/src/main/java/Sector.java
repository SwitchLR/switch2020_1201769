public class Sector {
    //attributes

    private int[][] matrix;

    //constructor

    public Sector(int[][] matrix) {

        this.matrix = copyMatrix(matrix);
    }


    public int[][] copyMatrix(int[][] matrix) {

        int highestNumColumns = Utilities.highestRow(matrix);

        int[][] board = new int[matrix.length][highestNumColumns];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                board[i][j] = matrix[i][j];
            }
        }
        return board;
    }

    public int[] getSector(int row, int column) {
        int[] sector = new int[2];
        sector[0] = row / 3;
        sector[1] = column / 3;

        return sector;
    }

    public int [] fromSectorToArray(int[] sector) {
        int[] aux = new int[9];
        int indexAux = 0;
        for (int i = 0; i < this.matrix.length; i++) {
            for (int j = 0; j < this.matrix[i].length; j++) {
                if (i/3 == sector[0] && j/3 == sector[1]) {
                    aux[indexAux] = this.matrix[i][j];
                    indexAux++;
                }
            }
        }
        return aux;
    }


}
