public class Resolution {
    //attributes
    private int[][] gameMatrix;

    //constructor


    public Resolution(int[][] gameMatrix) {

        if (!isValid(gameMatrix))
            throw new IllegalArgumentException("Invalid Board");

        this.gameMatrix = copyBoard(gameMatrix);
    }

    //methods


    public int[][] copyBoard(int[][] gameMatrix) {

        int highestNumColumns = Utilities.highestRow(gameMatrix);

        int[][] board = new int[gameMatrix.length][highestNumColumns];
        for (int i = 0; i < gameMatrix.length; i++) {
            for (int j = 0; j < gameMatrix[i].length; j++) {
                board[i][j] = gameMatrix[i][j];
            }
        }
        return board;
    }

    public boolean isValid(int[][] gameMatrix) {

        if (gameMatrix == null)
            return false;

        if (gameMatrix.length != 9)
            return false;

        for (int i = 0; i < gameMatrix.length; i++) {
            if (gameMatrix[i].length != 9)
                return false;
        }

        for (int i = 0; i < gameMatrix.length; i++) {
            for (int j = 0; j < gameMatrix[i].length; j++) {
                if (gameMatrix[i][j] < 0 || gameMatrix[i][j] > 9)
                    return false;
            }
        }
        return true;
    }

    public boolean resolved() {

        int [][] columnsInRow = RowOrColumn.fromColumnToArray(this.gameMatrix);

        for (int i = 0; i < this.gameMatrix.length; i++) {
            if (!RowOrColumn.verifyArray(this.gameMatrix[i]))
                return false;

            if(!RowOrColumn.verifyArray(columnsInRow[i]))
                return false;
        }

        return true;
    }



   /* private void maskMatrix() {
        int[][] maskMatrix = new int[9][9];
        for (int i = 0; i < this.gameMatrix.length; i++) {
            for (int j = 0; j < this.gameMatrix[0].length; j++) {
                if (this.gameMatrix[i][j] == 0) {
                    maskMatrix[i][j] = 0;
                } else maskMatrix[i][j] = 1;
            }
        }
    }*/

}
