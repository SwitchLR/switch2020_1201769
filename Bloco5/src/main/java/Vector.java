import java.util.Arrays;

public class Vector {
    //attributes

    private int[] vector;

    //a)
    //construtor
    public Vector() {
        int[] copy = new int[0];
        this.vector = copy;
    }

    //b)
    public Vector(int[] vector) {
        if (!isValid(vector)) {
            throw new IllegalArgumentException("O vector não pode ser nulo");
        } else this.vector = vector;
/*
        int[] copy = new int[this.vector.length];
        for (int i = 0; i < this.vector.length; i++) {
            copy[i] = this.vector[i];
        }
        this.vector = copy;*/
    }

    public Vector(int size) {
        //  if (!isValid(vector)) {
        //    throw new IllegalArgumentException("O vector não pode ser nulo");
        this.vector = new int[size];
    }

/*
    public Vector(int[] vector) {
        if (!isValid(vector)) {
            throw new IllegalArgumentException("O vector não pode ser nulo");
        } else {
            this.vector = new int[vector.length];
          //  System.arraycopy(vector.length,0,this.vector,0,vector.length);
        }
    }
*/

    //methods

    //função auxiliar para controlo de excepções
    public boolean isValid(int[] array) {
        if (array == null) {
            return false;
        } else return true;
    }


    public int[] toArray() {
        return Arrays.stream(this.vector).toArray();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vector vector1 = (Vector) o;
        return Arrays.equals(vector, vector1.vector);
    }


    //c)

    public void addValue(int value) {

        if (!isValid(this.vector))
            throw new IllegalArgumentException("O vector não pode ser nulo");

        int[] newVector = new int[this.vector.length + 1];
        System.arraycopy(this.vector, 0, newVector, 0, this.vector.length); //função para fazer copia do array
        newVector[newVector.length - 1] = value;

        this.vector = newVector;
    }

    public Vector copyVector() {
        int[] newVector = new int[this.vector.length];
        System.arraycopy(this.vector, 0, newVector, 0, this.vector.length);
        Vector copy = new Vector(newVector);
        return copy;
    }

    /* public Vector addValue(int value) {

         if (!isValid(this.vector))
             throw new IllegalArgumentException("O vector não pode ser nulo");

         Vector newVector = new Vector();
         System.arraycopy(this.vector, 0, newVector, 0, this.vector.length); //função para fazer copia do array
         newVector.addToIndex(this.vector.length-1,value);

         return newVector;
     }
 */
    public void addToIndex(int index, int value) {
        this.vector[index] = value;
    }

    //d)
  /*  public void removeElement(int element) {
        int index = 0;
        for (int i = 0; i < this.vector.length; i++) {
            if (this.vector[i] == element) {
                index = i;
                i = this.vector.length; //break
            }
        }
        int[] result = new int[this.vector.length - 1]; //
        System.arraycopy(this.vector, 0, result, 0, index); //copia o vector até à posição do elemento
        System.arraycopy(this.vector, index + 1, result, index, this.vector.length - index - 1); // copia o array apartir do elemento

        this.vector = result;
    }
*/
    public void removeElement(int element) {
        int index = 0;
        for (int i = 0; i < this.vector.length; i++) {
            if (this.vector[i] == element) {
                index = i;

                int[] result = new int[this.vector.length - 1]; //
                System.arraycopy(this.vector, 0, result, 0, index); //copia o vector até à posição do elemento
                System.arraycopy(this.vector, index + 1, result, index, this.vector.length - index - 1); // copia o array apartir do elemento

                i = this.vector.length; //break
                this.vector = result;
            }
        }
    }


    public boolean existsInArray(int element) { // função que verifica se um determinado elemento está no array
        for (int i = 0; i < this.vector.length; i++) {
            if (this.vector[i] == element)
                return true;
        }
        return false;
    }

    //e)
    public int valueByPosition(int position) {
        if (!isValid(this.vector))
            throw new IllegalArgumentException("O vector não pode ser nulo ou vazio");

        return this.vector[position];
    }


    //f)
    public int numberElements() {
        if (!isValid(this.vector))
            throw new IllegalArgumentException("O vector não pode ser nulo ou vazio");

        return this.vector.length;

        /*int count = 0;
        for (int i = 0; i < this.vector.length; i++) {
            count++;
        }
        return count;
        */
    }

    //g)
    public int maxElement() {
        if (!isValid(this.vector))
            throw new IllegalArgumentException("O vector não pode ser nulo ou vazio");

        int maximo = 0;
        for (int i = 0; i < this.vector.length; i++) {
            if (this.vector[i] > maximo)
                maximo = this.vector[i];
        }
        return maximo;
    }

    //h)
    public int minElement() {
        if (!isValid(this.vector))
            throw new IllegalArgumentException("O vector não pode ser nulo ou vazio");

        int minimo = this.vector[0];
        for (int i = 0; i < this.vector.length; i++) {
            if (this.vector[i] < minimo)
                minimo = this.vector[i];
        }
        return minimo;
    }

    //i)
    public int averageOfElements() {
        if (!isValid(this.vector))
            throw new IllegalArgumentException("O vector não pode ser nulo ou vazio");

        int sum = 0;
        int count = 0;
        for (int i = 0; i < this.vector.length; i++) {
            sum += this.vector[i];
            count++;
        }
        return sum / count;
    }

    //j)
    public int averageOfEvenElements() {
        if (!isValid(this.vector))
            throw new IllegalArgumentException("O vector não pode ser nulo ou vazio");

        int sum = 0;
        int count = 0;
        for (int i = 0; i < this.vector.length; i++) {
            if (this.vector[i] % 2 == 0) {
                sum += this.vector[i];
                count++;
            }
        }
        return sum / count;
    }

    //k)
    public int averageOfOddElements() {
        if (!isValid(this.vector))
            throw new IllegalArgumentException("O vector não pode ser nulo ou vazio");

        int sum = 0;
        int count = 0;
        for (int i = 0; i < this.vector.length; i++) {
            if (this.vector[i] % 2 != 0) {
                sum += this.vector[i];
                count++;
            }
        }
        return sum / count;
    }

    //l)
    public int averageOfElementMultiples(int element) {
        if (!isValid(this.vector))
            throw new IllegalArgumentException("O vector não pode ser nulo ou vazio");

        int sum = 0;
        int count = 0;
        for (int i = 0; i < this.vector.length; i++) {
            if (this.vector[i] % element == 0) {
                sum += this.vector[i];
                count++;
            }
        }
        return sum / count;
    }

    //m)
    public int[] orderAscendent() {
        if (!isValid(this.vector))
            throw new IllegalArgumentException("O vector não pode ser nulo ou vazio");

        int temp = 0;
        for (int i = 0; i < this.vector.length; i++) {
            for (int j = i + 1; j < this.vector.length; j++)
                if (this.vector[i] > this.vector[j]) {
                    temp = this.vector[i];
                    this.vector[i] = this.vector[j];
                    this.vector[j] = temp;
                }
        }
        return this.vector;
    }

    public int[] orderDescendent() {
        if (!isValid(this.vector))
            throw new IllegalArgumentException("O vector não pode ser nulo ou vazio");

        int temp = 0;
        for (int i = 0; i < this.vector.length; i++) {
            for (int j = i + 1; j < this.vector.length; j++)
                if (this.vector[i] < this.vector[j]) {
                    temp = this.vector[i];
                    this.vector[i] = this.vector[j];
                    this.vector[j] = temp;
                }
        }
        return this.vector;
    }

    //n)
    public boolean isEmpty() {
        return this.vector.length == 0;
    }

    //o)
    public boolean oneElementArray() {
        return this.vector.length == 1;
    }

    //p)
    public boolean arrayOfEvens() {
        if (!isValid(this.vector))
            throw new IllegalArgumentException("O vector não pode ser nulo ou vazio");

        int count = 0;
        for (int i = 0; i < this.vector.length; i++) {
            if (this.vector[i] % 2 == 0)
                count++;
        }

        if (count == this.vector.length)
            return true;
        else return false;
    }

    //q)
    public boolean arrayOfOdds() {
        if (!isValid(this.vector))
            throw new IllegalArgumentException("O vector não pode ser nulo ou vazio");

        int count = 0;
        for (int i = 0; i < this.vector.length; i++) {
            if (this.vector[i] % 2 != 0)
                count++;
        }

        if (count == this.vector.length)
            return true;
        else return false;
    }

    //r)
    public boolean arrayWithRepeatedElements() {

        for (int i = 0; i < this.vector.length; i++) {
            for (int j = i + 1; j < this.vector.length; j++) {
                if (this.vector[i] == this.vector[j])
                    return true;
            }
        }
        return false;
    }

    //s)
    public static int countDigits(int number) {
        int count = 0;
        int digit;

        while (number > 0) {
            digit = number % 10;
            number = number / 10;
            count++;
        }

        return count;
    }

    //s)
    public Vector elementsWithHigherNumberOfDigitsThanAverage() {
        int totalDigits = 0;

        for (int i = 0; i < this.vector.length; i++) { //corre uma vez o vector para contar o total de digitos no vector
            totalDigits += countDigits(this.vector[i]);
        }
        int averageOfDigits = totalDigits / this.vector.length; //calcula a média de digitos

        Vector higherNumberOfDigitsAboveAverage = new Vector();

        for (int i = 0; i < this.vector.length; i++) {
            if (countDigits(this.vector[i]) > averageOfDigits) { //verifica se o numero de digitos de um elemento é
                higherNumberOfDigitsAboveAverage.addValue((this.vector[i]));
            }
        }
        return higherNumberOfDigitsAboveAverage;
    }


    //t)Retorne os elementos do vetor cuja percentagem de algarismos pares é superior à média da percentagem --------
    // de algarismos pares de todos os elementos do vetor -----------------------------------------------------------

    public Vector evenDigitsPercentageSuperiorAverageEvenDigitsAllElements() {
        double totalEvenDigits = 0;
        double totalDigits = 0;

        for (int i = 0; i < this.vector.length; i++) {
            totalEvenDigits += countEvenDigits(this.vector[i]);
            totalDigits += countDigits(this.vector[i]);
        }
        double averageEvenDigits = totalEvenDigits / totalDigits;

        Vector evenDigitsPercentageSuperiorAverageEvenDigitsAllElements = new Vector();

        for (int i = 0; i < this.vector.length; i++) {
            if (percentageOfEvenDigits(this.vector[i]) > averageEvenDigits) {
                evenDigitsPercentageSuperiorAverageEvenDigitsAllElements.addValue(this.vector[i]);
            }
        }
        return evenDigitsPercentageSuperiorAverageEvenDigitsAllElements;
    }

    public double percentageOfEvenDigits(int numero) {
        double contaDigitos = 0;
        double contaPares = 0;
        double digito = 1;

        while (numero > 0) {
            digito = numero % 10;
            numero = numero / 10;
            contaDigitos++;
            if (digito % 2 == 0) {
                contaPares++;
            }
        }
        return contaPares / contaDigitos;
    }

    public int countEvenDigits(int numero) {
        int contaPares = 0;
        int digito = 1;

        while (numero > 0) {

            digito = numero % 10;
            numero = numero / 10;

            if (digito % 2 == 0) {
                contaPares++;
            } else ;
        }
        return contaPares;
    }

    //u) Retorne os elementos do vetor compostos exclusivamente por algarismos pares. ---------------------------

    public Vector onlyEvenDigits() {
        Vector onlyEvenDigits = new Vector();
        for (int i = 0; i < this.vector.length; i++) {
            if (percentageOfEvenDigits(this.vector[i]) == 1)
                onlyEvenDigits.addValue(this.vector[i]);
        }
        return onlyEvenDigits;
    }

    //v) Retorne os elementos que são sequências crescentes (e.g. 347) do vetor. ---------------------------------

    public Vector ascendentDigits() {
        Vector ascendentDigits = new Vector();

        for (int i = 0; i < this.vector.length; i++) {
            if (numberWithAscendingDigits(this.vector[i]))
                ascendentDigits.addValue(this.vector[i]);
        }

        return ascendentDigits;
    }

    public static int[] fromNumberToArrayOfDigits(int number) {
        int size = countDigits(number);
        int[] arrayOfDigits = new int[size];

        for (int i = size - 1; i >= 0; i--) {
            arrayOfDigits[i] = number % 10;
            number = number / 10;
        }
        return arrayOfDigits;
    }

    public static boolean numberWithAscendingDigits(int number) {

        int[] arrayOfDigits = fromNumberToArrayOfDigits(number);

        for (int i = 0, j = i + 1; i < arrayOfDigits.length - 1; i++, j++) {
            if (arrayOfDigits[i] > arrayOfDigits[j])
                return false;
        }
        return true;
    }

    //w) // retorna as capicuas existentes no vector ---------------------------------------------------------

    public Vector palindromes() {
        Vector palindromes = new Vector();
        for (int i = 0; i < this.vector.length; i++) {
            if (verifyPalindrome(this.vector[i]))
                palindromes.addValue(this.vector[i]);
        }

        return palindromes;
    }

    public static int reverseDigits(int number) {
        int digit = 0;

        while (number > 0) {
            digit = digit * 10;
            digit += number % 10;
            number = number / 10;
        }
        return digit;
    }

    public static boolean verifyPalindrome(int number) {
        if (number == reverseDigits(number)) {
            return true;
        }
        return false;
    }

    //x) Retorne os números existentes no vetor compostos exclusivamente por um mesmo algarismo (e.g. 222). ----------

    public Vector sameDigitsNumbers() {
        Vector sameDigitVector = new Vector();
        for (int i = 0; i < this.vector.length; i++) {
            if (sameDigit(this.vector[i]))
                sameDigitVector.addValue(this.vector[i]);
        }
        return sameDigitVector;
    }


    public static boolean sameDigit(int number) {
        int[] sameDigitArray = fromNumberToArrayOfDigits(number);

        for (int i = 0, j = i + 1; i < sameDigitArray.length - 1; i++, j++) {
            if (sameDigitArray[i] != sameDigitArray[j])
                return false;
        }
        return true;
    }

    //y) Retorne os números existentes no vetor que não são de Amstrong. -----------------------------------

    public Vector notAmstrongNumbers() {

        Vector notAmstrong = new Vector();

        for (int i = 0; i < this.vector.length; i++) {
            if (!amstrongNumber(this.vector[i]))
                notAmstrong.addValue(this.vector[i]);
        }
        return notAmstrong;
    }

    public static double sumDigitsPowerThree(int number) {
        int digit = 0;
        int sumPowerThree = 0;

        while (number > 0) {
            digit = number % 10;
            number /= 10;
            sumPowerThree += Math.pow(digit, 3);
        }
        return sumPowerThree;
    }

    public static boolean amstrongNumber(int number) {
        if (number == sumDigitsPowerThree(number)) {
            return true;
        } else
            return false;
    }


    //z) Retorne os elementos que contêm uma sequência crescente de pelo menos n algarismos (e.g. n=3,347) do vetor.

    public Vector ascendingDigitsWithNElements(int n) {
        Vector ascendingDigitsForN = new Vector();

        for (int i = 0; i < this.vector.length; i++) {
            if (countDigits(this.vector[i]) >= n && numberWithAscendingDigits(this.vector[i]))
                ascendingDigitsForN.addValue(this.vector[i]);
        }
        return ascendingDigitsForN;
    }

    //aa) Retorne True ou False, consoante o vetor é igual a um vetor passado por parâmetro.

    public boolean verifyEqualityBetweenTwoVectors(int[] vector) {
        if (this.vector.length != vector.length)
            throw new IllegalArgumentException("Os vectores têm tamanho diferente, não são iguais");

        for (int i = 0; i < this.vector.length; i++) {
            if (this.vector[i] != vector[i])
                return false;
        }
        return true;
    }


}