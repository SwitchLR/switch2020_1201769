public class RowOrColumn {

    //attributes
    private int[] array;

    //constructor

    public RowOrColumn(int[] array) {
        this.array = copyArray(array);
    }

    //methods

    private int[] copyArray(int[] array) {
        int[] arrayCopy = new int[array.length];

        for (int i = 0; i < array.length; i++) {
            arrayCopy[i] = array[i];
        }
        return arrayCopy;
    }

    //verifica se o array não tem elementos do repetidos
    public static boolean verifyArray(int[] array) {
        if (Utilities.naoRepetidosArray(array).length != array.length)
            return false;

        return true;
    }

    public static int[][] fromColumnToArray(int[][] matrix) {

        int[][] transposed = Utilities.transposedMatrix(matrix);

        int[][] fromColumnToArray = new int[matrix.length][matrix[0].length];
        for (int i = 0; i < transposed.length; i++) {
            fromColumnToArray[i] = Utilities.inverteArray(transposed[i]);
        }
        return fromColumnToArray;
    }


        /*int[] array = new int[Utilities.highestRow(matrix)];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                if (j == column) {
                    array[i] = matrix[i][j];
                }
            }
        }
        return array; */
}




