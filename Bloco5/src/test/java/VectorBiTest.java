import org.junit.jupiter.api.Test;

import javax.annotation.processing.SupportedAnnotationTypes;
import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class VectorBiTest {

    @Test
    void isValidThrowIllegalArgumentExceptionTest() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    BiDimVector biVector = new BiDimVector((int[][]) null);
                });
    }

    @Test
    void copyBiDimVector() {
        int[][] testVector = {
                {1, 2},
                {3, 4}
        };
        BiDimVector biVector = new BiDimVector(testVector);

        int[][] expectedMatrix = {
                {1, 2},
                {3, 4}
        };
        BiDimVector expectedBi = new BiDimVector(expectedMatrix);

        Vector[] result = biVector.copyBiDimVector();

        assertArrayEquals(expectedBi.toVector(), result);
    }

    @Test
    void copyBiDimVectorTesteDois() {
        int[][] testVector = {
                {1, 2},
                {3, 4}
        };
        BiDimVector biVector = new BiDimVector(testVector);

        BiDimVector biVector2 = new BiDimVector(biVector.copyBiDimVector());

        assertEquals(biVector, biVector2);
    }

    @Test
    void copyBiDimVectorTesteTres() {
        int[][] testVector = {
                {1, 2},
                {3, 4, 5, 6}
        };
        BiDimVector biVector = new BiDimVector(testVector);

        BiDimVector biVector2 = new BiDimVector(biVector.copyBiDimVector());

        assertEquals(biVector, biVector2);
    }


    @Test
    void copyBiDimVectorTesteQuatro() {
        int[][] testVector = {
                {1, 2, 5},
                {3, 4, 6, 7, 8}
        };
        BiDimVector biVector = new BiDimVector(testVector);

        BiDimVector biVector2 = new BiDimVector(biVector.copyBiDimVector());

        assertArrayEquals(biVector.toArray(), biVector2.toArray());
    }

    @Test
    void addValueInSpecificLineTest() {
        int[][] testVector = {
                {1, 2, 5},
                {3, 4, 6, 7, 8}
        };
        int line = 1;
        int value = 9;

        int[][] expectedVector = {
                {1, 2, 5},
                {3, 4, 6, 7, 8, 9}
        };

        BiDimVector expected = new BiDimVector(expectedVector);

        BiDimVector result = new BiDimVector(testVector);
        result.addValueInSpecificLine(line, value);

        assertArrayEquals(expected.toArray(), result.toArray());
    }


    @Test
    void removeElementFromVector() {
        int[][] testVector = {
                {1, 2, 5},
                {3, 4, 6, 7, 8}
        };
        int value = 8;

        int[][] expectedVector = {
                {1, 2, 5},
                {3, 4, 6, 7}
        };

        BiDimVector expected = new BiDimVector(expectedVector);

        BiDimVector result = new BiDimVector(testVector);
        result.removeValueFromVector(value);

        assertEquals(expected, result);
    }


    /*  @Test
      void testRemoveFirstElementsOfGivenValue() {
          //Array
          Array array = new Array(new int[]{1, 2, 3, 4});
          Array array2 = new Array(new int[]{1, 2, 3, 5});

          //Array Of Arrays
          Array[] arrays = new Array[2];
          arrays[0] = array;
          arrays[1] = array2;

          //Array Of Arrays Expected
          int elementToRemove = 5;
          Array[] expected = new Array[2];
          expected[0] = array;
          expected[1] = array2;
          expected[1].removeFirstElementOf(elementToRemove);

          // Create BidimensionalArray Object
          BiDimVector arrayOfArrays = new BiDimVector(arrays);

          arrayOfArrays.removeFristElementOfGivenValue(elementToRemove);

          assertArrayEquals(expected, arrayOfArrays.toVector());
      }
  */
    @Test
    void emptyMatrixTest() {
        int[][] testBiVector = {
                {1, 2, 3},
                {4, 5, 6}
        };
        BiDimVector bivector = new BiDimVector(testBiVector);

        assertFalse(bivector.emptyMatrix());

    }

    @Test
    void emptyMatrixTrueTest() {
        int[][] testBiVector = {};
        BiDimVector bivector = new BiDimVector(testBiVector);

        assertTrue(bivector.emptyMatrix());
    }

    @Test
    void maxValueTest() {
        int[][] testVector = {
                {1, 2, 5},
                {3, 4, 6, 7, 8}
        };

        int expected = 8;

        BiDimVector test = new BiDimVector(testVector);

        int result = test.maxValue();

        assertEquals(expected, result);
    }

    @Test
    void minValueTest() {
        int[][] testVector = {
                {1, 2, 5},
                {3, 4, 6, 7, 0, 8}
        };
        int expected = 0;
        BiDimVector test = new BiDimVector(testVector);

        int result = test.minValue();

        assertEquals(expected, result);
    }


    @Test
    void averageValueTest() {
        int[][] testVector = {
                {1, 2, 7},
                {3, 6, 5}
        };
        int expected = 4;

        BiDimVector test = new BiDimVector(testVector);

        int result = test.averageValue();

        assertEquals(expected, result);
    }

    @Test
    void sumRowVectorTest() {
        int[][] testVector = {
                {1, 2},
                {3, 4}
        };

        BiDimVector test = new BiDimVector(testVector);

        Vector expectedVector = new Vector(new int[]{3, 7});

        Vector result = test.sumRowVector();

        assertEquals(expectedVector, result);
    }

    @Test
    void sumColumnVectorTest() {
        int[][] testVector = {
                {1, 2},
                {3, 4}
        };

        BiDimVector test = new BiDimVector(testVector);

        Vector expectedVector = new Vector(new int[]{4, 6});

        Vector result = test.sumColumnVector();

        assertEquals(expectedVector, result);
    }

    @Test
    void sumColumnVectorTestNotSquareMatrix() {
        int[][] testVector = {
                {1, 2},
                {3, 4, 5}
        };

        BiDimVector test = new BiDimVector(testVector);

        Vector expectedVector = new Vector(new int[]{4, 6, 5});

        Vector result = test.sumColumnVector();

        assertEquals(expectedVector, result);
    }

    @Test
    void highestSumIndexTest() {
        int[][] testVector = {
                {1, 2},
                {3, 4}
        };

        BiDimVector test = new BiDimVector(testVector);

        int expected = 1;

        int result = test.highestSumIndex();

        assertEquals(expected, result);
    }

    @Test
    void highestSumIndexTestTwo() {
        int[][] testVector = {
                {1, 2},
                {3, 4},
                {18, 20},
                {10, 12}
        };

        BiDimVector test = new BiDimVector(testVector);

        int expected = 2;

        int result = test.highestSumIndex();

        assertEquals(expected, result);
    }

    @Test
    void squareArrayVectorTestTrue() {
        int[][] testVector = {
                {1, 2},
                {3, 4},
        };

        BiDimVector test = new BiDimVector(testVector);

        boolean result = test.squareArrayVector();

        assertTrue(result);
    }

    @Test
    void squareArrayVectorTestFalse() {
        int[][] testVector = {
                {1, 2, 8},
                {3, 4, 6},
        };

        BiDimVector test = new BiDimVector(testVector);

        boolean result = test.squareArrayVector();

        assertFalse(result);
    }

    @Test
    void symmetricalSquareArrayVectorTestTrue() {
        int[][] testVector = {
                {1, 5, 9},
                {5, 3, 8},
                {9, 8, 7},
        };

        BiDimVector test = new BiDimVector(testVector);

        boolean result = test.symmetricalSquareArrayVector();

        assertTrue(result);
    }

    @Test
    void symmetricalSquareArrayVectorTestFalse() {
        int[][] testVector = {
                {1, 5, 9},
                {5, 3, 6},
                {9, 8, 7},
        };

        BiDimVector test = new BiDimVector(testVector);

        boolean result = test.symmetricalSquareArrayVector();

        assertFalse(result);
    }

    @Test
    void notZeroElementsInDiagonalTest() {
        int[][] testVector = {
                {1, 5, 9},
                {5, 2, 6},
                {9, 8, 7},
        };

        BiDimVector test = new BiDimVector(testVector);

        int expeted = 3;

        int result = test.notZeroElementsInDiagonal();

        assertEquals(expeted, result);
    }

    @Test
    void notZeroElementsInDiagonalTestWithZero() {
        int[][] testVector = {
                {1, 5, 9},
                {5, 0, 6},
                {9, 8, 0},
        };

        BiDimVector test = new BiDimVector(testVector);

        int expeted = 1;

        int result = test.notZeroElementsInDiagonal();

        assertEquals(expeted, result);
    }

    @Test
    void sameDiagonalsTestTrue() {
        int[][] testVector = {
                {2, 5, 2},
                {5, 2, 6},
                {2, 8, 2},
        };

        BiDimVector test = new BiDimVector(testVector);

        boolean result = test.sameDiagonals();

        assertTrue(result);
    }

    @Test
    void sameDiagonalsTestFalse() {
        int[][] testVector = {
                {2, 5, 2},
                {5, 2, 6},
                {8, 8, 2},
        };

        BiDimVector test = new BiDimVector(testVector);

        boolean result = test.sameDiagonals();

        assertFalse(result);
    }

    @Test
    void superiorToAverageTest() {
        int[][] testVector = {
                {1, 2, 3},
                {1, 2, 3},
                {1, 3, 4},
        };

        BiDimVector test = new BiDimVector(testVector);

        Vector expected = new Vector(new int[]{3, 3, 3, 4});

        Vector result = test.superiorToAverage();

        assertEquals(expected, result);
    }

    @Test
    void evenPercentageSuperiorToAverageTest() {
        int[][] testVector = {
                {10, 21},
                {30, 40},
        };

        BiDimVector test = new BiDimVector(testVector);

        Vector expected = new Vector(new int[]{40});

        Vector result = test.evenPercentageSuperiorToAverage();

        assertEquals(expected, result);
    }

    @Test
    void invertRowTestSquare() {
        int[][] testVector = {
                {1, 2, 3},
                {1, 2, 3},
                {1, 3, 4},
        };


        int[][] testExpected = {
                {3, 2, 1},
                {3, 2, 1},
                {4, 3, 1},
        };

        BiDimVector expected = new BiDimVector(testExpected);
        BiDimVector result = new BiDimVector(testVector);
        result.invertRow();

        assertEquals(expected, result);
    }

    @Test
    void invertRowTestRectangle() {
        int[][] testVector = {
                {1, 2, 3},
                {1, 2, 3},
                //{1, 3, 4},
        };


        int[][] testExpected = {
                {3, 2, 1},
                {3, 2, 1},
                //{4, 3, 1},
        };

        BiDimVector expected = new BiDimVector(testExpected);
        BiDimVector result = new BiDimVector(testVector);
        result.invertRow();

        assertEquals(expected, result);
    }

    @Test
    void invertColumnsVectorTest() {
        int[][] testVector = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9},
        };


        int[][] testExpected = {
                {7, 8, 9},
                {4, 5, 6},
                {1, 2, 3},
        };

        BiDimVector expected = new BiDimVector(testExpected);
        BiDimVector result = new BiDimVector(testVector);
        result.invertColumnsVector();

        assertEquals(expected, result);
    }

    //t)

    @Test
    void turnRightNinetyDegreesTest() {
        int[][] testVector = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9},
        };

        int[][] testExpected = {
                {7, 4, 1},
                {8, 5, 2},
                {9, 6, 3},
        };

        BiDimVector expected = new BiDimVector(testExpected);
        BiDimVector result = new BiDimVector(testVector);
        result.turnRightNinetyDegres();

        assertEquals(expected, result);
    }

    //u)
    @Test
    void turnAroundTest() {
        int[][] testVector = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9},
        };

        int[][] testExpected = {
                {9, 8, 7},
                {6, 5, 4},
                {3, 2, 1},
        };

        BiDimVector expected = new BiDimVector(testExpected);
        BiDimVector result = new BiDimVector(testVector);
        result.turnAround();

        assertEquals(expected, result);
    }

    //v)
    @Test
    void turnLeftNinetyDegreesTest() {
        int[][] testVector = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9},
        };

        int[][] testExpected = {
                {3, 6, 9},
                {2, 5, 8},
                {1, 4, 7},
        };

        BiDimVector expected = new BiDimVector(testExpected);
        BiDimVector result = new BiDimVector(testVector);
        result.turnLeftNinetyDegrees();

        assertEquals(expected, result);
    }
}