import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VectorTest {

    @Test
    void addValuetestTest() {
        int[] newVector = {1, 2, 3};
        Vector vector = new Vector(newVector);
        int value = 4;
        int[] expected = {1, 2, 3, 4};

        vector.addValue(value);

        assertArrayEquals(expected, vector.toArray());
    }

    @Test
    void addValuetestTest2() {
        int[] newVector = {};
        Vector vector = new Vector(newVector);
        int value = 4;
        int[] expected = {4};

        vector.addValue(value);

        assertArrayEquals(expected, vector.toArray());
    }

    @Test
    void removeElementTest() {
        int[] testVector = {1, 2, 3, 4, 5, 6};
        Vector vector = new Vector(testVector);
        int element = 5;

        int[] expected = {1, 2, 3, 4, 6};

        vector.removeElement(element);

        assertArrayEquals(expected, vector.toArray());
    }

    @Test
    void removeElementTestMoreThanOneRepetition() {
        int[] testVector = {1, 2, 3, 3, 3, 4, 5, 6};
        Vector vector = new Vector(testVector);
        int element = 3;

        int[] expected = {1, 2, 3, 3, 4, 5, 6};

        vector.removeElement(element);

        assertArrayEquals(expected, vector.toArray());
    }



    @Test
    void removeElementTestElementInLastPosition2() {
        int[] testVector = {1, 2, 3, 3, 3, 4, 5, 6};
        Vector vector = new Vector(testVector);
        int element = 6;

        int[] expected = {1, 2, 3, 3, 3, 4, 5};

        vector.removeElement(element);

        assertArrayEquals(expected, vector.toArray());
    }


    @Test
    void valueByPositionTest() {
        int[] newVector = {1, 2, 3};
        Vector vector = new Vector(newVector);
        int position = 1;
        int expected = 2;

        int result = vector.valueByPosition(position);

        assertEquals(expected, result);
    }

    @Test
    void numberElementsTest() {
        int[] newVector = {1, 2, 3};
        Vector vector = new Vector(newVector);
        int expected = 3;

        int result = vector.numberElements();

        assertEquals(expected, result);
    }

    @Test
    void maxElementTest() {
        int[] newVector = {1, 5, 3};
        Vector vector = new Vector(newVector);
        int expected = 5;

        int result = vector.maxElement();

        assertEquals(expected, result);
    }

    @Test
    void minElementTest() {
        int[] newVector = {10, 2, 3};
        Vector vector = new Vector(newVector);
        int expected = 2;

        int result = vector.minElement();

        assertEquals(expected, result);

    }

    @Test
    void averageOfElementsTest() {
        int[] newVector = {1, 2, 3};
        Vector vector = new Vector(newVector);
        int expected = 2;

        int result = vector.averageOfElements();

        assertEquals(expected, result);
    }

    @Test
    void averageOfEvenElementsTest() {
        int[] newVector = {1, 2, 3, 4, 5, 6};
        Vector vector = new Vector(newVector);
        int expected = 4;

        int result = vector.averageOfEvenElements();

        assertEquals(expected, result);
    }

    @Test
    void averageOfOddElementsTest() {
        int[] newVector = {1, 2, 3, 4, 5, 6};
        Vector vector = new Vector(newVector);
        int expected = 3;

        int result = vector.averageOfOddElements();

        assertEquals(expected, result);
    }

    @Test
    void averageOfElementMultiples() {
        int[] newVector = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        Vector vector = new Vector(newVector);
        int element = 3;
        int expected = 6;

        int result = vector.averageOfElementMultiples(element);

        assertEquals(expected, result);
    }

    @Test
    void orderAscendentTest() {
        int[] newVector = {3, 1, 2, 4};
        Vector vector = new Vector(newVector);
        int[] expected = {1, 2, 3, 4};

        int[] result = vector.orderAscendent();

        assertArrayEquals(expected, result);
    }

    @Test
    void orderDescendentTest() {
        int[] newVector = {3, 1, 2, 4};
        Vector vector = new Vector(newVector);
        int[] expected = {4, 3, 2, 1};

        int[] result = vector.orderDescendent();

        assertArrayEquals(expected, result);
    }

    @Test
    void arrayOfEvensFalseTest() {
        int[] newVector = {3, 1, 2, 4};
        Vector vector = new Vector(newVector);
        boolean expected = false;

        boolean result = vector.arrayOfEvens();

        assertFalse(result);
    }

    @Test
    void arrayOfEvensTrueTest() {
        int[] newVector = {10, 4, 2, 8};
        Vector vector = new Vector(newVector);
        boolean expected = true;

        boolean result = vector.arrayOfEvens();

        assertTrue(result);
    }

    @Test
    void arrayOfOddsTrueTest() {
        int[] newVector = {3, 15, 9, 7};
        Vector vector = new Vector(newVector);
        //boolean expected = true;

        boolean result = vector.arrayOfOdds();

        assertTrue(result);
    }

    @Test
    void arrayWithRepeatedElementsTrueTest() {
        int[] newVector = {1, 1, 3, 4, 5};
        Vector vector = new Vector(newVector);
        //boolean expected = true;

        boolean result = vector.arrayWithRepeatedElements();

        assertTrue(result);
    }

    @Test
    void arrayWithRepeatedElementsFalseTest() {
        int[] newVector = {1, 10, 3, 4, 5};
        Vector vector = new Vector(newVector);
        //boolean expected = true;

        boolean result = vector.arrayWithRepeatedElements();

        assertFalse(result);
    }

    @Test
    void countDigitsTest() {
        int number = 12345;
        int expected = 5;

        int result = Vector.countDigits(number);
    }

    @Test
    void elementsWithHigherNumberOfDigitsThanAverageTest() {
        int[] newVector = {3, 1, 200, 4, 50, 6};
        Vector vector = new Vector(newVector);
        int[] expected = {200, 50};

        Vector result = vector.elementsWithHigherNumberOfDigitsThanAverage();

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    void elementsWithHigherNumberOfDigitsThanAverageTest2() {
        int[] newVector = {3, 1, 200, 4, 50, 6, 40};
        Vector vector = new Vector(newVector);
        int[] expected = {200, 50, 40};

        Vector result = vector.elementsWithHigherNumberOfDigitsThanAverage();

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    void evenDigitsPercentageSuperiorAverageEvenDigitsAllElementsTest() {
        int[] newVector = {3, 1, 200, 4, 50, 6, 40};
        Vector vector = new Vector(newVector);
        int[] expected = {200, 4, 6, 40};

        Vector result = vector.evenDigitsPercentageSuperiorAverageEvenDigitsAllElements();

        assertArrayEquals(expected, result.toArray());
    }


    @Test
    void onlyEvenDigitsTest() {
        int[] testVector = {20, 31, 4, 10};
        Vector vector = new Vector(testVector);
        int[] expected = {20, 4};

        Vector result = vector.onlyEvenDigits();

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    void fromNumberToArrayOfDigitsTest() {
        int number = 347;
        int[] expected = {3, 4, 7};

        int[] result = Vector.fromNumberToArrayOfDigits(number);

        assertArrayEquals(expected, result);
    }

    @Test
    void numberWithAscendingDigitsTest() {
        int number = 347;
        boolean expected = true;

        boolean result = Vector.numberWithAscendingDigits(number);

        assertTrue(result);
    }

    @Test
    void numberWithAscendingDigitsTest2() {
        int number = 387;
        boolean expected = false;

        boolean result = Vector.numberWithAscendingDigits(number);

        assertFalse(result);
    }

    @Test
    void numberWithAscendingDigitsTest3() {
        int number = 4;
        boolean expected = true;

        boolean result = Vector.numberWithAscendingDigits(number);

        assertTrue(result);
    }

    @Test
    void ascendentDigitsTest() {
        int[] testVector = {20, 31, 4, 10, 123};
        Vector vector = new Vector(testVector);
        int[] expected = {4, 123};

        Vector result = vector.ascendentDigits();

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    void verifyPalindromeTest() {
        int number = 2332;

        boolean result = Vector.verifyPalindrome(number);

        assertTrue(result);
    }

    @Test
    void verifyPalindromeTestFalse() {
        int number = 234532;

        boolean result = Vector.verifyPalindrome(number);

        assertFalse(result);
    }

    @Test
    void palindromesTest() {
        int[] testVector = {121, 8, 10, 323};
        Vector vector = new Vector(testVector);
        int[] expected = {121, 8, 323};

        Vector result = vector.palindromes();

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    void sameDigitsNumbersTest() {
        int[] testVector = {121, 8, 10, 333, 54};
        Vector vector = new Vector(testVector);
        int[] expected = {8, 333};

        Vector result = vector.sameDigitsNumbers();

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    void notAmstrongTest() {
        int[] testVector = {153, 8, 371, 407, 54};
        Vector vector = new Vector(testVector);
        int[] expected = {8, 54};

        Vector result = vector.notAmstrongNumbers();

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    void ascendingDigitsWithNElementsTest() {
        int[] testVector = {157, 8, 371, 479, 54};
        int n = 3;
        Vector vector = new Vector(testVector);
        int[] expected = {157, 479};

        Vector result = vector.ascendingDigitsWithNElements(n);

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    void verifyEqualityBetweenTwoVectorsTest() {
        int[] testVector = {1, 2, 3};
        Vector vector = new Vector(testVector);
        int[] vectorAux = {1, 2, 3};

        boolean result = vector.verifyEqualityBetweenTwoVectors(vectorAux);

        assertTrue(result);
    }

    @Test
    void verifyEqualityBetweenTwoVectorsTestTwo() {
        int[] testVector = {1, 2, 3, 6};
        Vector vector = new Vector(testVector);
        int[] vectorAux = {1, 2, 3, 5};

        boolean result = vector.verifyEqualityBetweenTwoVectors(vectorAux);

        assertFalse(result);
    }




}