import org.junit.jupiter.api.Test;

import java.util.function.BooleanSupplier;

import static org.junit.jupiter.api.Assertions.*;

class ResolutionTest {

    @Test
    void isValidTestNull() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    Resolution test = new Resolution(null);
                });
    }

    @Test
    void isValidTestTrue9x9() {
        int[][] testMatrix = {

                {8, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 3, 6, 0, 0, 0, 0, 0},
                {0, 7, 0, 0, 9, 0, 2, 0, 0},

                {0, 5, 0, 0, 0, 7, 0, 0, 0},
                {0, 0, 0, 0, 4, 5, 7, 0, 0},
                {0, 0, 0, 1, 0, 0, 0, 3, 0},

                {0, 0, 1, 0, 0, 0, 0, 6, 8},
                {0, 0, 8, 5, 0, 0, 0, 1, 0},
                {0, 9, 0, 0, 0, 1, 0, 4, 0}

        };

        Resolution test = new Resolution(testMatrix);

        boolean result = test.isValid(testMatrix);

        assertTrue(result);
    }

    @Test
    void isValidColumnsDifferentFrom9x9Test() {
        int[][] testMatrix = {

                {8, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                {0, 0, 3, 6, 0, 0, 0, 0, 0},
                {0, 7, 0, 0, 9, 0, 2, 0, 0},

                {0, 5, 0, 0, 0, 7, 0, 0, 0},
                {0, 0, 0, 0, 4, 5, 7, 0, 0},
                {0, 0, 0, 1, 0, 0, 0, 3, 0},

                {0, 0, 1, 0, 0, 0, 0, 6, 8},
                {0, 0, 8, 5, 0, 0, 0, 1, 0},
                {0, 9, 0, 0, 0, 1, 0, 4, 0}

        };


        assertThrows(IllegalArgumentException.class,
                () -> {
                    Resolution test = new Resolution(testMatrix);
                });
    }

    @Test
    void isValidTestNumberHigherThan9() {
        int[][] testMatrix = {

                {8, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 3, 6, 0, 0, 0, 40, 0},
                {0, 7, 0, 0, 9, 0, 2, 0, 0},

                {0, 5, 0, 0, 0, 7, 0, 0, 0},
                {0, 0, 0, 0, 4, 5, 7, 0, 0},
                {0, 0, 0, 1, 0, 0, 0, 3, 0},

                {0, 0, 1, 0, 0, 0, 0, 6, 8},
                {0, 0, 8, 5, 0, 0, 0, 1, 0},
                {0, 9, 0, 0, 0, 1, 0, 4, 0}

        };

        assertThrows(IllegalArgumentException.class,
                () -> {
                    Resolution test = new Resolution(testMatrix);
                });
    }

    @Test
    void fromSectorToArrayTest() {
        int[][] testMatrix = {

                {8, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 3, 6, 0, 0, 0, 0, 0},
                {0, 7, 0, 0, 9, 0, 2, 0, 0},

                {0, 5, 0, 0, 0, 7, 0, 0, 0},
                {0, 0, 0, 0, 4, 5, 7, 0, 0},
                {0, 0, 0, 1, 0, 0, 0, 3, 0},

                {0, 0, 1, 0, 0, 0, 0, 6, 8},
                {0, 0, 8, 5, 0, 0, 0, 1, 0},
                {0, 9, 0, 0, 0, 1, 0, 4, 0}

        };

        Sector test = new Sector(testMatrix);

        int[] sector = {2, 1};

        int[] result = test.fromSectorToArray(sector);

        int[] expected = {0, 0, 0, 5, 0, 0, 0, 0, 1};

        assertArrayEquals(expected, result);
    }

    @Test
    void resolvedTestLineTrue() {
        int[][] solvedMatrix = {
                {8, 1, 2, 7, 5, 3, 6, 4, 9},
                {9, 4, 3, 6, 8, 2, 1, 7, 5},
                {6, 7, 5, 4, 9, 1, 2, 8, 3},

                {1, 5, 4, 2, 3, 7, 8, 9, 6},
                {3, 6, 9, 8, 4, 5, 7, 2, 1},
                {2, 8, 7, 1, 6, 9, 5, 3, 4},

                {5, 2, 1, 9, 7, 4, 3, 6, 8},
                {4, 3, 8, 5, 2, 6, 9, 1, 7},
                {7, 9, 6, 3, 1, 8, 4, 5, 2}
        };

        Resolution test = new Resolution(solvedMatrix);

        boolean result = test.resolved();

        assertTrue(result);
    }


    @Test
    void resolvedTestLineFalse() { // o numero 1 está repetido na primeira linha
        int[][] solvedMatrix = {
                {8, 1, 2, 7, 5, 1, 6, 4, 9},
                {9, 4, 3, 6, 8, 2, 1, 7, 5},
                {6, 7, 5, 4, 9, 1, 2, 8, 3},

                {1, 5, 4, 2, 3, 7, 8, 9, 6},
                {3, 6, 9, 8, 4, 5, 7, 2, 1},
                {2, 8, 7, 1, 6, 9, 5, 3, 4},

                {5, 2, 1, 9, 7, 4, 3, 6, 8},
                {4, 3, 8, 5, 2, 6, 9, 1, 7},
                {7, 9, 6, 3, 1, 8, 4, 5, 2}
        };

        Resolution test = new Resolution(solvedMatrix);

        boolean result = test.resolved();

        assertFalse(result);
    }

    @Test
    void resolvedTestColumnTrue() {
        int[][] solvedMatrix = {
                {8, 1, 2, 7, 5, 3, 6, 4, 9},
                {9, 4, 3, 6, 8, 2, 1, 7, 5},
                {6, 7, 5, 4, 9, 1, 2, 8, 3},

                {1, 5, 4, 2, 3, 7, 8, 9, 6},
                {3, 6, 9, 8, 4, 5, 7, 2, 1},
                {2, 8, 7, 1, 6, 9, 5, 3, 4},

                {5, 2, 1, 9, 7, 4, 3, 6, 8},
                {4, 3, 8, 5, 2, 6, 9, 1, 7},
                {7, 9, 6, 3, 1, 8, 4, 5, 2}
        };

        Resolution test = new Resolution(solvedMatrix);

        boolean result = test.resolved();

        assertTrue(result);
    }


}